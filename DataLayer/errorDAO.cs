﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

using NHibernate;
using NHibernate.Criterion;

namespace myVRM.DataLayer
{
    public class errorlogDAO
    {
        private ISession session;
        private log4net.ILog m_log;
        private string m_configPath;

        public errorlogDAO(string config, log4net.ILog olog)
        {
            try
            {
                m_log = olog;
                m_configPath = config;

                bool valid = Init();
                if (!valid)
                    throw new Exception("Could not initialize errorlogDAO");

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }

        private bool Init()
        {
            try
            {
                session = SessionManagement.GetSession(m_configPath);
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }

        public IvrmErrorLogDAO GetErrorLogDAO()
        {
            return new ErrorLogDAO(m_configPath);
        }

        public class ErrorLogDAO : 
            AbstractPersistenceDao<vrmErrorLog, int>, IvrmErrorLogDAO
        {
            public ErrorLogDAO(string ConfigPath) : base(ConfigPath) { }
            
        }
    }
}
