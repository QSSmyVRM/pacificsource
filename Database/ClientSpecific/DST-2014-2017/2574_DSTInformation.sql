/* ************************* FB 2574 Starts - 11 Apr 2013 ***************************** */
update gen_timezone_s set CurDSTstart = CurDSTstart4, CurDSTend = CurDSTend4,
CurDSTstart1 = CurDSTstart5, CurDSTend1 =CurDSTend5, CurDSTstart2 = CurDSTstart6, CurDSTend2 =CurDSTend6, 
CurDSTstart3 = CurDSTstart7, CurDSTend3 =CurDSTend7, CurDSTstart4 = CurDSTstart8, CurDSTend4 =CurDSTend8, 
CurDSTstart5 = CurDSTstart9, CurDSTend5 =CurDSTend9 

update gen_timezone_s set TimeZone = 'UTC' where timezoneid = 33  -- Casablanca to UTC

--1
insert into gen_timezone_s values
(76, 'Greenwich Standard Time',0,'Casablanca','(GMT)',
0,0,-60,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
'1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000',
'1980-01-01 00:00:00.000', '1980-01-01 00:00:00.000',
'2010-05-22 02:00:00.000', '2010-08-08 03:00:00.000',
'2011-04-03 02:00:00.000', '2011-07-31 03:00:00.000',
'2012-04-29 02:00:00.000', '2012-09-30 03:00:00.000',
'2013-04-28 02:00:00.000', '2013-09-29 03:00:00.000',
'2014-04-27 02:00:00.000', '2014-09-28 03:00:00.000',
'2015-04-26 02:00:00.000', '2015-09-27 03:00:00.000',
'2016-04-24 02:00:00.000', '2016-09-25 03:00:00.000',
'2017-04-30 02:00:00.000', '2017-09-24 03:00:00.000')

--10	
update gen_timezone_s set 
CurDSTstart6 = '1980-01-01 00:00:00.000', CurDSTend6 = '1980-01-01 00:00:00.000',
CurDSTstart7 = '1980-01-01 00:00:00.000', CurDSTend7 = '1980-01-01 00:00:00.000',
CurDSTstart8 = '1980-01-01 00:00:00.000', CurDSTend8 = '1980-01-01 00:00:00.000',
CurDSTstart9 = '1980-01-01 00:00:00.000', CurDSTend9 = '1980-01-01 00:00:00.000'
where TimeZoneID in (28,44,48,49,53,68,74,5,27,41)

--1
update gen_timezone_s set 
CurDSTstart6 = '2014-03-22 00:00:00.000', CurDSTend6 = '2014-09-22 00:00:00.000',
CurDSTstart7 = '2015-03-22 00:00:00.000', CurDSTend7 = '2015-09-22 00:00:00.000',
CurDSTstart8 = '2016-03-21 00:00:00.000', CurDSTend8 = '2016-09-21 00:00:00.000',
CurDSTstart9 = '2017-03-22 00:00:00.000', CurDSTend9 = '2017-09-22 00:00:00.000'
where TimeZoneID in (37)


--7
update gen_timezone_s set 
CurDSTstart6 = '2014-03-09 02:00:00.000', CurDSTend6 = '2014-11-02 02:00:00.000',
CurDSTstart7 = '2015-03-08 02:00:00.000', CurDSTend7 = '2015-11-01 02:00:00.000',
CurDSTstart8 = '2016-03-13 02:00:00.000', CurDSTend8 = '2016-11-06 02:00:00.000',
CurDSTstart9 = '2017-03-12 02:00:00.000', CurDSTend9 = '2017-11-15 02:00:00.000'
where TimeZoneID in (19,26,42,51,47,2,6)

--3
update gen_timezone_s set 
CurDSTstart6 = '2014-03-30 03:00:00.000', CurDSTend6 = '2014-10-26 04:00:00.000',
CurDSTstart7 = '2015-03-29 03:00:00.000', CurDSTend7 = '2015-10-25 04:00:00.000',
CurDSTstart8 = '2016-03-27 03:00:00.000', CurDSTend8 = '2016-10-30 04:00:00.000',
CurDSTstart9 = '2017-03-26 03:00:00.000', CurDSTend9 = '2017-10-29 04:00:00.000'
where TimeZoneID in (24,34,30)

--1
update gen_timezone_s set 
CurDSTstart6 = '2014-03-30 01:00:00.000', CurDSTend6 = '2014-10-26 02:00:00.000',
CurDSTstart7 = '2015-03-29 01:00:00.000', CurDSTend7 = '2015-10-25 02:00:00.000',
CurDSTstart8 = '2016-03-27 01:00:00.000', CurDSTend8 = '2016-10-30 02:00:00.000',
CurDSTstart9 = '2017-03-26 01:00:00.000', CurDSTend9 = '2017-10-29 02:00:00.000'
where TimeZoneID in (31)

--1
update gen_timezone_s set 
CurDSTstart6 = '2014-03-30 00:00:00.000', CurDSTend6 = '2014-10-26 01:00:00.000',
CurDSTstart7 = '2015-03-29 00:00:00.000', CurDSTend7 = '2015-10-25 01:00:00.000',
CurDSTstart8 = '2016-03-27 00:00:00.000', CurDSTend8 = '2016-10-30 01:00:00.000',
CurDSTstart9 = '2017-03-26 00:00:00.000', CurDSTend9 = '2017-10-29 01:00:00.000'
where TimeZoneID in (9)

--1
update gen_timezone_s set 
CurDSTstart6 = '2014-03-30 04:00:00.000', CurDSTend6 = '2014-10-26 05:00:00.000',
CurDSTstart7 = '2015-03-29 04:00:00.000', CurDSTend7 = '2015-10-25 05:00:00.000',
CurDSTstart8 = '2016-03-27 04:00:00.000', CurDSTend8 = '2016-10-30 05:00:00.000',
CurDSTstart9 = '2017-03-26 04:00:00.000', CurDSTend9 = '2017-10-29 05:00:00.000'
where TimeZoneID in (12)

--5
update gen_timezone_s set 
CurDSTstart6 = '2014-03-30 02:00:00.000', CurDSTend6 = '2014-10-26 03:00:00.000',
CurDSTstart7 = '2015-03-29 02:00:00.000', CurDSTend7 = '2015-10-25 03:00:00.000',
CurDSTstart8 = '2016-03-27 02:00:00.000', CurDSTend8 = '2016-10-30 03:00:00.000',
CurDSTstart9 = '2017-03-26 02:00:00.000', CurDSTend9 = '2017-10-29 03:00:00.000'
where TimeZoneID in (16,17,52,71,75)

--1
update gen_timezone_s set 
CurDSTstart6 = '2014-03-29 22:00:00.000', CurDSTend6 = '2014-10-25 23:00:00.000',
CurDSTstart7 = '2015-03-28 22:00:00.000', CurDSTend7 = '2015-10-24 23:00:00.000',
CurDSTstart8 = '2016-03-26 22:00:00.000', CurDSTend8 = '2016-10-29 23:00:00.000',
CurDSTstart9 = '2017-03-25 22:00:00.000', CurDSTend9 = '2017-10-28 23:00:00.000'
where TimeZoneID in (32)

--1
update gen_timezone_s set 
CurDSTstart6 = '2014-04-06 02:00:00.000', CurDSTend6 = '2014-10-26 03:00:00.000',
CurDSTstart7 = '2015-04-05 02:00:00.000', CurDSTend7 = '2015-10-25 03:00:00.000',
CurDSTstart8 = '2016-04-03 02:00:00.000', CurDSTend8 = '2016-10-30 03:00:00.000',
CurDSTstart9 = '2017-04-02 02:00:00.000', CurDSTend9 = '2017-10-29 03:00:00.000'
where TimeZoneID in (40)

--1
update gen_timezone_s set 
CurDSTstart6 = '2013-09-29 02:00:00.000', CurDSTend6 = '2014-04-06 03:00:00.000',
CurDSTstart7 = '2014-09-28 02:00:00.000', CurDSTend7 = '2015-04-05 03:00:00.000',
CurDSTstart8 = '2015-09-27 02:00:00.000', CurDSTend8 = '2016-04-03 03:00:00.000',
CurDSTstart9 = '2016-09-25 02:00:00.000', CurDSTend9 = '2017-04-02 03:00:00.000'
where TimeZoneID in (46)

--3
update gen_timezone_s set 
CurDSTstart6 = '2013-10-06 02:00:00.000', CurDSTend6 = '2014-04-06 03:00:00.000',
CurDSTstart7 = '2014-10-05 02:00:00.000', CurDSTend7 = '2015-04-05 03:00:00.000',
CurDSTstart8 = '2015-10-04 02:00:00.000', CurDSTend8 = '2016-04-03 03:00:00.000',
CurDSTstart9 = '2016-10-02 02:00:00.000', CurDSTend9 = '2017-04-02 03:00:00.000'
where TimeZoneID in (63,8,13)

--1
update gen_timezone_s set 
CurDSTstart6 = '2013-10-13 00:00:00.000', CurDSTend6 = '2014-03-09 00:00:00.000',
CurDSTstart7 = '2014-10-12 00:00:00.000', CurDSTend7 = '2015-03-15 00:00:00.000',
CurDSTstart8 = '2015-10-11 00:00:00.000', CurDSTend8 = '2016-03-13 00:00:00.000',
CurDSTstart9 = '2016-10-09 00:00:00.000', CurDSTend9 = '2017-03-12 00:00:00.000'
where TimeZoneID in (50)

--1
update gen_timezone_s set 
CurDSTstart6 = '2013-10-20 00:00:00.000', CurDSTend6 = '2014-02-16 00:00:00.000',
CurDSTstart7 = '2014-10-19 00:00:00.000', CurDSTend7 = '2015-02-22 00:00:00.000',
CurDSTstart8 = '2015-10-18 00:00:00.000', CurDSTend8 = '2016-02-21 00:00:00.000',
CurDSTstart9 = '2016-10-16 00:00:00.000', CurDSTend9 = '2017-02-19 00:00:00.000'
where TimeZoneID in (25)

/* ************************* FB 2574 Ends - 11 Apr 2013 ***************************** */


/* ************************* FB 2574 Starts - 19 Apr 2013 ***************************** */
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


ALTER FUNCTION 
[dbo].[GetDSTDate]
(@tzid INTEGER, @time DATETIME, @flag INTEGER)  
RETURNS DATETIME 
AS  
BEGIN 
	DECLARE @year AS INTEGER
	DECLARE @return AS DATETIME
	SELECT TOP 1 @year = year(@time) - 2008 FROM SYS_SETTINGS_D
	IF @year = 0
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart FROM GEN_TIMEZONE_S  WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 1
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart1 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend1 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 2
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart2 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend2 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 3
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart3 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend3 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 4
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart4 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend4 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 5
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart5 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend5 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 6
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart6 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend6 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 7
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart7 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend7 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 8
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart8 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend8 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	ELSE IF @year = 9
	BEGIN
		IF @flag = 1
		BEGIN
			/* Return the start date of DST */
			SELECT @return = curdststart9 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
		ELSE IF @flag = 2
		BEGIN
			/* Return the end date of DST */
			SELECT @return = curdstend9 FROM GEN_TIMEZONE_S WHERE timezoneid = @tzid
			RETURN @return
		END
	END
	RETURN @return
END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


/* ************************* FB 2574 Ends - 19 Apr 2013 ***************************** */