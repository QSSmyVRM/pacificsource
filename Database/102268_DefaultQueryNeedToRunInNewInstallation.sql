
/* **************************** ZD 102268 - Default Options on new myVRM Installations  START  ************************** */

---Admin->Options

update Org_Settings_D set tzsystemid= 0 --- World Wide Time Zone

update Org_Settings_D set DefLinerate= 1024--- Default line rate 1 mbps

update Org_Settings_D set EnablePublicConf= 0 --- Enable Public Conference: No

update Org_Settings_D set SpecialRecur= 0 ---Enable Special Recurrence: No

update Org_Settings_D set Open24hrs=1 , Offdays=''---Open 24 Hours: Checked, Days Closed: None Checked

update Org_Settings_D set EnableStartMode= 0 ----Enable Start Mode: No

update Org_Settings_D set DefaultToPublic = 0 ---Default All Conferences to Public: No

---Site->Settings

update Sys_Settings_D set ViewPublicConf= 2 ----Display Public Conference on Login Screen: No


/* **************************** ZD 102268 - Default Options on new myVRM Installations  END  ************************** */