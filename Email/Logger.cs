//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886  End
/* FILE : Logger.cs
 * DESCRIPTION : All file and database logging methods are stored in this file. 
 * AUTHOR : Kapil M
 */
using System;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace NS_LOGGER
{
	#region References
	
	#endregion 

	class Log
	{
		private string logFilePath = "C:\\VRMSchemas_v1.8.3\\RTCLog.log";
		private bool fileTraceEnabled = true;
		private NS_MESSENGER.ConfigParams configParams;
		internal Log (NS_MESSENGER.ConfigParams config)
		{
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;

			if (configParams.logFilePath.Length > 1)
				logFilePath = configParams.logFilePath;
			fileTraceEnabled = configParams.fileTraceEnabled;
		}

		internal void Exception (int errorCode,string message)
		{
			message = ReplaceInvalidChars(message);			
			int severity = FetchSeverityLevel(errorCode);			

			StackFrame CallStack = new StackFrame(1, true);
			string file = CallStack.GetFileName();
			string method = CallStack.GetMethod().Name;
			int line = CallStack.GetFileLineNumber();

			string logRecord = "<EXCEPTION>";
			logRecord += "<MESSAGE>" + message + "</MESSAGE>";
			logRecord += "<SEVERITY>" + severity.ToString() + "</SEVERITY>";
			logRecord += "<FILE>" + file + "</FILE>";
			logRecord += "<METHOD>" + method + "</METHOD>";
			logRecord += "<LINE>" + line.ToString() + "</LINE>";
			logRecord += "<TIMESTAMP>" + DateTime.Now.ToString() + "</TIMESTAMP>";
			logRecord += "</EXCEPTION>";
			
			WriteToLogFile(logRecord);	
			LogInDatabase(errorCode,severity,file,method,line,message);
		}
		
		internal  void Trace (string message)
		{
			if (fileTraceEnabled)
			{
				StackFrame CallStack = new StackFrame(1, true);
				string file = CallStack.GetFileName();
				string method = CallStack.GetMethod().Name;
				int line = CallStack.GetFileLineNumber();

				WriteToLogFile(message);
				LogInDatabase(9999,9,file,method,line,message);
			}			
		}
		
		internal  string FetchComMsg(int errorCode,string level,string message)
		{
			string errorMsg = "<error>";
			errorMsg += "<errorCode>" + errorCode.ToString() + "</errorCode>";
			errorMsg += "<message>" + message + "</message>";
			errorMsg += "<level>" + level + "</level>";
			errorMsg += "</error>";

			return (errorMsg);
		}

		private int FetchSeverityLevel(int errorCode)
		{
			if (errorCode >=0 && errorCode <=99)
				return (0);
			if (errorCode >=100 && errorCode <=199)
				return (1);
			if (errorCode >=200 && errorCode <=299)
				return (2);
			if (errorCode >=300 && errorCode <=399)
				return (3);

			return (0);
		}

        private void WriteToLogFile(string logRecord)
        {

            Console.WriteLine(logRecord);
            StreamWriter sw;
            String newLogfilepath = "";

            try
            {
                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();

                string lgName = sYear + sMonth + sDay;

                newLogfilepath = logFilePath;

                newLogfilepath = newLogfilepath + "_" + lgName + ".log";

                if (!File.Exists(newLogfilepath))
                {
                    // file doesnot exist . hence, create a new log file.
                    sw = File.CreateText(newLogfilepath);
                    sw.Flush();
                    sw.Close();
                }
                else
                {
                    // check if exisiting log file size is greater than 50 MB
                    FileInfo fi = new FileInfo(newLogfilepath);
                    if (fi.Length > 50000000)
                    {
                        // delete the log file						
                        File.Delete(logFilePath);

                        // create a new log file 
                        sw = File.CreateText(newLogfilepath);
                        sw.Flush();
                        sw.Close();
                    }
                }

                // write the log record.
                sw = File.AppendText(newLogfilepath);
                sw.WriteLine(logRecord);
                sw.Flush();
                sw.Close();
            }
            catch (Exception)
            {
                // do nothing
            }
        }

        // Public common method 
        // TODO : Move it to another central class in future.
        internal string ReplaceInvalidChars(string input)
        {
            input = input.Replace("<", "&lt;");
            input = input.Replace(">", "&gt;");
            input = input.Replace("&", "&amp;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\"", "&quot;");
            input = input.Replace("\'", "&apos;;");
            return (input);
        }

        // Public common method 
        // TODO : Move it to another central class in future.
        internal string RevertBackTheInvalidChars(string input)
        {
            input = input.Replace("&lt;", "<");
            input = input.Replace("&gt;", ">");
            input = input.Replace("&amp;", "&");
            input = input.Replace("&quot;", "\"");
            input = input.Replace("&quot;", "\"");
            input = input.Replace("&apos;", "\'");
            return (input);
        }

		private  void LogInDatabase (int errorCode,int severity,string file,string function,int line , string message)
		{	
			try 
			{				
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
				switch (severity)
				{
					case 0:
					{
						if (configParams.logLevel == NS_MESSENGER.ConfigParams.eLogLevel.SYSTEM_ERROR)
						{
							db.InsertLogRecord(errorCode,severity,file,function,line,message);
						}
						break;
					}
					case 1:
					{
						if (configParams.logLevel == NS_MESSENGER.ConfigParams.eLogLevel.USER_ERROR)
						{
							db.InsertLogRecord(errorCode,severity,file,function,line,message);
						}
						break;
					}
					case 2:
					{
						if (configParams.logLevel == NS_MESSENGER.ConfigParams.eLogLevel.WARNING)
						{
							db.InsertLogRecord(errorCode,severity,file,function,line,message);
						}
						break;
					}
					case 3:
					{
						if (configParams.logLevel == NS_MESSENGER.ConfigParams.eLogLevel.INFO)
						{
							db.InsertLogRecord(errorCode,severity,file,function,line,message);
						}
						break;
					}
					case 9:
					{
						if (configParams.logLevel == NS_MESSENGER.ConfigParams.eLogLevel.DEBUG)																																																																																  
						{
							db.InsertLogRecord(errorCode,severity,file,function,line,message);
						}
						break;
					}
				}
				
			}
			catch (Exception)
			{
				// do nothing
			}			
		}
	}
	
	class EvtLog
	{
		internal string errMsg = null;
		private NS_MESSENGER.ConfigParams configParams;
		
		internal EvtLog (NS_MESSENGER.ConfigParams config)
		{
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;
		}
	
		internal bool Log(string sSource,string sLog,string sEvent)
		{
			try
			{
				if (!EventLog.SourceExists(sSource))
					EventLog.CreateEventSource(sSource,sLog);
				EventLog.WriteEntry(sSource,sEvent);
				EventLog.WriteEntry(sSource,sEvent,EventLogEntryType.Error,100);
				
				return true;
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				return false;
			}
			
		}
	}

}
