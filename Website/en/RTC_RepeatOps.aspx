<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100886 End--%>
 <script runat="server">
    
public void Page_Load(Object sender, System.EventArgs e)
{
    ns_SuperAdministrator.SuperAdministrator objActv = new ns_SuperAdministrator.SuperAdministrator();
    objActv.GetActivation(Request.QueryString["path"].ToString());
    
}
</script>
