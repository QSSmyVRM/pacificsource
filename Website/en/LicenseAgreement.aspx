﻿<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_LicenseAgreement" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" /><%--FB 2852--%>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
  <title>myVRM</title>
  <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
  <meta name="Keywords" content="VRM, myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <META NAME="LANGUAGE" CONTENT="en">
  <META NAME="DOCUMENTCOUNTRYCODE" CONTENT="us">

  <link title="Expedite base styles" href="../en/Organizations/Original/Styles/main.css" type="text/css" rel="stylesheet" /> <%--FB 1830--%>
<STYLE>
@media print {

    .btprint {
        display:none;
        }
}
</STYLE> 

    <script type="text/javascript">        // FB 2815
        var path = '<%=Session["OrgCSSPath"]%>';
        path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css";
        document.write("<link rel='stylesheet' title='Expedite base styles' type='text/css' href='" + path + "'?'" + new Date().getTime() + "'>"); //ZD 100152
    </script> 

  <script language="javascript">
  <!--
  
	function international (cb)
	{
		if ( cb.options[cb.selectedIndex].value != "" ) {
			str="" + window.location; 
			newstr=str.replace(/\/en\//i, "/" + cb.options[cb.selectedIndex].value + "/"); 
			if ((ind1 = newstr.indexOf("sct=")) != -1) {
				if ( (ind2=newstr.indexOf("&", ind1)) != -1)
					newstr = newstr.substring(0, ind1-1) + newstr.substring(ind2, newstr.length-1);
				else
					newstr = newstr.substring(0, ind1-1);
			}
			newstr += ((newstr.indexOf("?") == -1) ? "?" : "&") + "sct=" + cb.selectedIndex; 
			document.location = newstr; 
		} 
	}
    function adjustWidth(obj)
    {
        if(obj.style.width == "")
        if (obj.src.indexOf("lobbytop1024.jpg") >= 0)
        {
            obj.style.width=window.screen.width-25;
            //FB 1830
            if (window.screen.width <= 1024)
                obj.src = "../en/Organizations/Original/Image/lobbytop1024.jpg";
            else
                obj.src = "../en/Organizations/Original/Image/lobbytop1024.jpg";
                
        }
    }
    
      //-->	
  </script>
	


</head>


	<script language="JavaScript">
		onback = history.forward(1);

		document.onkeydown = function (e) {		// can not deal with chkbx
			if (window.event) {	// IE
				if (window.event.keyCode == 113)
					openhelp();
					
				switch (window.event.srcElement.tagName) {
					case "INPUT" :
					case "TEXTAREA" :
						return true;
						break;
					default :
						return ( (window.event.keyCode != 8) && (window.event.keyCode != 37) );
						break;
				}

			} else if (e.which) {	// netscape
				if (e.which == 113)
					openhelp();
					
				switch (e.target.tagName) {
					case "INPUT" :
					case "TEXTAREA" :
						return true;
						break;
					default :
						return ( (e.which != 8) && (e.which != 37) );
						break;
				}
			} else {
				return -1;
			}
			
		};
		
	</script>


<script type="text/javascript" src="script/errorList.js"></script>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0" marginwidth="0">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr valign="top">
      <td width="100%" height="72">
        <img border="0" src="../en/<%=Session["OrgBanner1024Path"]%>" id="mainTop" height="72"  alt="organization Banner"/> <%--FB 2164--%><%--ZD 100419--%>
       </td>
    </tr>
  </table>
<table width="90%" border="0" cellpadding="0" cellspacing="0">
  <tr valign="top">    
    <td width="20%" valign="top" align="center">
	  <br/><br/><br/><br/><br><br><br><br>
      <%--<img src="../image/company-logo/SiteLogo.jpg" width="90" height="90">--%> <!-- FB 1830-->
      <img src="../image/company-logo/SiteLogo.jpg" alt="SiteLogo" style="display:none" /><%--FB 1928--%> <%--width="122" height="44" FB 2407--%><%--ZD 100419--%> <%--ZD 100943--%>
    </td>
    <td width="80%">
      <br>
 <%--CONTENT GOES HERE--%>
    <center>
      <h3><asp:Literal Text="<%$ Resources:WebResources, LicenseAgreement_myVRMEndUser%>" runat="server"></asp:Literal></h3>
    </center>
    <center>
    <%--FB 2337--%>
            <iframe id="ifrmlocation" runat="server" name="ifrmLocation" width="100%" height="380" align="left" valign="top"> <%--ZD 101384--%>
              <p><asp:Literal Text="<%$ Resources:WebResources, LicenseAgreement_goto%>" runat="server"></asp:Literal><a href="../<%=Session["language"] %>/license/licenseAgreement.htm"><asp:Literal Text="<%$ Resources:WebResources, LicenseAgreement_LocationList%>" runat="server"></asp:Literal></a></p>
            </iframe> 

    </center>
    </td>
    </tr>
    </table>
  <form name="frmLicenseagreement" method="post" runat="server" defaultbutton="BtnSubmit">
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
     <input type="hidden" name="f3" value="<%=Request.QueryString ["f3"]%>"/>
    <center>
      <table border="0" cellpadding="3" cellspacing="0" width="80%">
        <tr>
          <td align="center">
            <asp:Button id="BtnReset" runat="server" text="<%$ Resources:WebResources, LicenseAgreement_BtnReset%>" onclick="RejectLicense" cssclass="altMedium0BlueButtonFormat" width="150px"></asp:Button> <%--FB 1336 --%>
          </td>
          <td align="center">
             <asp:Button id="BtnSubmit" runat="server" text="<%$ Resources:WebResources, LicenseAgreement_BtnSubmit%>" onclick="AcceptLicense" cssclass="altMedium0BlueButtonFormat"></asp:Button>
          </td>
        </tr>
      </table>
    </center>
  </form>


<!----------------------------------------->

     

<script Language="JavaScript">
<!--

	document.body.style.background = "";
	document.body.style.margin = "0px";
	document.body.style.bgcolor = "#fff7e7";

//-->
</script>

</body>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>