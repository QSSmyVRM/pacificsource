/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.IO;
using System.Xml;
using System.Data;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web;

namespace CustomizationUtil
{
    /// <summary>
    /// This class is used to generate the styles, menu and the artifacts.
    /// </summary>

    public partial class CSSReplacementUtility
    {
        #region Private members and methods
        /// <summary>
        /// Private variables/members declaration
        /// </summary>
        private String applicationPath = "";
        private myVRMNet.ImageUtil imageUtil = null;
        private String mirrorCssXmlPath = "";
        private String tempCssXmlPath = "";
        private String bestCssXmlPath = "";
        private String originalCssXmlPath = "";

        private String mirrorCssPath = "";
        private String tempCssPath = "";
        private String bestCssPath = "";
        private String originalCssPath = "";

        private String mirrorJSPath = "";
        private String tempJSPath = "";
        private String bestJSPath = "";
        private String originalJSPath = "";
        //code Added by Gopi on 26-Jul-2006
        private String tempJSPath1 = "";
        //End

        private String mirrorJSXmlPath = "";
        private String tempJSXmlPath = "";
        private String bestJSXmlPath = "";
        private String originalJSXmlPath = "";

        private String mirrorBannerPath1600 = "";
        private String tempBannerPath1600 = "";
        private String bestBannerPath1600 = "";
        private String originalBannerPath1600 = "";

        private String mirrorBannerPath1024 = "";
        private String tempBannerPath1024 = "";
        private String bestBannerPath1024 = "";
        private String originalBannerPath1024 = "";

        private String mirrorLogoPath = "";
        private String tempLogoPath = "";
        private String bestLogoPath = "";
        private String originalLogoPath = "";

        private String menuTemplatePath = "";
        private String menuTemplatePath1 = "";

        private XmlDocument xmlDocument = null;
        private XmlNodeList xmlNodeList = null;
        private XmlNodeList colorNodeList = null;
        private XmlNodeList fontNodeList = null;
        private XmlNodeList titleNodeList = null;
        private XmlNodeList subTitleNodeList = null;
        private XmlNodeList buttonNodeList = null;
        private XmlNodeList tableheaderNodeList = null;
        private XmlNodeList errormessageNodeList = null;
        private XmlNodeList inputtextNodeList = null;
        //Window Dressing
        private XmlNodeList tablebodyNodeList = null;
        private XmlNodeList themeNodeList = null;

        //1428
        private String mirrorTxtXmlPath = "";
        private String tempTxtXmlPath = "";
        private String bestTxtXmlPath = "";
        private String originalTxtXmlPath = "";
        //1428

        //Code added for FB 1473 - start
        private String themeName = "";
        private String themeCssXmlPath = "";
        private String themeCssPath = "";
        private String themeJSPath = "";
        private String themeJSXmlPath = "";
        private String themeBannerPath1600 = "";
        private String themeBannerPath1024 = "";
        private String themeLogoPath = "";
        //Code added for FB 1473 - end

        private CSSReplacementUtility cssUtil = null;
        private String folderName = "";
        private String errXML = ""; //FB 1881
        
        #endregion

        #region Method to Retrieve All Styles Path From WDConfig.xml
        /// <summary>
        /// Method to read the WDConfig.xml in the application path and sets the 
        /// class member with respective mirror /temp /original /Best paths.
        /// </summary>
        /// <returns type="void" >nothing</returns>
        public void GetFilePath()
        {
            String configPath = "";
            XmlNodeList mirrorXml = null;
            XmlNodeList bestXml = null;
            XmlNodeList originalXml = null;
            XmlNodeList tempXml = null;
            //code added for FB 1473
            XmlNodeList themeXml = null;
            XmlNodeList cssFilePathXml = null;  //Organization/CSS Module

            XmlDocument xDocument = null;
            try
            {
                if (applicationPath == "")
                {
                    //throw new Exception("Application Path Error");
                    throw new Exception();//FB 1881
                    //log.Trace("GetFilePath:Application Path Error");//Fb 1881
                }
                configPath = applicationPath + "\\WDConfig.xml";

                if (!File.Exists(configPath))
                {
                    throw new FileNotFoundException("WDConfig.xml file does not exist", configPath);
                }

                xDocument = new XmlDocument();
                xDocument.Load(@configPath);

                //Method changed for Organization/CSS Module --- Start
                String cssOrgPath = "";
                String orgName = "";
                String cssPath = "";

                if(xDocument.SelectNodes("//Location//MainFolder") != null)
                    cssOrgPath  = xDocument.SelectSingleNode("//Location/MainFolder").InnerText.Trim()+"\\";
               
                if(HttpContext.Current.Session["organizationID"] != null)
                    orgName = "Org_" + HttpContext.Current.Session["organizationID"].ToString();

                if (folderName != "")
                {
                    orgName = folderName;
                }
                cssPath = cssOrgPath + orgName + "\\";

                mirrorXml = xDocument.GetElementsByTagName("Mirror");
                bestXml = xDocument.GetElementsByTagName("Best");
                originalXml = xDocument.GetElementsByTagName("Original");
                tempXml = xDocument.GetElementsByTagName("Temp");
                //code added for FB 1473
                themeXml = xDocument.GetElementsByTagName("Theme");

                colorNodeList = xDocument.GetElementsByTagName("BGOptions");
                colorNodeList = colorNodeList.Item(0).ChildNodes;

                fontNodeList = xDocument.GetElementsByTagName("GeneralStyles");
                fontNodeList = fontNodeList.Item(0).ChildNodes;

                titleNodeList = xDocument.GetElementsByTagName("TitleStyles");
                titleNodeList = titleNodeList.Item(0).ChildNodes;

                subTitleNodeList = xDocument.GetElementsByTagName("SubTitleStyles");
                subTitleNodeList = subTitleNodeList.Item(0).ChildNodes;

                buttonNodeList = xDocument.GetElementsByTagName("ButtonStyles");
                buttonNodeList = buttonNodeList.Item(0).ChildNodes;

                tableheaderNodeList = xDocument.GetElementsByTagName("TableHeaderStyles");
                tableheaderNodeList = tableheaderNodeList.Item(0).ChildNodes;

                errormessageNodeList = xDocument.GetElementsByTagName("ErrorMessageStyles");
                errormessageNodeList = errormessageNodeList.Item(0).ChildNodes;

                inputtextNodeList = xDocument.GetElementsByTagName("InputTextStyles");
                inputtextNodeList = inputtextNodeList.Item(0).ChildNodes;

                //Window Dressing
                tablebodyNodeList = xDocument.GetElementsByTagName("TableBodyStyles");
                tablebodyNodeList = tablebodyNodeList.Item(0).ChildNodes;

                themeNodeList = xDocument.GetElementsByTagName("ThemeName");
                themeNodeList = themeNodeList.Item(0).ChildNodes;

                if (mirrorXml.Count <= 0)
                    throw new XmlException("Mirror location node is null or invalid");

                foreach (XmlNode xnode in mirrorXml)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        switch (xnod.LocalName)
                        {
                            case "Banner1024":
                                {
                                    mirrorBannerPath1024 = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Banner1600":
                                {
                                    mirrorBannerPath1600 = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Logo":
                                {
                                    mirrorLogoPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Javascript":
                                {
                                    mirrorJSPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Styles":
                                {
                                    mirrorCssPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "CssXmlVersion":
                                {
                                    mirrorCssXmlPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "MenuXmlVersion":
                                {
                                    mirrorJSXmlPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "TextXmlVersion": //FB 1428
                                {
                                    mirrorTxtXmlPath = cssPath + xnod.InnerText;
                                    break;
                                }
                        }
                    }
                }

                if (bestXml.Count <= 0)
                    throw new XmlException("Best location node is null or invalid");

                foreach (XmlNode xnode in bestXml)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        switch (xnod.LocalName)
                        {
                            case "Banner1024":
                                {
                                    bestBannerPath1024 = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Banner1600":
                                {
                                    bestBannerPath1600 = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Logo":
                                {
                                    bestLogoPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Javascript":
                                {
                                    bestJSPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Styles":
                                {
                                    bestCssPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "CssXmlVersion":
                                {
                                    bestCssXmlPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "MenuXmlVersion":
                                {
                                    bestJSXmlPath = cssPath + xnod.InnerText;
                                    break;
                                }
                        }
                    }
                }

                if (originalXml.Count <= 0)
                    throw new XmlException("Original path node is null or invalid");

                foreach (XmlNode xnode in originalXml)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        switch (xnod.LocalName)
                        {
                            case "Banner1024":
                                {
                                    originalBannerPath1024 = cssOrgPath + xnod.InnerText;
                                    break;
                                }
                            case "Banner1600":
                                {
                                    originalBannerPath1600 = cssOrgPath + xnod.InnerText;
                                    break;
                                }
                            case "Logo":
                                {
                                    originalLogoPath = cssOrgPath + xnod.InnerText;
                                    break;
                                }
                            case "Javascript":
                                {
                                    originalJSPath = cssOrgPath + xnod.InnerText;
                                    break;
                                }
                            case "Styles":
                                {
                                    originalCssPath = cssOrgPath + xnod.InnerText;
                                    break;
                                }
                            case "CssXmlVersion":
                                {
                                    originalCssXmlPath = cssOrgPath + xnod.InnerText;
                                    break;
                                }
                            case "MenuXmlVersion":
                                {
                                    originalJSXmlPath = cssOrgPath + xnod.InnerText;
                                    break;
                                }
                            case "MenuTemplate":
                                {
                                    menuTemplatePath = cssOrgPath + xnod.InnerText;
                                    break;
                                }
                            case "MenuTemplateNewVersion":
                                {
                                    menuTemplatePath1 = cssOrgPath + xnod.InnerText;
                                    break;
                                }
                            case "TextXmlVersion"://FB 1428
                                {
                                    originalTxtXmlPath = cssOrgPath + xnod.InnerText;
                                    break;
                                }
                        }
                    }
                }

                if (tempXml.Count <= 0)
                    throw new XmlException("Temp location node is null or invalid");

                foreach (XmlNode xnode in tempXml)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        switch (xnod.LocalName)
                        {
                            case "Banner1024":
                                {
                                    tempBannerPath1024 = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Banner1600":
                                {
                                    tempBannerPath1600 = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Logo":
                                {
                                    tempLogoPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Javascript":
                                {
                                    tempJSPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "JavascriptNewVersion":
                                {
                                    tempJSPath1 = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "Styles":
                                {
                                    tempCssPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "CssXmlVersion":
                                {
                                    tempCssXmlPath = cssPath + xnod.InnerText;
                                    break;
                                }
                            case "MenuXmlVersion":
                                {
                                    tempJSXmlPath = cssPath + xnod.InnerText;
                                    break;
                                }
                        }
                    }
                }

                //code added for FB 1473 - start
                if (themeXml.Count <= 0)
                    throw new XmlException("Temp location node is null or invalid");

                foreach (XmlNode xnode in themeXml)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        switch (xnod.LocalName)
                        {
                            case "Banner1024":
                                {
                                    themeBannerPath1024 = cssOrgPath + themeName + "\\" + xnod.InnerText;
                                    break;
                                }
                            case "Banner1600":
                                {
                                    themeBannerPath1600 = cssOrgPath + themeName + "\\" + xnod.InnerText;
                                    break;
                                }
                            case "Logo":
                                {
                                    themeLogoPath = cssOrgPath + themeName + "\\" + xnod.InnerText;
                                    break;
                                }
                            case "Javascript":
                                {
                                    themeJSPath = cssOrgPath + themeName + "\\" + xnod.InnerText;
                                    break;
                                }
                            case "JavascriptNewVersion":
                                {
                                    themeJSPath = cssOrgPath + themeName + "\\" + xnod.InnerText;
                                    break;
                                }
                            case "Styles":
                                {
                                    themeCssPath = cssOrgPath + themeName + "\\" + xnod.InnerText;
                                    break;
                                }
                            case "CssXmlVersion":
                                {
                                    themeCssXmlPath = cssOrgPath + themeName + "\\" + xnod.InnerText;
                                    break;
                                }
                            case "MenuXmlVersion":
                                {
                                    themeJSXmlPath = cssOrgPath + themeName + "\\" + xnod.InnerText;
                                    break;
                                }
                        }
                    }
                }
                //code added for FB 1473 - end
                //Method changed for Organization/CSS Module --- End

                mirrorXml = null;
                bestXml = null;
                originalXml = null;
                tempXml = null;

                //code added for FB 1473
                themeXml = null;

                xDocument = null;
            }
            catch (FileLoadException fa)
            {
                throw fa;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods To Return Xml Version File Name For CSS

        #region Method To Return Xml Version File Name For CSS
        /// <summary>
        /// Method to return file name of the style sheet xml version.
        /// It returns the xml version in the Mirror path. If no such file
        /// in the mirror location, it will return the xml in the Original location.
        /// </summary>
        /// <returns type="String" >xml file name</returns>
        private string CSSXMLVersion()
        {
            String xmlFileName = "";

            if (mirrorCssPath == "")
                GetFilePath();

            xmlFileName = applicationPath + "\\" + mirrorCssXmlPath;

            if (!File.Exists(xmlFileName))
            {
                xmlFileName = applicationPath + "\\" + originalCssXmlPath;
            }
            return xmlFileName;
        }
        #endregion

        #region Method To Return CSSXmlversion File Name In Mirror
        /// <summary>
        /// Method to return file name of the style sheet xml version.
        /// It returns the xml version in the Mirror path. If no such file
        /// in the mirror location, it will return the xml in the Original location.
        /// </summary>
        /// <returns type="String" >xml file name</returns>
        private string MirrorCssXmlVersion()
        {
            String xmlFileName = "";

            if (mirrorCssPath == "")
                GetFilePath();

            xmlFileName = applicationPath + "\\" + mirrorCssXmlPath;

            if (!File.Exists(xmlFileName))
                OriginalToMirrorLocation();

            return xmlFileName;
        }
        #endregion

        #region Method To Retrieve CSSXmlversion File Name In Temp
        /// <summary>
        /// Method to return file name of the style sheet xml version.
        /// It returns the xml version in the Temp path.
        /// </summary>
        /// <returns type="String" >xml file name</returns>
        private string TempCssXmlVersion()
        {
            String xmlFileName = "";

            if (tempCssPath == "")
                GetFilePath();

            xmlFileName = applicationPath + "\\" + tempCssXmlPath;

            //if(!File.Exists(xmlFileName))
            //	MirrorToTempLocation();

            return xmlFileName;
        }
        #endregion

        #endregion

        #region Methods To Return Xml File Name For Menu

        #region Method To Return Xml File Name For Menu - Mirror Location
        /// <summary>
        /// Method to return the xml version of the menu file.
        /// It returns the xml version in the Mirror path. If no such file
        /// in the mirror location, it will return the xml in the Original location.
        /// </summary>
        /// <returns type="String" >menu xml file name</returns>
        private string MenuXMLVersion()
        {
            String xmlFileName = "";

            if (mirrorJSPath == "")
                GetFilePath();

            xmlFileName = applicationPath + "\\" + mirrorJSXmlPath;

            if (!File.Exists(xmlFileName))
            {
                xmlFileName = applicationPath + "\\" + originalJSXmlPath;
            }

            return xmlFileName;
        }
        #endregion

        #region Method To Return Xml File Name For Menu - Temp Location
        /// <summary>
        /// Method to return the xml version of the menu file in the Temp Location.
        /// </summary>
        /// <returns type="String" >menu xml file name</returns>
        private string TempMenuXMLVersion()
        {
            String xmlFileName = "";

            if (tempJSPath == "")
                GetFilePath();

            xmlFileName = applicationPath + "\\" + tempJSXmlPath;

            return xmlFileName;
        }
        #endregion

        #endregion

        #region Method To Return Javascript Template File Name For Menu
        /// <summary>
        /// Method to return the template version of the menu file.
        /// It returns the javascript in the Mirror path. If no such file
        /// in the mirror location, it will return the template in the Original location.
        /// </summary>
        /// <returns type="String" >menu template/JS file name</returns>
        private string MenuTemplateVersion()
        {
            String menuTemplateFile = "";

            if (mirrorJSPath == "" || menuTemplatePath == "")
                GetFilePath();

            /*menuTemplateFile = applicationPath + "\\" + mirrorJSPath;
		
            if(!File.Exists(menuTemplateFile))
            {
                menuTemplateFile = applicationPath + "\\" + menuTemplatePath;
            }
            */
            menuTemplateFile = applicationPath + "\\" + menuTemplatePath;

            return menuTemplateFile;
        }

        private string MenuTemplateVersion1()
        {
            String menuTemplateFile1 = "";

            if (mirrorJSPath == "" || menuTemplatePath1 == "")
                GetFilePath();

            menuTemplateFile1 = applicationPath + "\\" + menuTemplatePath1;

            return menuTemplateFile1;
        }

        #endregion

        #region Method To Read the existing styles from the CSS XML version
        /// <summary>
        /// Method read the existing styles from the CSS XML version in Mirror location. 
        /// </summary>
        /// <param name="CSSFileName" >CSS File name</param>
        /// <returns type="Hashtable">existing styles</returns>
        private Hashtable RetrieveCSSStyles(String CSSFileName)
        {
            String tclassName = "";
            String stclassName = "";
            String btclassName = "";
            String tblhclassName = "";
            String emclassname = "";
            Hashtable htStyles = null;

            if (CSSFileName == "")
                return htStyles;

            xmlDocument = new XmlDocument();
            try
            {
                xmlDocument.Load(CSSFileName);

                htStyles = new Hashtable();

                if (colorNodeList.Count > 0)
                    tclassName = colorNodeList.Item(0).Attributes.Item(0).Value;

                colorNodeList = xmlDocument.SelectNodes("/classNames/classname[@Name='" + tclassName + "' ]");
                foreach (XmlNode xnode in colorNodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        if (xnod.LocalName.ToUpper() == "BACKGROUND-COLOR")
                        {
                            htStyles.Add("NewBGColor", xnod.InnerText);
                            break;
                        }
                    }
                }

                if (fontNodeList.Count > 0)
                    tclassName = fontNodeList.Item(0).Attributes.Item(0).Value;

                fontNodeList = xmlDocument.SelectNodes("/classNames/classname[@Name='" + tclassName + "' ]");
                foreach (XmlNode xnode in fontNodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        switch (xnod.LocalName.ToUpper())
                        {
                            case "FONT-FAMILY":
                                {
                                    htStyles.Add("NewFontName", xnod.InnerText);
                                    break;
                                }
                            case "FONT-SIZE":
                                {
                                    htStyles.Add("NewFontSize", xnod.InnerText);
                                    break;
                                }
                            case "COLOR":
                                {
                                    htStyles.Add("NewFontColor", xnod.InnerText);
                                    break;
                                }
                        }
                    }
                }

                if (titleNodeList.Count > 0)
                    tclassName = titleNodeList.Item(0).Attributes.Item(0).Value;

                titleNodeList = xmlDocument.SelectNodes("/classNames/classname[@Name='" + tclassName + "' ]");

                foreach (XmlNode xnode in titleNodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        switch (xnod.LocalName.ToUpper())
                        {
                            case "FONT-FAMILY":
                                {
                                    htStyles.Add("TitleFontName", xnod.InnerText);
                                    break;
                                }
                            case "FONT-SIZE":
                                {
                                    htStyles.Add("TitleFontSize", xnod.InnerText);
                                    break;
                                }
                            case "COLOR":
                                {
                                    htStyles.Add("TitleFontColor", xnod.InnerText);
                                    break;
                                }
                        }
                    }
                }

                if (subTitleNodeList.Count > 0)
                    stclassName = subTitleNodeList.Item(0).Attributes.Item(0).Value;

                subTitleNodeList = xmlDocument.SelectNodes("/classNames/classname[@Name='" + stclassName + "' ]");
                foreach (XmlNode xnode in subTitleNodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        switch (xnod.LocalName.ToUpper())
                        {
                            case "FONT-FAMILY":
                                {
                                    htStyles.Add("SubTitleFontName", xnod.InnerText);
                                    break;
                                }
                            case "FONT-SIZE":
                                {
                                    htStyles.Add("SubTitleFontSize", xnod.InnerText);
                                    break;
                                }
                            case "COLOR":
                                {
                                    htStyles.Add("SubTitleFontColor", xnod.InnerText);
                                    break;
                                }
                        }
                    }
                }

                if (buttonNodeList.Count > 0)
                    btclassName = buttonNodeList.Item(0).Attributes.Item(0).Value;

                buttonNodeList = xmlDocument.SelectNodes("/classNames/classname[@Name='" + btclassName + "' ]");

                foreach (XmlNode xnode in buttonNodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        switch (xnod.LocalName.ToUpper())
                        {
                            case "FONT-FAMILY":
                                {
                                    htStyles.Add("ButtonFontName", xnod.InnerText);
                                    break;
                                }
                            case "FONT-SIZE":
                                {
                                    htStyles.Add("ButtonFontSize", xnod.InnerText);
                                    break;
                                }
                            case "COLOR":
                                {
                                    htStyles.Add("ButtonFontColor", xnod.InnerText);
                                    break;
                                }
                            case "BACKGROUND-COLOR":
                                {
                                    htStyles.Add("ButtonBackColor", xnod.InnerText);
                                    break;
                                }
                        }
                    }
                }

                if (tableheaderNodeList.Count > 0)
                    tblhclassName = tableheaderNodeList.Item(0).Attributes.Item(0).Value;

                tableheaderNodeList = xmlDocument.SelectNodes("/classNames/classname[@Name='" + tblhclassName + "' ]");

                foreach (XmlNode xnode in tableheaderNodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        //HttpContext.Current.Response.Write("<br>" + xnod.LocalName);
                        switch (xnod.LocalName.ToUpper())
                        {
                            case "BACKGROUND-COLOR":
                                {
                                    //                                    HttpContext.Current.Response.Write(" in bgcolor ");
                                    htStyles.Add("TableHeaderBackColor", xnod.InnerText);
                                    break;
                                }
                            case "FONT-WEIGHT":
                                {
                                    htStyles.Add("TableHeaderFontWeight", xnod.InnerText);
                                    break;
                                }
                            case "COLOR":
                                {
                                    htStyles.Add("TableHeaderForeColor", xnod.InnerText);
                                    //                                    HttpContext.Current.Response.Write(" in color: " + htStyles["TableHeaderForeColor"].ToString());

                                    break;
                                }
                            case "BORDER-COLOR":
                                {
                                    htStyles.Add("TableHeaderBorderColor", xnod.InnerText);
                                    break;
                                }
                            case "BORDER-BOTTOM":
                                {
                                    htStyles.Add("TableHeaderBorderBottom", xnod.InnerText);
                                    break;
                                }
                            case "BORDER-LEFT":
                                {
                                    htStyles.Add("TableHeaderBorderLeft", xnod.InnerText);
                                    break;
                                }
                            case "BORDER-RIGHT":
                                {
                                    htStyles.Add("TableHeaderBorderRight", xnod.InnerText);
                                    break;
                                }
                            case "BORDER-TOP":
                                {
                                    htStyles.Add("TableHeaderBorderTop", xnod.InnerText);
                                    break;
                                }
                        }
                    }
                }
                if (errormessageNodeList.Count > 0)
                    emclassname = errormessageNodeList.Item(0).Attributes.Item(0).Value;

                errormessageNodeList = xmlDocument.SelectNodes("/classNames/classname[@Name='" + emclassname + "' ]");

                foreach (XmlNode xnode in errormessageNodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        //HttpContext.Current.Response.Write("<br>" + xnod.LocalName);
                        switch (xnod.LocalName.ToUpper())
                        {
                            case "BACKGROUND-COLOR":
                                {
                                    htStyles.Add("ErrorMessageBackColor", xnod.InnerText);
                                    break;
                                }
                            case "FONT-WEIGHT":
                                {
                                    htStyles.Add("ErrorMessageFontWeight", xnod.InnerText);
                                    break;
                                }
                            case "PADDING":
                                {
                                    htStyles.Add("ErrorMessagePadding", xnod.InnerText);
                                    //                                    HttpContext.Current.Response.Write(" in color: " + htStyles["TableHeaderForeColor"].ToString());

                                    break;
                                }
                            case "COLOR":
                                {
                                    htStyles.Add("ErrorMessageForeColor", xnod.InnerText);
                                    break;
                                }
                            case "FONT-SIZE":
                                {
                                    //HttpContext.Current.Response.Write(xnod.InnerText + " : " + htStyles["ErrorMessageFontSize"].ToString());
                                    htStyles.Add("ErrorMessageFontSize", xnod.InnerText);
                                    break;
                                }
                            case "BORDER":
                                {
                                    htStyles.Add("ErrorMessageBorder", xnod.InnerText);
                                    break;
                                }
                        }
                    }
                }

                if (inputtextNodeList.Count > 0)
                    emclassname = inputtextNodeList.Item(0).Attributes.Item(0).Value;

                inputtextNodeList = xmlDocument.SelectNodes("/classNames/classname[@Name='" + emclassname + "' ]");

                foreach (XmlNode xnode in inputtextNodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        //HttpContext.Current.Response.Write("<br>aa" + xnod.LocalName);
                        switch (xnod.LocalName.ToUpper())
                        {
                            case "BACKGROUND-COLOR":
                                {
                                    htStyles.Add("InputTextBackColor", xnod.InnerText);
                                    break;
                                }
                            case "FONT-FAMILY":
                                {
                                    htStyles.Add("InputTextFontName", xnod.InnerText);
                                    break;
                                }
                            case "COLOR":
                                {
                                    htStyles.Add("InputTextForeColor", xnod.InnerText);
                                    break;
                                }
                            case "BORDER-COLOR":
                                {
                                    htStyles.Add("InputTextBorderColor", xnod.InnerText);
                                    break;
                                }
                            case "FONT-SIZE":
                                {
                                    htStyles.Add("InputTextFontSize", xnod.InnerText);
                                    break;
                                }
                            /*case "BORDER-BOTTOM":
                                {
                                    htStyles.Add("InputTextBorderColor", xnod.InnerText);
                                    break;
                                }
                            case "BORDER-LEFT":
                                {
                                    htStyles.Add("InputTextBorderLeft", xnod.InnerText);
                                    break;
                                }
                            case "BORDER-RIGHT":
                                {
                                    htStyles.Add("InputTextBorderRight", xnod.InnerText);
                                    break;
                                }
                            case "BORDER-TOP":
                                {
                                    htStyles.Add("InputTextBorderTop", xnod.InnerText);
                                    break;
                                }*/
                        }
                    }
                }

                //Window Dressind - start
                if (tablebodyNodeList.Count > 0)
                    tblhclassName = tablebodyNodeList.Item(0).Attributes.Item(0).Value;

                tablebodyNodeList = xmlDocument.SelectNodes("/classNames/classname[@Name='" + tblhclassName + "' ]");

                foreach (XmlNode xnode in tablebodyNodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        //HttpContext.Current.Response.Write("<br>" + xnod.LocalName);
                        switch (xnod.LocalName.ToUpper())
                        {
                            case "BACKGROUND-COLOR":
                                {
                                    // HttpContext.Current.Response.Write(" in bgcolor ");
                                    htStyles.Add("TableBodyBackColor", xnod.InnerText);
                                    break;
                                }
                            case "COLOR":
                                {
                                    htStyles.Add("TableBodyForeColor", xnod.InnerText);
                                    // HttpContext.Current.Response.Write(" in color: " + htStyles["TableHeaderForeColor"].ToString());

                                    break;
                                }
                        }
                    }
                }

                if (themeNodeList.Count > 0)
                    tblhclassName = themeNodeList.Item(0).Attributes.Item(0).Value;

                themeNodeList = xmlDocument.SelectNodes("/classNames/classname[@Name='" + tblhclassName + "' ]");

                foreach (XmlNode xnode in themeNodeList)
                {
                    foreach (XmlNode xnod in xnode.ChildNodes)
                    {
                        //HttpContext.Current.Response.Write("<br>" + xnod.LocalName);
                        switch (xnod.LocalName.ToUpper())
                        {
                            case "NAME":
                                {
                                    // HttpContext.Current.Response.Write(" in bgcolor ");
                                    htStyles.Add("ThemeName", xnod.InnerText);
                                    break;
                                }
                        }
                    }
                }

                //Window Dressind - end

                //HttpContext.Current.Response.Write("4");

                xmlDocument = null;
                return htStyles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method To Read the existing styles from the Menu XML version
        /// <summary>
        /// Method to read the existing styles from the Menu XML version in Mirror location. 
        /// </summary>
        /// <param name="menuFileName" >Menu File name</param>
        /// <returns type="Hashtable">existing styles</returns>
        private Hashtable RetrieveMenuStyles(String menuFileName, Hashtable htStyles)
        {
            String menuStyle = "";
            String dropDownStyle = "";
            XmlTextReader readXML = null;

            if (menuFileName == "")
                return htStyles;

            try
            {
                if (!File.Exists(menuFileName))
                {
                    throw new FileNotFoundException("Menu Xml version does not exist", menuFileName);
                }

                readXML = new XmlTextReader(@menuFileName);
                while (readXML.Read())
                {
                    if (readXML.LocalName == "style1_2" && readXML.IsStartElement())
                    {
                        menuStyle = readXML.ReadInnerXml();
                        readXML.Skip();
                    }

                    if (readXML.LocalName == "style2_2" && readXML.IsStartElement())
                    {
                        dropDownStyle = readXML.ReadInnerXml();
                        readXML.Skip();
                    }
                }
                readXML.Close();

                if (menuStyle == "")
                    throw new XmlException("Menu styles node is null or invalid");

                readXML = new XmlTextReader(menuStyle, XmlNodeType.Element, null);

                if (htStyles == null)
                    htStyles = new Hashtable();

                while (readXML.Read())
                {
                    if (readXML.LocalName == "MouseOffFont" && readXML.IsStartElement())
                        htStyles.Add("MMouseOffFont", readXML.ReadElementString());

                    if (readXML.LocalName == "Mouseoffbackground" && readXML.IsStartElement())
                        htStyles.Add("MMouseoffbackground", readXML.ReadElementString());

                    if (readXML.LocalName == "Mouseonfont" && readXML.IsStartElement())
                        htStyles.Add("MMouseonfont", readXML.ReadElementString());

                    if (readXML.LocalName == "Mouseonbackground" && readXML.IsStartElement())
                        htStyles.Add("MMouseonbackground", readXML.ReadElementString());

                    if (readXML.LocalName == "Menuborder" && readXML.IsStartElement())
                        htStyles.Add("MMenuborder", readXML.ReadElementString());

                    if (readXML.LocalName == "Menuheaderfontcolor" && readXML.IsStartElement())
                        htStyles.Add("MMenuheaderfontcolor", readXML.ReadElementString());

                    if (readXML.LocalName == "Menuheaderbackgroundcolor" && readXML.IsStartElement())
                        htStyles.Add("MMenuheaderbackgroundcolor", readXML.ReadElementString());
                }
                readXML.Close();

                if (dropDownStyle == "")
                    throw new XmlException("Menu Dropdown styles node is null or invalid");

                readXML = new XmlTextReader(dropDownStyle, XmlNodeType.Element, null);

                while (readXML.Read())
                {
                    if (readXML.LocalName == "MouseOffFont" && readXML.IsStartElement())
                        htStyles.Add("DMouseOffFont", readXML.ReadElementString());

                    if (readXML.LocalName == "Mouseoffbackground" && readXML.IsStartElement())
                        htStyles.Add("DMouseoffbackground", readXML.ReadElementString());

                    if (readXML.LocalName == "Mouseonfont" && readXML.IsStartElement())
                        htStyles.Add("DMouseonfont", readXML.ReadElementString());

                    if (readXML.LocalName == "Mouseonbackground" && readXML.IsStartElement())
                        htStyles.Add("DMouseonbackground", readXML.ReadElementString());

                    if (readXML.LocalName == "Menuborder" && readXML.IsStartElement())
                        htStyles.Add("DMenuborder", readXML.ReadElementString());

                    if (readXML.LocalName == "Menuheaderfontcolor" && readXML.IsStartElement())
                        htStyles.Add("DMenuheaderfontcolor", readXML.ReadElementString());

                    if (readXML.LocalName == "Menuheaderbackgroundcolor" && readXML.IsStartElement())
                        htStyles.Add("DMenuheaderbackgroundcolor", readXML.ReadElementString());
                }
                readXML.Close();

                return htStyles;
            }
            catch (Exception ex)
            {
                throw new FileLoadException("Permission Denied: ", ex.InnerException);
                //throw ex;
            }
        }
        #endregion

        #region Method to Get the Style Tag Name
        /// <summary>
        /// Method to return the CSS property used as tag(node) name in the CSS xml version
        /// </summary>
        /// <param name="styleType">Denotes whether Font/Color</param>
        /// <returns type="string">xml tag name</returns>
        private String GetStyleTagName(String styleType)
        {
            if (styleType.ToUpper() == "F")
            {
                return "FONT-FAMILY";
            }
            else if (styleType.ToUpper() == "C")
            {
                return "BACKGROUND-COLOR";
            }
            else if (styleType.ToUpper() == "FS")
            {
                return "FONT-SIZE";
            }
            else if (styleType.ToUpper() == "FC")
            {
                return "COLOR";
            }
            else
            {
                return "";
            }
        }
        #endregion

        #region Method to Generate CSS for the Xml
        /// <summary>
        /// Method to read the given xml document and generate a css file in the 
        /// given location.(1 of 2 overloads)
        /// </summary>
        /// <param name="xmlDoc" type="XmlDocument">Xml Version of CSS</param>
        /// <param name="cssStorePath" type="String">file path to save CSS</param>
        /// <returns type="Boolean">True or False</returns>
        private Boolean GenerateCSSFromXML(XmlDocument xmlDoc, String cssStorePath)
        {
            StreamWriter writer = null;
            try
            {
                if (xmlDoc == null)
                {
                    throw new ArgumentNullException("xmlDoc", "Xml document is null");
                }

                if (cssStorePath == "")
                {
                    throw new ArgumentNullException("cssStorePath", "Save file path is empty");
                }

                xmlNodeList = null;
                xmlNodeList = xmlDoc.GetElementsByTagName("classname");

                if (xmlNodeList.Count <= 0)
                {
                    throw new ArgumentException("Xml document is not valid one", "xmlDoc");
                }
                writer = new StreamWriter(@cssStorePath, false, System.Text.Encoding.UTF8);

                foreach (XmlNode xnode in xmlNodeList)
                {
                    writer.Write(xnode.Attributes.Item(0).Value);
                    writer.Write(" {");
                    writer.WriteLine("");

                    foreach (XmlNode xcnode in xnode.ChildNodes)
                    {
                        string tempVar = "";
                        if (xcnode == xnode.LastChild)
                        {
                            tempVar = xcnode.Name.ToUpper() + ": " + xcnode.InnerText;
                        }
                        else
                        {
                            tempVar = xcnode.Name.ToUpper() + ": " + xcnode.InnerText + "; ";
                        }

                        writer.Write(tempVar);
                    }
                    writer.WriteLine("");
                    writer.WriteLine("}");
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                writer.Flush();
                writer.Close();
            }
        }
        #endregion

        #region Method To Write Xml version of Menu styles
        /// <summary>
        /// Method To Write Xml version of Menu styles in the temp location
        /// </summary>
        /// <param name="hashTable">Hashtable collection of menu styles</param>
        /// <returns type="Dataset">Artifacts xml as dataset</returns>
        private DataSet GenerateMenuStyleXml(Hashtable hashTable)
        {
            String menuXmlPath = "";
            String tempXmlPath = "";
            DataSet dset = null;
            try
            {
                if (hashTable == null)
                    throw new ArgumentNullException("hashTable", "Argument hash table is null or invalid");

                if (applicationPath == "" || originalJSXmlPath == "" || mirrorJSXmlPath == "")
                    GetFilePath();

                menuXmlPath = MenuXMLVersion();
                tempXmlPath = applicationPath + "\\" + tempJSXmlPath;

                dset = new DataSet();
                DataRow row = null;

                dset.ReadXml(menuXmlPath);
                row = dset.Tables[0].Rows[0];

                if (hashTable.ContainsKey("MMouseOffFont"))
                    dset.Tables[1].Rows[0]["MouseOffFont"] = hashTable["MMouseOffFont"];

                if (hashTable.ContainsKey("DMouseOffFont"))
                    dset.Tables[2].Rows[0]["MouseOffFont"] = hashTable["DMouseOffFont"];

                if (hashTable.ContainsKey("MMouseoffbackground"))
                    dset.Tables[1].Rows[0]["Mouseoffbackground"] = hashTable["MMouseoffbackground"];

                if (hashTable.ContainsKey("DMouseoffbackground"))
                    dset.Tables[2].Rows[0]["Mouseoffbackground"] = hashTable["DMouseoffbackground"];

                if (hashTable.ContainsKey("MMouseonfont"))
                    dset.Tables[1].Rows[0]["Mouseonfont"] = hashTable["MMouseonfont"];

                if (hashTable.ContainsKey("DMouseonfont"))
                    dset.Tables[2].Rows[0]["Mouseonfont"] = hashTable["DMouseonfont"];

                if (hashTable.ContainsKey("MMouseonbackground"))
                    dset.Tables[1].Rows[0]["Mouseonbackground"] = hashTable["MMouseonbackground"];

                if (hashTable.ContainsKey("DMouseonbackground"))
                    dset.Tables[2].Rows[0]["Mouseonbackground"] = hashTable["DMouseonbackground"];

                if (hashTable.ContainsKey("MMenuborder"))
                    dset.Tables[1].Rows[0]["Menuborder"] = hashTable["MMenuborder"];

                if (hashTable.ContainsKey("DMenuborder"))
                    dset.Tables[2].Rows[0]["Menuborder"] = hashTable["DMenuborder"];

                if (hashTable.ContainsKey("MMenuheaderfontcolor"))
                    dset.Tables[1].Rows[0]["Menuheaderfontcolor"] = hashTable["MMenuheaderfontcolor"];

                if (hashTable.ContainsKey("DMenuheaderfontcolor"))
                    dset.Tables[2].Rows[0]["Menuheaderfontcolor"] = hashTable["DMenuheaderfontcolor"];

                if (hashTable.ContainsKey("MMenuheaderbackgroundcolor"))
                    dset.Tables[1].Rows[0]["Menuheaderbackgroundcolor"] = hashTable["MMenuheaderbackgroundcolor"];

                if (hashTable.ContainsKey("DMenuheaderbackgroundcolor"))
                    dset.Tables[2].Rows[0]["Menuheaderbackgroundcolor"] = hashTable["DMenuheaderbackgroundcolor"];

                dset.WriteXml(tempXmlPath);

                return dset;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        //code added for FB 1473 
        #region Method to Copy the Theme configuaration into Temp
        /// <summary>
        /// Method to copy the CSS, Css-xml, Menu, menu-xml, Company logo
        /// and banner from original into temp location.
        /// </summary>
        /// <returns type="void")>null</returns>
        public bool ThemeAsPreview()//ZD 100263
        {
            try
            {
                if (themeJSPath == "" || tempCssPath == "" || tempJSPath == "" || tempBannerPath1024 == "" || tempBannerPath1600 == "" || themeCssPath == "")
                    GetFilePath();

                String themeFileName = applicationPath + "\\" + themeCssPath;
                String tempFileName = applicationPath + "\\" + tempCssPath;

                // Quick fix for infocomm starts
                HttpContext.Current.Session.Add("sourceCssPath", themeFileName);
                // Quick fix for infocomm ends

                //To move CSS and its Xml
                if (File.Exists(themeFileName))
                    File.Copy(themeFileName, tempFileName, true);

                themeFileName = applicationPath + "\\" + themeCssXmlPath;
                tempFileName = applicationPath + "\\" + tempCssXmlPath;

                if (File.Exists(themeFileName))
                    File.Copy(themeFileName, tempFileName, true);

                //To move Menu - JS and its Xml
                themeFileName = applicationPath + "\\" + themeJSPath;
                tempFileName = applicationPath + "\\" + tempJSPath;


                // Quick fix for infocomm starts
                HttpContext.Current.Session.Add("sourceJsPath", themeFileName);
                // Quick fix for infocomm ends

                if (File.Exists(themeFileName))
                    File.Copy(themeFileName, tempFileName, true);

                themeFileName = applicationPath + "\\" + themeJSXmlPath;
                tempFileName = applicationPath + "\\" + tempJSXmlPath;

                if (File.Exists(themeFileName))
                    File.Copy(themeFileName, tempFileName, true);

                ////To move the banner
                //themeFileName = applicationPath + "\\" + themeBannerPath1024;
                //tempFileName = applicationPath + "\\" + tempBannerPath1024;

                //if (File.Exists(themeFileName))
                //    File.Copy(themeFileName, tempFileName, true);

                //themeFileName = applicationPath + "\\" + themeBannerPath1600;
                //tempFileName = applicationPath + "\\" + tempBannerPath1600;

                //if (File.Exists(themeFileName))
                //    File.Copy(themeFileName, tempFileName, true);

                ////To move the logo
                //themeFileName = applicationPath + "\\" + themeLogoPath;
                //tempFileName = applicationPath + "\\" + tempLogoPath;

                //if (File.Exists(themeFileName))
                //    File.Copy(themeFileName, tempFileName, true);
                return true;//ZD 100263
            }
            catch (FileNotFoundException fx)
            {
                return false;//ZD 100263
                //throw fx;
            }
            catch (Exception ex)
            {
                return false;//ZD 100263
                //throw ex;
            }
        }

        #endregion

        #region Public Member and Methods
        /// <summary>
        /// Default constructor region
        /// </summary>
        public CSSReplacementUtility()
        {
            //
            // TODO: Add constructor logic here
            //
            errXML = "A system error has occurred. Please contact your myVRM system administrator and give them the following error code.";//ZD 102432
            if (HttpContext.Current.Session["SystemError"] != null)
                if (HttpContext.Current.Session["SystemError"].ToString() != "")
                    errXML = HttpContext.Current.Session["SystemError"].ToString();
        }

        #region Public Property Definition To Private Member
        /// <summary>
        /// Public Property definition to private member - application path
        /// </summary>
        public String ApplicationPath
        {
            get
            {
                return (applicationPath);
            }
            set
            {
                if (value != "")
                {
                    applicationPath = value;
                }
            }
        }

        //code added for FB 1473
        public String ThemeName
        {
            get
            {
                return (themeName);
            }
            set
            {
                if (value != "")
                {
                    themeName = value;
                }
            }
        }
        //code added for Organization\CSS Module
        public String FolderName
        {
            get
            {
                return (folderName);
            }
            set
            {
                if (value != "")
                {
                    folderName = value;
                }
            }
        }
        #endregion

        #region Method to Generate CSS for the Xml
        /// <summary>
        /// Method to read the given xml and generate a css file in the 
        /// given location.
        /// </summary>
        /// <param name="xmlFilePath">original XML file path</param>
        /// <param name="cssStorePath">css path</param>
        /// <returns type="Boolean">True/False</returns>
        public Boolean GenerateCSSFromXML(String xmlFilePath, String cssStorePath)
        {
            StreamWriter writer = null;
            try
            {
                if (xmlFilePath == null)
                {
                    throw new ArgumentNullException("xmlFilePath", "Xml file path is empty or null");
                }

                if (cssStorePath == "")
                {
                    throw new ArgumentNullException("cssStorePath", "Save file path is empty");
                }

                xmlDocument = new XmlDocument();
                xmlDocument.Load(xmlFilePath);

                xmlNodeList = null;
                xmlNodeList = xmlDocument.GetElementsByTagName("classname");

                if (xmlNodeList.Count <= 0)
                {
                    throw new ArgumentException("Xml document is not valid one", "xmlDocument");
                }

                writer = new StreamWriter(@cssStorePath, false, System.Text.Encoding.UTF8);

                foreach (XmlNode xnode in xmlNodeList)
                {
                    writer.Write(xnode.Attributes.Item(0).Value);
                    writer.Write(" {");
                    writer.WriteLine("");

                    foreach (XmlNode xcnode in xnode.ChildNodes)
                    {
                        string tempVar = "";
                        if (xcnode == xnode.LastChild)
                        {
                            tempVar = xcnode.Name.ToUpper() + ": " + xcnode.InnerText;
                        }
                        else
                        {
                            tempVar = xcnode.Name.ToUpper() + ": " + xcnode.InnerText + "; ";
                        }
                        writer.Write(tempVar);
                    }
                    writer.WriteLine("");
                    writer.WriteLine("}");
                }
                xmlDocument = null;

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                writer.Flush();
                writer.Close();
            }
        }
        #endregion

        #region Methods To Read Existing CSS Styles

        #region Method To Read the existing styles from the Mirror Location.
        /// <summary>
        /// Method read the existing styles from the XML version in Mirror location. 
        /// </summary>
        /// <returns type="Hashtable">existing styles</returns>
        public Hashtable RetrieveMirrorStyles()
        {
            String xmlFileName = "";
            Hashtable htStyles = null;
            try
            {
                xmlFileName = MirrorCssXmlVersion();

                if (xmlFileName == "")
                    return htStyles;

                htStyles = RetrieveCSSStyles(xmlFileName);

                xmlFileName = "";
                xmlFileName = MenuXMLVersion();

                if (xmlFileName == "")
                    return htStyles;

                htStyles = RetrieveMenuStyles(xmlFileName, htStyles);

                return htStyles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method To Read the existing styles from the Temp Location.
        /// <summary>
        /// Method read the existing styles from the XML version in Temp location. 
        /// </summary>
        /// <returns type="Hashtable">existing styles</returns>
        public Hashtable RetrieveTempStyles(ref int errid)//ZD 100263
        {
            String xmlFileName = "";
            Hashtable htStyles = null;
            try
            {
                xmlFileName = TempCssXmlVersion();

                if (xmlFileName == "")
                    return htStyles;

                htStyles = RetrieveCSSStyles(xmlFileName);

                xmlFileName = "";
                xmlFileName = TempMenuXMLVersion();

                if (xmlFileName == "")
                    return htStyles;

                htStyles = RetrieveMenuStyles(xmlFileName, htStyles);

                return htStyles;
            }
            catch (Exception ex)
            {
                errid = 1;//ZD 100263
                throw ex;
            }
        }
        #endregion

        #endregion

        #region Method to Replace the CSS style values
        /// <summary>
        /// Method to set the font family /background color of the css file.
        /// The changed version of the css and xml are saved in the Temp location.
        /// </summary>
        /// <param name="htStyles" type="Hashtable">style properties</param>
        /// <returns type="void">Nothing</returns>
        public void ReplaceStyles(Hashtable htStyles, ref int errid)//ZD 100263
        {
            String saveFilePath = "";
            try
            {
                if (htStyles == null)
                    throw new Exception("Arguments cannot be empty");

                if (tempCssPath == "")
                    GetFilePath();

                if (xmlDocument == null)
                {
                    xmlDocument = new XmlDocument();
                    xmlDocument.Load(CSSXMLVersion());
                }

                foreach (XmlNode xnode in colorNodeList)
                {
                    String tclassname = String.Empty;
                    tclassname = xnode.Attributes.Item(0).Value.ToString();
                    XmlNodeList xnod = null;

                    xnod = xmlDocument.SelectNodes("/classNames/classname[@Name='" + tclassname + "' ]");

                    foreach (XmlNode nod in xnod)
                    {
                        foreach (XmlElement element in nod)
                        {
                            if (element.Name.ToUpper() == "BACKGROUND-COLOR")
                                element.InnerText = htStyles["NewBGColor"].ToString();
                        }
                    }
                }

                foreach (XmlNode xnode in fontNodeList)
                {
                    String tclassname = String.Empty;
                    tclassname = xnode.Attributes.Item(0).Value.ToString();
                    XmlNodeList xnod = null;

                    xnod = xmlDocument.SelectNodes("/classNames/classname[@Name='" + tclassname + "' ]");

                    foreach (XmlNode nod in xnod)
                    {
                        foreach (XmlElement element in nod)
                        {
                            if (element.Name.ToUpper() == "FONT-SIZE")
                                element.InnerText = htStyles["NewFontSize"].ToString() + "pt";

                            if (element.Name.ToUpper() == "COLOR")
                                element.InnerText = htStyles["NewFontColor"].ToString();

                            if (element.Name.ToUpper() == "FONT-FAMILY")
                                element.InnerText = htStyles["NewFontName"].ToString();
                        }
                    }
                }

                foreach (XmlNode xnode in titleNodeList)
                {
                    String tclassname = String.Empty;
                    tclassname = xnode.Attributes.Item(0).Value.ToString();
                    XmlNodeList xnod = null;

                    xnod = xmlDocument.SelectNodes("/classNames/classname[@Name='" + tclassname + "' ]");

                    foreach (XmlNode nod in xnod)
                    {
                        foreach (XmlElement element in nod)
                        {
                            if (element.Name.ToUpper() == "FONT-SIZE")
                                element.InnerText = htStyles["TitleFontSize"].ToString() + "pt";

                            if (element.Name.ToUpper() == "COLOR")
                                element.InnerText = htStyles["TitleFontColor"].ToString();

                            if (element.Name.ToUpper() == "FONT-FAMILY")
                                element.InnerText = htStyles["TitleFontName"].ToString();
                        }
                    }
                }

                foreach (XmlNode xnode in subTitleNodeList)
                {
                    String stclassname = String.Empty;
                    stclassname = xnode.Attributes.Item(0).Value.ToString();
                    XmlNodeList xnod = null;

                    xnod = xmlDocument.SelectNodes("/classNames/classname[@Name='" + stclassname + "' ]");

                    foreach (XmlNode nod in xnod)
                    {
                        foreach (XmlElement element in nod)
                        {
                            if (element.Name.ToUpper() == "FONT-SIZE")
                                element.InnerText = htStyles["SubTitleFontSize"].ToString() + "pt";

                            if (element.Name.ToUpper() == "COLOR")
                                element.InnerText = htStyles["SubTitleFontColor"].ToString();

                            if (element.Name.ToUpper() == "FONT-FAMILY")
                                element.InnerText = htStyles["SubTitleFontName"].ToString();
                        }
                    }
                }

                foreach (XmlNode xnode in buttonNodeList)
                {
                    String bclassname = String.Empty;
                    bclassname = xnode.Attributes.Item(0).Value.ToString();
                    XmlNodeList xnod = null;

                    xnod = xmlDocument.SelectNodes("/classNames/classname[@Name='" + bclassname + "' ]");

                    foreach (XmlNode nod in xnod)
                    {
                        foreach (XmlElement element in nod)
                        {
                            if (element.Name.ToUpper() == "FONT-FAMILY")
                                element.InnerText = htStyles["ButtonFontName"].ToString();

                            if (element.Name.ToUpper() == "FONT-SIZE")
                                element.InnerText = htStyles["ButtonFontSize"].ToString() + "pt";

                            if (element.Name.ToUpper() == "COLOR")
                                element.InnerText = htStyles["ButtonFontColor"].ToString();

                            if (element.Name.ToUpper() == "BACKGROUND-COLOR")
                                element.InnerText = htStyles["ButtonBackColor"].ToString();
                        }
                    }
                }
                foreach (XmlNode xnode in tableheaderNodeList)
                {
                    String bclassname = String.Empty;
                    bclassname = xnode.Attributes.Item(0).Value.ToString();
                    XmlNodeList xnod = null;

                    xnod = xmlDocument.SelectNodes("/classNames/classname[@Name='" + bclassname + "' ]");

                    foreach (XmlNode nod in xnod)
                    {
                        foreach (XmlElement element in nod)
                        {
                            //HttpContext.Current.Response.Write("<br>" + element.Name);

                            if (element.Name.ToUpper() == "BACKGROUND-COLOR")
                                element.InnerText = htStyles["TableHeaderBackColor"].ToString();

                            if (element.Name.ToUpper() == "FONT-WEIGHT")
                                element.InnerText = element.InnerText; // htStyles["TableHeaderFontWeight"].ToString() + "pt";

                            if (element.Name.ToUpper() == "COLOR")
                                element.InnerText = htStyles["TableHeaderForeColor"].ToString();

                            if (element.Name.ToUpper() == "BORDER-COLOR")
                                element.InnerText = htStyles["TableHeaderBackColor"].ToString(); // htStyles["TableHeaderBorderColor"].ToString();

                            if (element.Name.ToUpper() == "BORDER-BOTTOM")
                                element.InnerText = htStyles["TableHeaderBackColor"].ToString() + " 1px solid"; // htStyles["TableHeaderBorderBottom"].ToString();

                            if (element.Name.ToUpper() == "BORDER-LEFT")
                                element.InnerText = htStyles["TableHeaderBackColor"].ToString() + " 1px solid"; // htStyles["TableHeaderBorderLeft"].ToString();

                            if (element.Name.ToUpper() == "BORDER-RIGHT")
                                element.InnerText = htStyles["TableHeaderBackColor"].ToString() + " 1px solid"; // htStyles["TableHeaderBorderRight"].ToString();

                            if (element.Name.ToUpper() == "BORDER-TOP")
                                element.InnerText = htStyles["TableHeaderBackColor"].ToString() + " 1px solid"; // htStyles["TableHeaderBorderTop"].ToString();
                        }
                    }
                }

                foreach (XmlNode xnode in errormessageNodeList)
                {
                    String emclassname = String.Empty;
                    emclassname = xnode.Attributes.Item(0).Value.ToString();
                    XmlNodeList xnod = null;

                    xnod = xmlDocument.SelectNodes("/classNames/classname[@Name='" + emclassname + "' ]");

                    foreach (XmlNode nod in xnod)
                    {
                        foreach (XmlElement element in nod)
                        {
                            //HttpContext.Current.Response.Write("<br>" + element.Name);

                            if (element.Name.ToUpper() == "BACKGROUND-COLOR")
                                element.InnerText = htStyles["ErrorMessageBackColor"].ToString();

                            if (element.Name.ToUpper() == "FONT-WEIGHT")
                                element.InnerText = element.InnerText; // htStyles["ErrorMessageFontWeight"].ToString();

                            if (element.Name.ToUpper() == "FONT-SIZE")
                                element.InnerText = htStyles["ErrorMessageFontSize"].ToString();

                            if (element.Name.ToUpper() == "COLOR")
                                element.InnerText = htStyles["ErrorMessageForeColor"].ToString();

                            if (element.Name.ToUpper() == "PADDING")
                                element.InnerText = element.InnerText; // htStyles["ErrorMessagePadding"].ToString();

                            if (element.Name.ToUpper() == "BORDER")
                                element.InnerText = element.InnerText;
                        }
                    }
                }

                foreach (XmlNode xnode in inputtextNodeList)
                {
                    String emclassname = String.Empty;
                    emclassname = xnode.Attributes.Item(0).Value.ToString();
                    XmlNodeList xnod = null;
                    //HttpContext.Current.Response.Write(emclassname);
                    xnod = xmlDocument.SelectNodes("/classNames/classname[@Name='" + emclassname + "' ]");
                    foreach (XmlNode nod in xnod)
                    {
                        foreach (XmlElement element in nod)
                        {
                            if (element.Name.ToUpper() == "BACKGROUND-COLOR")
                                element.InnerText = htStyles["InputTextBackColor"].ToString();

                            if (element.Name.ToUpper() == "FONT-FAMILY")
                                element.InnerText = htStyles["InputTextFontName"].ToString(); //element.InnerText   //Organization Css Module

                            if (element.Name.ToUpper() == "FONT-SIZE")
                                element.InnerText = htStyles["InputTextFontSize"].ToString();

                            if (element.Name.ToUpper() == "COLOR")
                                element.InnerText = htStyles["InputTextForeColor"].ToString();

                            if (element.Name.ToUpper() == "BORDER-COLOR")
                                element.InnerText = htStyles["InputTextBorderColor"].ToString();

                            if (element.Name.ToUpper() == "BORDER-BOTTOM")
                                element.InnerText = element.InnerText.Replace(element.InnerText.Split(' ')[0], htStyles["InputTextBorderColor"].ToString());

                            if (element.Name.ToUpper() == "BORDER-LEFT")
                                element.InnerText = element.InnerText.Replace(element.InnerText.Split(' ')[0], htStyles["InputTextBorderColor"].ToString()); // htStyles["InputTextBorderColor"].ToString() +" 2px solid"; // +

                            if (element.Name.ToUpper() == "BORDER-RIGHT")
                                element.InnerText = element.InnerText.Replace(element.InnerText.Split(' ')[0], htStyles["InputTextBorderColor"].ToString()); //" htStyles["InputTextBorderColor"].ToString() + 2px solid"; // +

                            if (element.Name.ToUpper() == "BORDER-TOP")
                                element.InnerText = element.InnerText.Replace(element.InnerText.Split(' ')[0], htStyles["InputTextBorderColor"].ToString()); // htStyles["InputTextBorderColor"].ToString() + + " 2px solid";
                        }
                    }
                }

                //Window Dressing - start
                foreach (XmlNode xnode in tablebodyNodeList)
                {
                    String bclassname = String.Empty;
                    bclassname = xnode.Attributes.Item(0).Value.ToString();
                    XmlNodeList xnod = null;

                    xnod = xmlDocument.SelectNodes("/classNames/classname[@Name='" + bclassname + "' ]");

                    foreach (XmlNode nod in xnod)
                    {
                        foreach (XmlElement element in nod)
                        {
                            //HttpContext.Current.Response.Write("<br>" + element.Name);

                            if (element.Name.ToUpper() == "BACKGROUND-COLOR")
                                element.InnerText = htStyles["TableBodyBackColor"].ToString();

                            if (element.Name.ToUpper() == "COLOR")
                                element.InnerText = htStyles["TableBodyForeColor"].ToString();
                        }
                    }
                }

                foreach (XmlNode xnode in themeNodeList)
                {
                    String bclassname = String.Empty;
                    bclassname = xnode.Attributes.Item(0).Value.ToString();
                    XmlNodeList xnod = null;

                    xnod = xmlDocument.SelectNodes("/classNames/classname[@Name='" + bclassname + "' ]");

                    foreach (XmlNode nod in xnod)
                    {
                        foreach (XmlElement element in nod)
                        {
                            //HttpContext.Current.Response.Write("<br>" + element.Name);

                            if (element.Name.ToUpper() == "NAME")
                                element.InnerText = htStyles["ThemeName"].ToString();

                        }
                    }
                }
                //Window Dressing - end

                //HttpContext.Current.Response.Write("3");

                saveFilePath = applicationPath + "\\" + tempCssPath;

                if (GenerateCSSFromXML(xmlDocument, saveFilePath))
                {
                    saveFilePath = applicationPath + "\\" + tempCssXmlPath;
                    xmlDocument.Save(saveFilePath);
                }
                xmlNodeList = null;
                titleNodeList = null;
                colorNodeList = null;
                fontNodeList = null;
                subTitleNodeList = null;
                buttonNodeList = null;
                xmlDocument = null;
            }
            catch (FileLoadException fx)
            {
                errid = 1;//ZD 100263
                //throw fx;
            }
            catch (Exception ex)
            {
                errid = 1;//ZD 100263
                //throw ex;
            }
        }
        #endregion

        #region Methods to Move the Previewed Configuration into Mirror Location
        /// <summary>
        /// Method to move the previewed CSS, Css-xml, Menu, menu-xml, Company logo
        /// and banner in the temp location to the mirror.
        /// </summary>
        /// <returns type="void")>null</returns>
        public bool TempToMirrorLocation()//ZD 100263
        {
            if (tempCssPath == "" || mirrorCssPath == "" || mirrorJSPath == "" || mirrorBannerPath1024 == "" || mirrorBannerPath1600 == "")
                GetFilePath();

            String mirrorFileName = applicationPath + "\\" + mirrorCssPath;
            String tempFileName = applicationPath + "\\" + tempCssPath;

            try
            {	//To move CSS and its Xml
                if (File.Exists(mirrorFileName))
                {
                    File.Delete(mirrorFileName);
                }
                if (File.Exists(tempFileName))
                    File.Move(tempFileName, mirrorFileName);

                mirrorFileName = applicationPath + "\\" + mirrorCssXmlPath;
                tempFileName = applicationPath + "\\" + tempCssXmlPath;

                if (File.Exists(mirrorFileName))
                {
                    File.Delete(mirrorFileName);
                }
                if (File.Exists(tempFileName))
                    File.Move(tempFileName, mirrorFileName);

                //To move menu and its xml
                mirrorFileName = applicationPath + "\\" + mirrorJSPath;
                tempFileName = applicationPath + "\\" + tempJSPath;

                if (File.Exists(mirrorFileName))
                {
                    File.Delete(mirrorFileName);
                }
                if (File.Exists(tempFileName))
                    File.Move(tempFileName, mirrorFileName);

                mirrorFileName = applicationPath + "\\" + mirrorJSXmlPath;
                tempFileName = applicationPath + "\\" + tempJSXmlPath;

                if (File.Exists(mirrorFileName))
                {
                    File.Delete(mirrorFileName);
                }
                if (File.Exists(tempFileName))
                    File.Move(tempFileName, mirrorFileName);

                //To move the banner
                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1024;
                tempFileName = applicationPath + "\\" + tempBannerPath1024;

                if (File.Exists(mirrorFileName))
                {
                    File.Delete(mirrorFileName);
                }
                if (File.Exists(tempFileName))
                    File.Move(tempFileName, mirrorFileName);

                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1600;
                tempFileName = applicationPath + "\\" + tempBannerPath1600;

                if (File.Exists(mirrorFileName))
                {
                    File.Delete(mirrorFileName);
                }
                if (File.Exists(tempFileName))
                    File.Move(tempFileName, mirrorFileName);

                //To move the logo
                mirrorFileName = applicationPath + "\\" + mirrorLogoPath;
                tempFileName = applicationPath + "\\" + tempLogoPath;

                if (File.Exists(mirrorFileName))
                {
                    File.Delete(mirrorFileName);
                }
                if (File.Exists(tempFileName))
                    File.Move(tempFileName, mirrorFileName);

                // Quick fix for infocomm starts
                if (HttpContext.Current.Session["sourceJsPath"] != null)
                {
                    tempFileName = HttpContext.Current.Session["sourceJsPath"].ToString();
                    mirrorFileName = applicationPath + "\\" + mirrorJSPath;
                    if (File.Exists(tempFileName))
                        File.Copy(tempFileName, mirrorFileName, true);
                }

                if (HttpContext.Current.Session["sourceCssPath"] != null)
                {
                    tempFileName = HttpContext.Current.Session["sourceCssPath"].ToString();
                    mirrorFileName = applicationPath + "\\" + mirrorCssPath;
                    if (File.Exists(tempFileName))
                        File.Copy(tempFileName, mirrorFileName, true);
                    HttpContext.Current.Session.Remove("sourceCssPath");
                }
                // Quick fix for infocomm ends
                return true;//ZD 100263
            }
            catch (FileNotFoundException fx)
            {
                return false;//ZD 100263
                throw fx;
            }
            catch (FieldAccessException fa)
            {
                return false;//ZD 100263
                throw new FieldAccessException("Please set the permissions on Best, Mirror, Temp and Original Folders");
            }
            catch (Exception ex)
            {
                return false;//ZD 100263
                throw ex;
            }
        }
        #endregion

        #region Methods to Copy Mirror Configuration into Temp Location
        /// <summary>
        /// Method to copy the CSS, Css-xml, Menu, menu-xml, Company logo
        /// and banner from mirror / original intotemp location.
        /// </summary>
        /// <returns type="void")>null</returns>
        public bool MirrorToTempLocation()
        {
            try
            {
                if (tempCssPath == "" || mirrorCssPath == "" || mirrorJSPath == "" || mirrorBannerPath1024 == "" || mirrorBannerPath1600 == "")
                    GetFilePath();

                String mirrorFileName = applicationPath + "\\" + mirrorCssPath;
                String tempFileName = applicationPath + "\\" + tempCssPath;

                //To move CSS and its Xml
                if (!File.Exists(mirrorFileName))
                    OriginalToMirrorLocation();

                if (File.Exists(mirrorFileName))
                    File.Copy(mirrorFileName, tempFileName, true);

                mirrorFileName = applicationPath + "\\" + mirrorCssXmlPath;
                tempFileName = applicationPath + "\\" + tempCssXmlPath;

                if (File.Exists(mirrorFileName))
                    File.Copy(mirrorFileName, tempFileName, true);

                //To move Menu - JS and its Xml
                mirrorFileName = applicationPath + "\\" + mirrorJSPath;
                tempFileName = applicationPath + "\\" + tempJSPath;

                if (File.Exists(mirrorFileName))
                    File.Copy(mirrorFileName, tempFileName, true);

                mirrorFileName = applicationPath + "\\" + mirrorJSXmlPath;
                tempFileName = applicationPath + "\\" + tempJSXmlPath;

                if (File.Exists(mirrorFileName))
                    File.Copy(mirrorFileName, tempFileName, true);

                //To move the banner
                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1600;
                tempFileName = applicationPath + "\\" + tempBannerPath1600;

                if (File.Exists(mirrorFileName))
                    File.Copy(mirrorFileName, tempFileName, true);

                //HttpContext.Current.Response.Write(mirrorFileName);
                //HttpContext.Current.Response.Write(tempFileName);

                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1024;
                tempFileName = applicationPath + "\\" + tempBannerPath1024;

                if (File.Exists(mirrorFileName))
                    File.Copy(mirrorFileName, tempFileName, true);

                //To move the logo
                mirrorFileName = applicationPath + "\\" + mirrorLogoPath;
                tempFileName = applicationPath + "\\" + tempLogoPath;

                if (File.Exists(mirrorFileName))
                    File.Copy(mirrorFileName, tempFileName, true);
                return true;
            }
            //            catch (FileNotFoundException fx)
            //            {
            ////                throw new FileNotFoundException("Please check permissions.");
            //                return false;
            //            }
            //            catch (FieldAccessException fa)
            //            {
            ////                throw new FieldAccessException("Please set the permissions on Best, Mirror, Temp and Original Folders");
            //                return false;
            //            }
            catch (Exception ex)
            {
                //                throw ex;
                return false;
            }
        }
        #endregion

        #region Method to Copy Default configuaration into Mirror Location
        /// <summary>
        /// Method to copy the CSS, Css-xml, Menu, menu-xml, Company logo
        /// and banner from original into mirror location.
        /// </summary>
        /// <returns type="void")>null</returns>
        public void OriginalToMirrorLocation()
        {
            try
            {
                if (originalCssPath == "" || mirrorCssPath == "" || mirrorJSPath == "" || mirrorBannerPath1024 == "" || mirrorBannerPath1600 == "" || originalCssPath == "")
                    GetFilePath();
                
                //HttpContext.Current.Response.Write("in original to mirror location");
                String originalFileName = applicationPath + "\\" + originalCssPath;
                String mirrorFileName = applicationPath + "\\" + mirrorCssPath;

                //To move CSS and its Xml

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                // FB 2815 Starts
                originalFileName = originalFileName.Replace("main", "main0");
                mirrorFileName = mirrorFileName.Replace("main", "main0");
                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                originalFileName = originalFileName.Replace("main0", "main1");
                mirrorFileName = mirrorFileName.Replace("main0", "main1");
                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                originalFileName = originalFileName.Replace("main1", "main2");
                mirrorFileName = mirrorFileName.Replace("main1", "main2");
                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);
                // FB 2815 Ends

                originalFileName = applicationPath + "\\" + originalCssXmlPath;
                mirrorFileName = applicationPath + "\\" + mirrorCssXmlPath;

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                //To move Menu - JS and its Xml
                originalFileName = applicationPath + "\\" + originalJSPath;
                mirrorFileName = applicationPath + "\\" + mirrorJSPath;

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                // Quick fix for infocomm starts 
                originalFileName = originalFileName.Replace("menu_array", "menu_arrayExp");
                mirrorFileName = mirrorFileName.Replace("menu_array", "menu_arrayExp");
                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);
                // Quick fix for infocomm Ends

                // FB 2815 Starts
                originalFileName = originalFileName.Replace("menu_arrayExp", "menu_array0");
                mirrorFileName = mirrorFileName.Replace("menu_arrayExp", "menu_array0");
                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                originalFileName = originalFileName.Replace("menu_array0", "menu_array1");
                mirrorFileName = mirrorFileName.Replace("menu_array0", "menu_array1");
                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                originalFileName = originalFileName.Replace("menu_array1", "menu_array2");
                mirrorFileName = mirrorFileName.Replace("menu_array1", "menu_array2");
                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);
                // FB 2815 Ends

                originalFileName = applicationPath + "\\" + originalJSXmlPath;
                mirrorFileName = applicationPath + "\\" + mirrorJSXmlPath;

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                //To move the banner
                originalFileName = applicationPath + "\\" + originalBannerPath1600;
                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1600;

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                originalFileName = applicationPath + "\\" + originalBannerPath1024;
                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1024;

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true); 

                //To move the logo
                originalFileName = applicationPath + "\\" + originalLogoPath;
                mirrorFileName = applicationPath + "\\" + mirrorLogoPath;

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                //code added for Organization\CSS Module

                originalFileName = applicationPath + "\\" + originalTxtXmlPath;
                mirrorFileName = applicationPath + "\\" + mirrorTxtXmlPath;

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, mirrorFileName, true);

                
            }
            catch (FileNotFoundException fx)
            {
                throw fx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method to Copy the Default configuaration into Temp
        /// <summary>
        /// Method to copy the CSS, Css-xml, Menu, menu-xml, Company logo
        /// and banner from original into temp location.
        /// </summary>
        /// <returns type="void")>null</returns>
        public bool DefaultAsPreview()//ZD 100263
        {
            try
            {
                if (originalJSPath == "" || tempCssPath == "" || tempJSPath == "" || tempBannerPath1024 == "" || tempBannerPath1600 == "" || originalCssPath == "")
                    GetFilePath();

                String originalFileName = applicationPath + "\\" + originalCssPath;
                String tempFileName = applicationPath + "\\" + tempCssPath;

                // Quick fix for infocomm starts
                HttpContext.Current.Session.Add("sourceCssPath", originalFileName);
                // Quick fix for infocomm ends

                //To move CSS and its Xml
                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, tempFileName, true);

                originalFileName = applicationPath + "\\" + originalCssXmlPath;
                tempFileName = applicationPath + "\\" + tempCssXmlPath;

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, tempFileName, true);

                //To move Menu - JS and its Xml
                originalFileName = applicationPath + "\\" + originalJSPath;
                tempFileName = applicationPath + "\\" + tempJSPath;

                // Quick fix for infocomm starts
                HttpContext.Current.Session.Add("sourceJsPath", originalFileName);
                // Quick fix for infocomm ends

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, tempFileName, true);

                originalFileName = applicationPath + "\\" + originalJSXmlPath;
                tempFileName = applicationPath + "\\" + tempJSXmlPath;

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, tempFileName, true);

                //To move the banner
                originalFileName = applicationPath + "\\" + originalBannerPath1024;
                tempFileName = applicationPath + "\\" + tempBannerPath1024;

                if (File.Exists(originalFileName) && !File.Exists(tempFileName)) //FB 1633
                    File.Copy(originalFileName, tempFileName, true);

                originalFileName = applicationPath + "\\" + originalBannerPath1600;
                tempFileName = applicationPath + "\\" + tempBannerPath1600;

                if (File.Exists(originalFileName) && !File.Exists(tempFileName)) //FB 1633
                    File.Copy(originalFileName, tempFileName, true);

                //To move the logo
                originalFileName = applicationPath + "\\" + originalLogoPath;
                tempFileName = applicationPath + "\\" + tempLogoPath;

                if (File.Exists(originalFileName))
                    File.Copy(originalFileName, tempFileName, true);
                return true;//ZD 100263
            }
            catch (FileNotFoundException fx)
            {
                return false;//ZD 100263
            }
            catch (Exception ex)
            {
                return false;//ZD 100263
            }
        }
        #endregion

        #region Methods to set the last saved configuaration as current
        /// <summary>
        /// Method to copy the CSS, Css-xml, Menu, menu-xml, Company logo
        /// and banner from best into mirror location.
        /// </summary>
        /// <returns type="void")>null</returns>
        public void LastSavedAsCurrent()
        {
            try
            {
                if (bestJSPath == "" || mirrorCssPath == "" || mirrorJSPath == "" || mirrorBannerPath1024 == "" || mirrorBannerPath1600 == "" || bestCssPath == "")
                    GetFilePath();

                String bestFileName = applicationPath + "\\" + bestCssPath;
                String mirrorFileName = applicationPath + "\\" + mirrorCssPath;

                //To move CSS and its Xml
                if (File.Exists(bestFileName))
                    File.Copy(bestFileName, mirrorFileName, true);

                bestFileName = applicationPath + "\\" + bestCssXmlPath;
                mirrorFileName = applicationPath + "\\" + mirrorCssXmlPath;

                if (File.Exists(bestFileName))
                    File.Copy(bestFileName, mirrorFileName, true);

                //To move Menu - JS and its Xml
                bestFileName = applicationPath + "\\" + bestJSPath;
                mirrorFileName = applicationPath + "\\" + mirrorJSPath;

                if (File.Exists(bestFileName))
                    File.Copy(bestFileName, mirrorFileName, true);

                bestFileName = applicationPath + "\\" + bestJSXmlPath;
                mirrorFileName = applicationPath + "\\" + mirrorJSXmlPath;

                if (File.Exists(bestFileName))
                    File.Copy(bestFileName, mirrorFileName, true);

                //To move the banner
                bestFileName = applicationPath + "\\" + bestBannerPath1024;
                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1024;

                if (File.Exists(bestFileName))
                    File.Copy(bestFileName, mirrorFileName, true);

                bestFileName = applicationPath + "\\" + bestBannerPath1600;
                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1600;

                if (File.Exists(bestFileName))
                    File.Copy(bestFileName, mirrorFileName, true);

                //To move the logo
                bestFileName = applicationPath + "\\" + bestLogoPath;
                mirrorFileName = applicationPath + "\\" + mirrorLogoPath;

                if (File.Exists(bestFileName))
                    File.Copy(bestFileName, mirrorFileName, true);
            }
            catch (FileNotFoundException fx)
            {
                throw fx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method to set the Last Saved Configuration
        /// <summary>
        /// Method to copy the CSS, Css-Xml, Menu, Menu-xml, Artifacts in the 
        /// mirror location to best.
        /// </summary>
        /// <returns type="void")>Nothing</returns>
        public void MoveToBestLocation()
        {
            if (bestCssPath == "" || mirrorCssPath == "" || bestJSPath == "" || bestBannerPath1024 == "" || bestBannerPath1600 == "")
                GetFilePath();

            String mirrorFileName = applicationPath + "\\" + mirrorCssPath;
            String bestFileName = applicationPath + "\\" + bestCssPath;

            try
            {
                if (File.Exists(mirrorFileName))
                {
                    File.Copy(mirrorFileName, bestFileName, true);
                }
                mirrorFileName = applicationPath + "\\" + mirrorCssXmlPath;
                bestFileName = applicationPath + "\\" + bestCssXmlPath;

                if (File.Exists(mirrorFileName))
                {
                    File.Copy(mirrorFileName, bestFileName, true);
                }

                //To copy the menu
                mirrorFileName = applicationPath + "\\" + mirrorJSPath;
                bestFileName = applicationPath + "\\" + bestJSPath;

                if (File.Exists(mirrorFileName))
                {
                    File.Copy(mirrorFileName, bestFileName, true);
                }

                mirrorFileName = applicationPath + "\\" + mirrorJSXmlPath;
                bestFileName = applicationPath + "\\" + bestJSXmlPath;

                if (File.Exists(mirrorFileName))
                {
                    File.Copy(mirrorFileName, bestFileName, true);
                }

                //To copy the artifacts(banner)
                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1024;
                bestFileName = applicationPath + "\\" + bestBannerPath1024;

                if (File.Exists(mirrorFileName))
                {
                    File.Copy(mirrorFileName, bestFileName, true);
                }

                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1600;
                bestFileName = applicationPath + "\\" + bestBannerPath1600;

                if (File.Exists(mirrorFileName))
                {
                    File.Copy(mirrorFileName, bestFileName, true);
                }

                //To copy the artifacts(logo)
                mirrorFileName = applicationPath + "\\" + mirrorLogoPath;
                bestFileName = applicationPath + "\\" + bestLogoPath;

                if (File.Exists(mirrorFileName))
                {
                    File.Copy(mirrorFileName, bestFileName, true);
                }
            }
            catch (FileNotFoundException fx)
            {
                throw fx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method to save the Banner in the Temp location
        /// <summary>
        /// Method to save the new Banner in the Temp location for Preview.
        /// </summary>
        /// <returns type="void")>Nothing</returns>
        public void PostArtifactsBanner(HtmlInputFile fileControl)
        {
            try
            {
                if (fileControl.PostedFile.FileName == "")
                    throw new ArgumentNullException("artifactFilePath", "Banner image name is null or invalid");

                if (tempBannerPath1024 == "")
                    GetFilePath();

                String tempFileName = applicationPath + "\\" + tempBannerPath1024;
                fileControl.PostedFile.SaveAs(tempFileName);

                if (tempBannerPath1600 == "")
                    GetFilePath();

                tempFileName = applicationPath + "\\" + tempBannerPath1600;
                fileControl.PostedFile.SaveAs(tempFileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        //Method added for FB 1633 start
        #region Method to save the BlankBanner in the Temp location
        /// <summary>
        /// Method to save the BlankBanner in the Temp location.
        /// </summary>
        /// <returns type="void")>Nothing</returns>
        public void PostBlankBanner(string blankImgPath)
        {
            try
            {
                if (blankImgPath == null)
                    throw new ArgumentNullException("artifactFilePath", "Blank Banner image name is null or invalid");

                if (tempBannerPath1024 == "")
                    GetFilePath();
                    
                File.Copy(blankImgPath, applicationPath + "\\" + tempBannerPath1024, true);
                if (tempBannerPath1600 == "")
                        GetFilePath();

                File.Copy(blankImgPath, applicationPath + "\\" + tempBannerPath1600, true);
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
		//FB 1633 end

        #region Method to save the Company logo  in the Temp location
        /// <summary>
        /// Method to save the new Company logo in the Temp location for Preview.
        /// </summary>
        /// <returns type="void")>Nothing</returns>
        public void PostArtifactsLogo(HtmlInputFile fileControl)
        {
            try
            {
                if (fileControl.PostedFile.FileName == "")
                    throw new ArgumentNullException("artifactFilePath", "Company logo name is null or invalid");

                if (tempLogoPath == "")
                    GetFilePath();

                String tempFileName = applicationPath + "\\" + tempLogoPath;
                fileControl.PostedFile.SaveAs(tempFileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        //ZD 104387 - start
        #region Method to save the Company Logo in the Temp location
        /// <summary>
        /// Method to save the Company Logo in the Temp location.
        /// </summary>
        /// <returns type="void")>Nothing</returns>
        public void PostBlankCompanyLogo(string blankImgPath)
        {
            try
            {
                if (blankImgPath == null)
                    throw new ArgumentNullException("artifactFilePath", "Company Logo image name is null or invalid");

                if (tempLogoPath == "")
                    GetFilePath();

                File.Copy(blankImgPath, applicationPath + "\\" + tempLogoPath, true);               

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        //ZD 104387 - end

        #region Method to Write the new styles in the javascript menu_array file
        /// <summary>
        /// Method to write the new styles in the javascript menu_array file.
        /// </summary>
        /// <param name="hashTable" type="HashTable">HashTable contains the menu styles</param> 
        /// <returns type="void")>Nothing</returns>
        public void ReplaceMenuStyle(Hashtable hashTable, ref int errid)//ZD 100263
        {
            String menuStr = "";
            String menuStr1 = "";
            String filePath = "";
            String filePath1 = "";
            DataSet dset = null;

            try
            {
                if (hashTable == null)
                    throw new ArgumentNullException("hashTable", "Hashtable collection is null or invalid");

                dset = GenerateMenuStyleXml(hashTable);

                if (dset == null)
                    throw new ArgumentNullException("dset", "Dataset is empty or null");

                if (applicationPath == "" || tempJSPath == "" || tempJSPath1 == "")
                    GetFilePath();

                DataRow menurow = dset.Tables[0].Rows[0];
                DataRow style1row = dset.Tables[1].Rows[0];
                DataRow style2row = dset.Tables[2].Rows[0];

                String strmenu = @"[";
                //Window Dressing
                strmenu = strmenu + "\"" + style1row["MouseOffFont"].ToString() + "\"" + "," + "\"" + style1row["Mouseoffbackground"].ToString() + "\"" + "," + "\"" + style1row["MouseOnFont"].ToString() + "\"" + "," + "\"" + style1row["MouseOnBackground"].ToString() + "\"" + "," + "\"" + style1row["MenuBorder"].ToString() + "\"" + "," + style1row["FontSizeinpixels"].ToString() + "," + "\"" + style1row["FontStyle"].ToString() + "\"" + "," + "\"" + style1row["Fontweight"].ToString() + "\"" + "," + "\"" + style1row["FontName"].ToString() + "\"" + "," + style1row["MenuItemPadding"].ToString() + "," + "\"" + style1row["SubMenuImage"].ToString() + "\"" + "," + "\"" + style1row["BorderSeparatorbar"].ToString() + "\"" + "," + "\"" + style1row["high"].ToString() + "\"" + "," + "\"" + style1row["low"].ToString() + "\"" + "," + "\"" + style1row["MouseOffFont"].ToString() + "\"" + "," + "\"" + style1row["Mouseoffbackground"].ToString() + "\"" + "," + "\"" + style1row["TopBarimage"].ToString() + "\"" + "," + "\"" + style1row["Menuheaderfontcolor"].ToString() + "\"" + "," + "\"" + style1row["Menuheaderbackgroundcolor"].ToString() + "\"";
                strmenu = strmenu + "," + "]";

                String strdrop = @"[";

                //Window Dressing
                strdrop = strdrop + "\"" + style2row["MouseOffFont"].ToString() + "\"" + "," + "\"" + style2row["Mouseoffbackground"].ToString() + "\"" + "," + "\"" + style2row["MouseOnFont"].ToString() + "\"" + "," + "\"" + style2row["MouseOnBackground"].ToString() + "\"" + "," + "\"" + style2row["MenuBorder"].ToString() + "\"" + "," + style2row["FontSizeinpixels"].ToString() + "," + "\"" + style2row["FontStyle"].ToString() + "\"" + "," + "\"" + style2row["Fontweight"].ToString() + "\"" + "," + "\"" + style2row["FontName"].ToString() + "\"" + "," + style2row["MenuItemPadding"].ToString() + "," + "\"" + style2row["SubMenuImage"].ToString() + "\"" + "," + "\"" + style2row["BorderSeparatorbar"].ToString() + "\"" + "," + "\"" + style2row["high"].ToString() + "\"" + "," + "\"" + style2row["low"].ToString() + "\"" + "," + "\"" + style2row["MouseOffFont"].ToString() + "\"" + "," + "\"" + style2row["Mouseoffbackground"].ToString() + "\"" + "," + "\"" + style2row["TopBarimage"].ToString() + "\"" + "," + "\"" + style2row["Menuheaderfontcolor"].ToString() + "\"" + "," + "\"" + style2row["Menuheaderbackgroundcolor"].ToString() + "\"";
                strdrop = strdrop + "," + "]";

                filePath = MenuTemplateVersion();
                filePath1 = MenuTemplateVersion1();
                //HttpContext.Current.Response.Write(filePath.ToString() + " : " + filePath1.ToString());
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite);

                StreamReader strReader = new StreamReader(fs);
                menuStr = strReader.ReadToEnd();

                strReader.Close();
                fs.Close();
                //HttpContext.Current.Response.Write("<br>menustr1 " + "filePath1: " + filePath);
                menuStr = menuStr.Replace("{0}", strmenu);
                menuStr = menuStr.Replace("{1}", strdrop);

                //Code added for FB 1428 - Start
                XmlNodeList nodes = null;
                xmlDocument = new XmlDocument();

                xmlDocument.Load(applicationPath + "\\" + mirrorTxtXmlPath);
                nodes = xmlDocument.SelectNodes("/Pages/Page");

                ReplaceTextInMenu(nodes, ref menuStr);
                //Code added for FB 1428 - End

                String writefilepath = String.Empty;

                writefilepath = applicationPath + "\\" + tempJSPath;

                fs = new FileStream(writefilepath, FileMode.Create, FileAccess.Write);
                StreamWriter strWriter = new StreamWriter(fs);
                strWriter.Write(menuStr);
                strWriter.Close();
                fs.Close();

                //Code Added by Gopi on 26-July-2006

                FileStream fs1 = new FileStream(filePath1, FileMode.Open, FileAccess.ReadWrite);
                StreamReader strReader1 = new StreamReader(fs1);
                menuStr1 = strReader1.ReadToEnd();

                strReader1.Close();
                fs1.Close();
                //HttpContext.Current.Response.Write("<br>menustr1 " + "filePath1: " + filePath1);
                //HttpContext.Current.Response.Write("<br>menustr1 " + "strdrop: " + strdrop);
                menuStr1 = menuStr1.Replace("{0}", strmenu);
                menuStr1 = menuStr1.Replace("{1}", strdrop);

                String writefilepath1 = String.Empty;

                writefilepath1 = applicationPath + "\\" + tempJSPath1;

                fs1 = new FileStream(writefilepath1, FileMode.Create, FileAccess.Write);
                StreamWriter strWriter1 = new StreamWriter(fs1);
                strWriter1.Write(menuStr1);
                strWriter1.Close();
                fs1.Close();
                //End

            }
            catch (Exception ex)
            {
                errid = 1;//ZD 100263
                //throw ex;
            }
        }
        #endregion

        #endregion
        //1428- Start

        #region Method To Return Xml Version File Name For Text
        /// <summary>
        /// Method to return file name of the style sheet xml version.
        /// It returns the xml version in the Mirror path. If no such file
        /// in the mirror location, it will return the xml in the Original location.
        /// </summary>
        /// <returns type="String" >xml file name</returns>
        private string TextXMLVersion()
        {
            String xmlFileName = "";

            if (mirrorTxtXmlPath == "")
                GetFilePath();

            xmlFileName = applicationPath + "\\" + mirrorTxtXmlPath;

            if (!File.Exists(xmlFileName))
            {
                xmlFileName = applicationPath + "\\" + originalTxtXmlPath;
            }
            return xmlFileName;
        }
        #endregion

        #region Replace texts

        public void ReplaceTexts(Hashtable htText)
        {
            String saveFilePath = "";
            try
            {
                if (htText == null)
                    throw new Exception("Arguments cannot be empty");

                if (tempTxtXmlPath == "")
                    GetFilePath();

                DataSet ds = new DataSet();
                DataTable dt = null;

                ds.ReadXml(TextXMLVersion());
                if (ds.Tables.Count > 0)
                {
                    if (htText.Count > 0)
                    {
                        dt = ds.Tables[0];
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (htText.ContainsKey(dr[0].ToString()))
                            {
                                dr["OriginalText"] = dr["NewText"].ToString();
                                dr["NewText"] = htText[dr[0].ToString()];
                            }
                        }
                    }

                    saveFilePath = applicationPath + "\\" + mirrorTxtXmlPath;
                    ds.WriteXml(saveFilePath);

                }
            }
            catch (FileLoadException fx)
            {
                throw fx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region GetTexts

        public DataSet GetTexts()
        {
            DataSet ds = null;
            try
            {
                //if (mirrorTxtXmlPath == "")
                //    GetFilePath();
                ds = new DataSet();
                ds.ReadXml(TextXMLVersion());

                if (ds.Tables.Count > 0)
                {
                    ds.Tables[0].Columns.Add(new DataColumn("CurrentText"));

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        dr["CurrentText"] = dr["NewText"].ToString();
                    }
                }

            }
            catch (FileLoadException fx)
            {
                throw fx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }
        #endregion

        #region GetTextToHash
        public Hashtable GetTextToHash()
        {
            Hashtable ht = null;
            try
            {
                if (mirrorTxtXmlPath == "")
                    GetFilePath();

                DataSet ds = new DataSet();
                DataTable dt = null;
                ds.ReadXml(TextXMLVersion());
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                    ht = new Hashtable();
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (!ht.ContainsKey(dr[0].ToString()))
                            ht.Add(dr[0].ToString(), dr["NewText"].ToString());
                    }
                }
            }
            catch (FileLoadException fx)
            {
                throw fx;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ht;
        }
        #endregion

        #region Replace Menu Text

        //Code Changed for Menu pbm 
        public void ReplaceMenuText(String defaultValue)
        {
            XmlNodeList nodes = null;
            String name = "";
            try
            {
                xmlDocument = new XmlDocument();

                if (mirrorTxtXmlPath == "")
                    GetFilePath();

                xmlDocument.Load(applicationPath + "\\" + mirrorTxtXmlPath);
                nodes = xmlDocument.SelectNodes("/Pages/Page");

                // Code added for Menu pbm - Start

                foreach (XmlNode xnode in nodes)
                    ReplaceTextInPage(xnode, defaultValue);

                String readFilePath = applicationPath + "\\" + menuTemplatePath;

                FileStream fs = new FileStream(readFilePath, FileMode.Open, FileAccess.ReadWrite);

                StreamReader strReader = new StreamReader(fs);
                string menuStr = strReader.ReadToEnd();

                strReader.Close();
                fs.Close();

                ReplaceTextInMenu(nodes, ref menuStr);

                DataSet dset = new DataSet();
                dset.ReadXml(applicationPath + "\\" + mirrorJSXmlPath);

                DataRow menurow = dset.Tables[0].Rows[0];
                DataRow style1row = dset.Tables[1].Rows[0];
                DataRow style2row = dset.Tables[2].Rows[0];

                String strmenu = @"[";
                //Window Dressing
                strmenu = strmenu + "\"" + style1row["MouseOffFont"].ToString() + "\"" + "," + "\"" + style1row["Mouseoffbackground"].ToString() + "\"" + "," + "\"" + style1row["MouseOnFont"].ToString() + "\"" + "," + "\"" + style1row["MouseOnBackground"].ToString() + "\"" + "," + "\"" + style1row["MenuBorder"].ToString() + "\"" + "," + style1row["FontSizeinpixels"].ToString() + "," + "\"" + style1row["FontStyle"].ToString() + "\"" + "," + "\"" + style1row["Fontweight"].ToString() + "\"" + "," + "\"" + style1row["FontName"].ToString() + "\"" + "," + style1row["MenuItemPadding"].ToString() + "," + "\"" + style1row["SubMenuImage"].ToString() + "\"" + "," + "\"" + style1row["BorderSeparatorbar"].ToString() + "\"" + "," + "\"" + style1row["high"].ToString() + "\"" + "," + "\"" + style1row["low"].ToString() + "\"" + "," + "\"" + style1row["MouseOffFont"].ToString() + "\"" + "," + "\"" + style1row["Mouseoffbackground"].ToString() + "\"" + "," + "\"" + style1row["TopBarimage"].ToString() + "\"" + "," + "\"" + style1row["Menuheaderfontcolor"].ToString() + "\"" + "," + "\"" + style1row["Menuheaderbackgroundcolor"].ToString() + "\"";
                strmenu = strmenu + "," + "]";

                String strdrop = @"[";

                //Window Dressing
                strdrop = strdrop + "\"" + style2row["MouseOffFont"].ToString() + "\"" + "," + "\"" + style2row["Mouseoffbackground"].ToString() + "\"" + "," + "\"" + style2row["MouseOnFont"].ToString() + "\"" + "," + "\"" + style2row["MouseOnBackground"].ToString() + "\"" + "," + "\"" + style2row["MenuBorder"].ToString() + "\"" + "," + style2row["FontSizeinpixels"].ToString() + "," + "\"" + style2row["FontStyle"].ToString() + "\"" + "," + "\"" + style2row["Fontweight"].ToString() + "\"" + "," + "\"" + style2row["FontName"].ToString() + "\"" + "," + style2row["MenuItemPadding"].ToString() + "," + "\"" + style2row["SubMenuImage"].ToString() + "\"" + "," + "\"" + style2row["BorderSeparatorbar"].ToString() + "\"" + "," + "\"" + style2row["high"].ToString() + "\"" + "," + "\"" + style2row["low"].ToString() + "\"" + "," + "\"" + style2row["MouseOffFont"].ToString() + "\"" + "," + "\"" + style2row["Mouseoffbackground"].ToString() + "\"" + "," + "\"" + style2row["TopBarimage"].ToString() + "\"" + "," + "\"" + style2row["Menuheaderfontcolor"].ToString() + "\"" + "," + "\"" + style2row["Menuheaderbackgroundcolor"].ToString() + "\"";
                strdrop = strdrop + "," + "]";

                menuStr = menuStr.Replace("{0}", strmenu);
                menuStr = menuStr.Replace("{1}", strdrop);

                String writefilepath = applicationPath + "\\" + mirrorJSPath;

                fs = new FileStream(writefilepath, FileMode.Create, FileAccess.Write);
                StreamWriter strWriter1 = new StreamWriter(fs);
                strWriter1.Write(menuStr);
                strWriter1.Close();
                fs.Close();
                // Code added for Menu pbm - End

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region ReplaceTextInPage

        //Code Changed for Menu pbm - Start
        private void ReplaceTextInPage(XmlNode node, String defaultValue)
        {
            try
            {
                String path = "";

                String type = node.SelectSingleNode("Type").InnerText;
                if (type == "Menu")
                    path = node.SelectSingleNode("Path").InnerText + "" + node.SelectSingleNode("Filename").InnerText;
                else
                    path = node.SelectSingleNode("Filename").InnerText;

                String str = HttpContext.Current.Server.MapPath(path);
                String[] allStr = null;
                String oriText = "";

                //if (str != null)
                //{
                //    if (File.Exists(str) == true)
                //    {
                //        allStr = File.ReadAllLines(str);

                //        for (int i = 0; i < allStr.Length; i++)
                //        {
                //            if (allStr[i].Contains(node.SelectSingleNode("ControlName").InnerText))
                //            {
                //                switch (type)
                //                {
                //                    //Code Changed for Menu pbm - Start
                //                    case "Button":
                //                    case "Login":
                //                        String replaceText = "";
                //                        allStr[i] = allStr[i].Replace("<sup style=\"font-size:x-small;font-weight:bold;\">TM</sup>", "");

                //                        if ((defaultValue == "1" && type == "Login")
                //                            || node.SelectSingleNode("NewText").InnerText.ToLower().StartsWith("saving the planet...one meeting at a time"))
                //                            replaceText = node.SelectSingleNode("NewText").InnerText + "<sup style=\"font-size:x-small;font-weight:bold;\">TM</sup>";
                //                        else
                //                            replaceText = node.SelectSingleNode("NewText").InnerText;

                //                        oriText = allStr[i].Replace(node.SelectSingleNode("OriginalText").InnerText, replaceText).
                //                            Replace(node.SelectSingleNode("OriginalText").InnerText.ToLower(), replaceText);
                //                        //Code Changed for Menu pbm - End
                //                        allStr[i] = oriText;
                //                        break;
                //                    case "Page":
                //                        String[] inStr = null;
                //                        String[] inStr1 = null;
                //                        if (allStr[i].IndexOf(">") > 0)
                //                            inStr = allStr[i].Split('>');

                //                        if (inStr != null)
                //                        {
                //                            if (inStr.Length > 0)
                //                            {
                //                                if (inStr[1].IndexOf("<") >= 0)
                //                                    inStr1 = inStr[1].Split('<');

                //                                if (inStr1 != null)
                //                                {
                //                                    if (inStr.Length > 0 && inStr1.Length > 1)
                //                                        allStr[i] = inStr[0] + ">" + node.SelectSingleNode("NewText").InnerText + "<" + inStr1[1] + ">";
                //                                }
                //                            }
                //                        }
                //                        break;
                //                }
                //            }
                //        }
                //    }

                //    File.WriteAllLines(str, allStr);
                //}

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        //Code added for Menu pbm
        #region ReplaceTextInMenu

        private void ReplaceTextInMenu(XmlNodeList nodes, ref String menuStr)
        {
            try
            {
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("Type").InnerText == "Menu")
                    {
                        String strMenu = node.SelectSingleNode("ControlText").InnerText;
                        String replaceText = node.SelectSingleNode("ControlName").InnerText;

                        strMenu = strMenu.Replace(replaceText, node.SelectSingleNode("NewText").InnerText);

                        menuStr = menuStr.Replace(node.SelectSingleNode("ControlName").InnerText, strMenu);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        //1428- End

        //FB 1473 - start

        #region Remove Images

        public void RemoveImages(String fileName)
        {
            String filePath = "";
            String mirrorFilePath = "";
            try
            {

                GetFilePath();
                if (fileName == "lobbytop1024.jpg")
                {
                    mirrorFilePath = applicationPath + "\\" + mirrorBannerPath1024;
                }
                else if (fileName == "lobbytop1600.jpg")
                {
                    mirrorFilePath = applicationPath + "\\" + mirrorBannerPath1600;
                }
                else if (fileName == "lobby_logo.jpg")
                {
                    mirrorFilePath = applicationPath + "\\"+ mirrorLogoPath;
                }
                
                if (File.Exists(mirrorFilePath))
                {
                    File.Delete(mirrorFilePath);
                }

                if (fileName == "lobby_logo.jpg")
                    filePath = applicationPath + "\\" + "Original\\Image\\lobby_logo_Remove.jpg";
                

                if (File.Exists(filePath))
                    File.Copy(filePath, mirrorFilePath);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        //FB 1473 - end

        //Organization\CSC Module -- Start

        #region GetUITextForControl
        /// <summary>
        /// Get the New UI Text for the given control ID
        /// Page name and controlID will be passed as a param. 
        /// </summary>
        /// <param name="pagename"></param>
        /// <returns></returns>

        public String GetUITextForControl(String pageName, String controlID)
        {
            XmlDocument OrgXMLDoc = null;
            String OrgTextXML = "";
            String newText = "";
            String controlId = "";
            XmlNodeList nodes = null;
            String path = "";
            try
            {
                if(HttpContext.Current.Session["OrgTextXML"] != null)
                    OrgTextXML = HttpContext.Current.Session["OrgTextXML"].ToString();

                OrgXMLDoc = new XmlDocument();
                OrgXMLDoc.LoadXml(OrgTextXML);

                nodes = OrgXMLDoc.SelectNodes("/Pages/Page");
                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        path = node.SelectSingleNode("Filename").InnerText;
                        controlId = node.SelectSingleNode("ControlName").InnerText;

                        if (path.Trim() == pageName.Trim())
                        {
                            if (controlID.Trim() == controlId.Trim())
                            {
                                newText = node.SelectSingleNode("NewText").InnerText;
                                break;
                            }
                        }
                    }
                }
                return newText;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion


        #region Create folder for UI Design on every Organization Creation
        /// <summary>
        /// Create folder for UI Design on every Organization Creation
        /// path - Website\en\Organizations\OrganizationName\CSS\
        /// </summary>
        /// <param name="orgpath"></param>
        /// <returns></returns>
        public bool CreateOrgStyles()
        {
            try
            {
                bool isFileExists = false;
                String mainFolderpath = applicationPath + "\\Organizations"; //Create Organization Folder if not exist.
                CreateNewFolder(mainFolderpath); 

                String orginalPath = applicationPath + "\\Organizations\\Original";
                String orgImgPath = applicationPath + "\\Organizations\\Original\\Image";
                String orgJSPath = applicationPath + "\\Organizations\\Original\\Javascript";
                String orgStylePath = applicationPath + "\\Organizations\\Original\\Styles";

                String cssFolderName = applicationPath + "\\Organizations\\" + folderName;

                if (CreateNewFolder(cssFolderName)) //Create main Folder
                {
                    String cssFolder = cssFolderName + "\\CSS";
                    CreateNewFolder(cssFolder);  //Create sub folder

                    String cssMFolder = cssFolder + "\\Mirror";
                    CreateNewFolder(cssMFolder);  //Create Mirror folder

                    cssFolder = cssMFolder + "\\Styles";
                    CreateNewFolder(cssFolder);  //Create Mirror\Styles folder

                    DirectoryInfo dInfo = new DirectoryInfo(cssFolder);

                    foreach (FileInfo file in dInfo.GetFiles())
                    {
                        if (file.Exists)
                        {
                            isFileExists = true;
                        }
                    }

                    cssFolder = cssMFolder + "\\Javascript";
                    CreateNewFolder(cssFolder);  //Create Mirror\Javascript folder

                    cssFolder = cssMFolder + "\\Image";
                    CreateNewFolder(cssFolder);  //Create Mirror\Image folder

                    if(!isFileExists)
                        OriginalToMirrorLocation(); //Move original folder to Mirror

                    cssFolder = cssFolderName + "\\CSS";
                    CreateNewFolder(cssFolder);  //Create sub folder

                     String cssTFolder = cssFolder + "\\Temp";
                     CreateNewFolder(cssTFolder);  //Create Mirror folder

                    cssFolder = cssTFolder + "\\Styles";
                    CreateNewFolder(cssFolder);  //Create Mirror\Styles folder

                    cssFolder = cssTFolder + "\\Javascript";
                    CreateNewFolder(cssFolder);  //Create Mirror\Javascript folder

                    cssFolder = cssTFolder + "\\Image";
                    CreateNewFolder(cssFolder);  //Create Mirror\Image folder

                    cssFolder = cssFolderName + "\\Rooms";
                    CreateNewFolder(cssFolder);  //Create A folder For rooms 1756

                    cssFolder = cssFolderName + "\\Endpoints"; //FB 2361
                    CreateNewFolder(cssFolder); 
                    
                    if (!isFileExists)
                        MirrorToTempLocation();    //Move Mirror folder to Temp
                }

                return true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Method to create folder if not exist
        /// <summary>
        /// Method to create folder if not exist - Need to send folder name with Savepath
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public bool CreateNewFolder(String foldername)
        {
            try
            {
                if (foldername == "")
                    return false;

                DirectoryInfo dInfo = new DirectoryInfo(foldername);
                if (!dInfo.Exists)
                {
                    dInfo.Create();
                    
                }
               
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method to the set the path to delete all files anf subfolders
        /// <summary>
        ///  Method to delete the set the path to delete all files anf subfolders
        ///  path - en/organizations/orgname/CSS/Image,Javascript,Styles
        /// </summary>
        /// <param name="directoryName"></param>
        /// <returns></returns>
        public bool DeleteDirectoryPath(String directoryName)
        {
            try
            {
                if (directoryName == "")
                    return false;

                String dirPath = applicationPath + "\\Organizations\\" + directoryName;

                DirectoryInfo dInfo = new DirectoryInfo(dirPath);

                 if (dInfo.Exists)
                 {
                     String imgFilePath = applicationPath + "\\Organizations\\" + directoryName + "\\CSS\\Mirror\\Image";
                     DeleteFiles(imgFilePath);

                     String jsFilePath = applicationPath + "\\Organizations\\" + directoryName + "\\CSS\\Mirror\\Javascript";
                     DeleteFiles(jsFilePath);

                     String styleFilePath = applicationPath + "\\Organizations\\" + directoryName + "\\CSS\\Mirror\\Styles";
                     DeleteFiles(styleFilePath);

                     imgFilePath = applicationPath + "\\Organizations\\" + directoryName + "\\CSS\\Temp\\Image";
                     DeleteFiles(imgFilePath);

                     jsFilePath = applicationPath + "\\Organizations\\" + directoryName + "\\CSS\\Temp\\Javascript";
                     DeleteFiles(jsFilePath);

                     styleFilePath = applicationPath + "\\Organizations\\" + directoryName + "\\CSS\\Temp\\Styles";
                     DeleteFiles(styleFilePath);

                     //String folderPath = applicationPath + "\\Organizations\\" + directoryName + "\\CSS\\Mirror";
                     //DeleteFolder(folderPath);

                     //folderPath = applicationPath + "\\Organizations\\" + directoryName + "\\CSS\\Temp";
                     //DeleteFolder(folderPath);

                     //folderPath = applicationPath + "\\Organizations\\" + directoryName + "\\CSS";
                     //DeleteFolder(folderPath);

                     //folderPath = applicationPath + "\\Organizations\\" + directoryName;
                     //DeleteFolder(folderPath);
                 }
                
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method to delete the Folder
        /// <summary>
        ///   Method to delete the Folder
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public bool DeleteFolder(String delFolderName)
        {
            try
            {
                if (delFolderName == "")
                    return false;

                DirectoryInfo dInfo = new DirectoryInfo(delFolderName);

                if (dInfo.Exists)
                {
                    dInfo.Delete();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method to delete all files in the folder
        /// <summary>
        ///  Method to delete all files in the folder 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public bool DeleteFiles(String filePath)
        {
            try
            {
                if (filePath == "")
                    return false;

                DirectoryInfo directoryInfo = new DirectoryInfo(filePath);

                foreach (FileInfo file in directoryInfo.GetFiles())
                {
                    if (file.Exists)
                    {
                        file.Delete();
                    }
                }

               // directoryInfo.Delete();
               
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Method to GetCSSXML
        /// <summary>
        ///   Method to GetCSSXML based on folder path
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public String GetCSSXML()
        {
            try
            {
                String inXML = "";
                
                if (mirrorCssPath == "" || mirrorJSPath == "")
                    GetFilePath();

                String artifactsXML = applicationPath + "\\" + mirrorJSXmlPath;
                String mirrorxmlPath = applicationPath + "\\" + mirrorCssXmlPath;
                imageUtil = new myVRMNet.ImageUtil();
                string cssXML = imageUtil.ConvertImageToBase64(artifactsXML);
                inXML = "<MirrorCssXml>" + cssXML + "</MirrorCssXml>";
                cssXML = imageUtil.ConvertImageToBase64(mirrorxmlPath);
                inXML += "<ArtifactsXml>" + cssXML + "</ArtifactsXml>";

                return inXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion  
        
        #region Method to GetTextChangeXML
        /// <summary>
        ///   Method to GetTextChangeXML based on folder path
        /// </summary>
        /// <param name="folderName"></param>
        /// <returns></returns>
        public String GetTextChangeXML()
        {
            try
            {
                String inXML = "";

                if (mirrorTxtXmlPath == "")
                    GetFilePath();

                String textChangeXML = applicationPath + "\\" + mirrorTxtXmlPath;
                imageUtil = new myVRMNet.ImageUtil();
                string textXML = imageUtil.ConvertImageToBase64(textChangeXML);
                inXML = "<TextChangeXml>" + textXML + "</TextChangeXml>";

                return inXML;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion      

        //FB 2739 Starts
        #region Methods to Move the Banner into Mirror Location
        /// <summary>
        /// Method to move the banner in the temp location to the mirror.
        /// </summary>
        /// <returns type="void")>null</returns>
        public bool TempBannerToMirrorLocation()//ZD 100263
        {
            if (mirrorBannerPath1024 == "" || mirrorBannerPath1600 == "")
                GetFilePath();

            string mirrorFileName = "";
            string tempFileName = "";
            try
            {	
                //To move the banner
                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1024;
                tempFileName = applicationPath + "\\" + tempBannerPath1024;

                //ZD 104387 - Start
                if (File.Exists(tempFileName))
                {
                    if (File.Exists(mirrorFileName))
                        File.Delete(mirrorFileName);
                    File.Move(tempFileName, mirrorFileName);
                }   

                mirrorFileName = applicationPath + "\\" + mirrorBannerPath1600;
                tempFileName = applicationPath + "\\" + tempBannerPath1600;

                if (File.Exists(tempFileName))
                {
                    if (File.Exists(mirrorFileName))
                        File.Delete(mirrorFileName);                    
                    File.Move(tempFileName, mirrorFileName);
                }                
                    
                //ZD 104387
                mirrorFileName = applicationPath + "\\" + mirrorLogoPath;
                tempFileName = applicationPath + "\\" + tempLogoPath;

                if (File.Exists(tempFileName))
                {
                    if (File.Exists(mirrorFileName))
                        File.Delete(mirrorFileName);                          
                    File.Move(tempFileName, mirrorFileName);
                }
                //ZD 104387 - End

                return true;//ZD 100263
            }
            catch (FileNotFoundException fx)
            {
                return false;//ZD 100263
                throw fx;
            }
            catch (FieldAccessException fa)
            {
                return false;//ZD 100263
                throw new FieldAccessException("Please set the permissions on Best, Mirror, Temp and Original Folders");
            }
            catch (Exception ex)
            {
                return false;//ZD 100263
                throw ex;
            }
        }
        #endregion
        //FB 2739 End

        //Organization\CSC Module -- End

    }
}