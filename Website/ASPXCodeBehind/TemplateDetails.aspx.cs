/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

public partial class en_TemplateDetails : System.Web.UI.Page
{
    #region Protected Data Members

    protected System.Web.UI.WebControls.Label ErrLabel;
    protected System.Web.UI.WebControls.Label LblDescription;
    protected System.Web.UI.WebControls.Label LblConference;
    protected System.Web.UI.WebControls.Label LblRoom;
    protected System.Web.UI.WebControls.DataGrid DgTemplates;
    protected System.Web.UI.WebControls.Table TblNoTemplates;
    protected System.Web.UI.HtmlControls.HtmlTable tbPrt;//Added for FB 1425 QA Bug
    protected System.Web.UI.HtmlControls.HtmlTable tbDgPrt;   //Added for FB 1425 QA Bug

    #endregion

    #region  Variables Declaration

    private myVRMNet.NETFunctions obj = null;
    private ns_Logger.Logger log;
    MyVRMNet.Util utilObj;//FB 2236

    #endregion

    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion

    #region Page Load  Event Hander

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("TemplateDetails.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            obj = new myVRMNet.NETFunctions(); //organization module
            utilObj = new MyVRMNet.Util(); //FB 2236
            /* *** Code added for FB 1425 QA Bug -Start *** */

            if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
            {
                tbPrt.Visible = false;
                tbDgPrt.Visible = false;
            }

            /* *** Code added for FB 1425 QA Bug -End *** */
            BindData();
        }
        catch (Exception ex)
        {
            ErrLabel.Visible = true;
            //ErrLabel.Text = "PageLoad: " + ex.Message;ZD 100263
            ErrLabel.Text = obj.ShowSystemMessage();
            log.Trace("Page_Load:" + ex.Message);//ZD 100263
            WriteIntoLog(ex.StackTrace);
        }

    }
    #endregion

    #region BindData
    /// <summary>
    /// Construct inXML. Get the outXML from Com (Command : GetTemplate) and bind it to the controls
    /// Application["COM_ConfigPath"] refers ComConfig.xml file path
    /// </summary>

    private void BindData()
{
    XmlNode node = null;

    try
    {
        //FB 2027 - GetTemplate - Starts
        String inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><templateID>" + Request.QueryString["tid"].ToString() + "</templateID></login>";
        String outXML = obj.CallMyVRMServer("GetTemplate", inXML, Application["MyVRMServer_ConfigPath"].ToString());
        //String outXML = obj.CallCOM("GetTemplate", inXML, Application["COM_ConfigPath"].ToString());
        //FB 2027 - GetTemplate - End
        if (outXML != "")
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(outXML);

            //To bind Template Conference description in the header using confinfo node
            node = xmlDoc.DocumentElement.SelectSingleNode("confInfo");
            this.LblDescription.Text = utilObj.ReplaceOutXMLSpecialCharacters(node.SelectSingleNode("description").InnerText.Trim(),2);//FB 2236
            this.LblConference.Text = (node.SelectSingleNode("publicConf").InnerText == "1") ? obj.GetTranslatedText("Yes") : obj.GetTranslatedText("No");
            this.LblRoom.Text = obj.GetOrgLocNames(obj.ParseStr(outXML, "locationList", 1), "1").Trim();

            BindGrid(xmlDoc.SelectNodes("//conference/confInfo/partys/party"));
        }
    }
    catch (Exception ex)
    {
        throw ex;
    }
    finally
    {
         obj = null;
    }
}

    #endregion

    #region BindGrid
    protected void BindGrid(XmlNodeList partyNodes)
    {
        try
        {
            XmlTextReader xtr;
            DataSet ds = new DataSet();

            if (partyNodes != null)
            {
                foreach (XmlNode node in partyNodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];

                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = null;
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            dr = dt.Rows[i];
                            if (dr["partyLastName"] != null) //FB 2023
                                if (dr["partyLastName"].ToString().Trim() == "")
                                    dr["partyEmail"] = "";

                        }

                        DgTemplates.DataSource = dt;
                        DgTemplates.DataBind();

                    }
                }
                else
                    TblNoTemplates.Visible = true;
            }
           

        }
        catch (Exception ex)
        {
            //ErrLabel.Text = ex.StackTrace;ZD 100263
            ErrLabel.Text = obj.ShowSystemMessage();
            ErrLabel.Visible = true;
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region BindGrid Item DataBound
    protected void BindGrid(Object sender, DataGridItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
            {
                DataRowView row = e.Item.DataItem as DataRowView;

                e.Item.Cells[0].Text = row["partyFirstName"].ToString() + " " + row["partyLastName"].ToString();
                e.Item.Cells[1].Text = row["partyEmail"].ToString();
                Int32 status = Convert.ToInt32(row["partyInvite"].ToString());
                if (status == 0)
                    e.Item.Cells[2].Text = obj.GetTranslatedText("CC");//FB 1830 - Translation
                else if (status == 1)
                    e.Item.Cells[2].Text = obj.GetTranslatedText("External Attendee");//FB 1830 - Translation
                else if (status == 2)
                    e.Item.Cells[2].Text = obj.GetTranslatedText("Room Attendee");//FB 1830 - Translation
                else
                    e.Item.Cells[2].Text = " ";


            }
        }
        catch (Exception ex)
        {
            //ErrLabel.Text = ex.StackTrace;
            ErrLabel.Text = obj.ShowSystemMessage();
            ErrLabel.Visible = true;
            log.Trace(ex.StackTrace);
        }
    }

    #endregion

    #region BuildInXML
    /// <summary>
    /// To construct the inputXML for fetching the data
    /// </summary>
    /// <returns></returns>

    private String BuildInXML()
    {
        String inXML = "";
        inXML += "<login>";
        inXML += obj.OrgXMLElement();//Organization Module Fixes
        inXML += "<userID>" + Session["userID"].ToString() + "</userID>";
        inXML += " <templateID>" + Request.QueryString["tid"].ToString() + "</templateID>";
        inXML += "</login>";
        return inXML;
    }

    #endregion

    #region WriteIntoLog

    private void WriteIntoLog(String stackTrace)
    {
        log = new ns_Logger.Logger();
        log.Trace(stackTrace);
        log = null;
    }

    #endregion
}
