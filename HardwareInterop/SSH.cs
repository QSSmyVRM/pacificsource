﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dart.Ssh;
using System.Threading;
using System.Windows.Forms;

namespace NS_SSH
{
    class SSH
    {
        internal enum eTelnetOperation { CONNECT_HANGUP, DISPLAY_MESSAGE, CONNECTION_STATUS };
        private NS_LOGGER.Log logger;
        private NS_MESSENGER.ConfigParams configParams;
        internal string errMsg = null;
        Dart.Ssh.Ssh ssh_Connecter = new Dart.Ssh.Ssh();
        Dart.Ssh.Data data = null;
        Dart.Ssh.SshConnection connectn = null;
        Dart.Ssh.SshLoginData lData = null;
        SessionStream sessionStream;
        String message = "";

        internal SSH(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
        }

        internal bool TestConnectionSSH(NS_MESSENGER.Party Party)
        {
            try
            {
                

                lData = new Dart.Ssh.SshLoginData();
                lData.HostNameOrAddress = Party.sAddress;
                lData.Port = Party.iAPIPortNo;

                if (string.IsNullOrEmpty(Party.sLogin))
                    Party.sLogin = "admin";

                logger.Trace("Login :" + Party.sLogin);
                logger.Trace("pwd :" + Party.sPwd);

                lData.Username = Party.sLogin;
                lData.Password = Party.sPwd;
                lData.Pem = "";
                lData.Passphrase = "";

                ssh_Connecter.Connection.RemoteEndPoint.Port = lData.Port;
                ssh_Connecter.Connection.RemoteEndPoint.HostNameOrAddress = lData.HostNameOrAddress;
                if (ssh_Connecter.State == Dart.Ssh.ConnectionState.Closed)
                    ssh_Connecter.Connect();
                ssh_Connecter.Authenticate(lData);
                
                sessionStream = ssh_Connecter.StartShell();

                return true;
            }
            catch (Exception e)
            {
                errMsg = "SSHS Connection failed for Endpoint : " + Party.sName;
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool ConnectDisconnectCall(NS_MESSENGER.Conference conf, NS_MESSENGER.Party caller, bool connectOrDisconnect)
        {
            try
            {
                System.Collections.IEnumerator partyEnumerator;
                partyEnumerator = conf.qParties.GetEnumerator();
                int partyCount = conf.qParties.Count;

                // get the caller videoconferencing details
                for (int i = 0; i < partyCount; i++)
                {
                    // src party 
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    // go to next party in the queue
                    partyEnumerator.MoveNext();
                    party = (NS_MESSENGER.Party)partyEnumerator.Current;

                    if (caller == null)
                    {
                        if (party.etCallerCallee == NS_MESSENGER.Party.eCallerCalleeType.CALLER)
                        {
                            caller = new NS_MESSENGER.Party();
                            caller.iDbId = party.iDbId;
                            caller.sAddress = party.sAddress;
                            caller.sLogin = party.sLogin;
                            caller.sPwd = party.sPwd;
                            caller.etProtocol = party.etProtocol;
                            caller.stLineRate.etLineRate = party.stLineRate.etLineRate;
                            caller.etModelType = party.etModelType;
                            caller.iAPIPortNo = party.iAPIPortNo; //Code added for API Port No
                            caller.ip2pCallid = party.ip2pCallid; 
                            caller.iconfnumname = party.iconfnumname; 
                            logger.Trace("1-Caller model type: " + caller.etModelType.ToString());

                            // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                            // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                            if (conf.stLineRate.etLineRate < party.stLineRate.etLineRate)
                            {
                                caller.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                            }
                            
                            caller.iTerminalType = party.iTerminalType;
                            
                            break;
                        }
                    }
                    else
                    {
                        if (party.iDbId == caller.iDbId)
                        {
                            // Party same as caller so skip as we have this party's info already.
                            caller.sAddress = party.sAddress;
                            caller.sLogin = party.sLogin;
                            caller.sPwd = party.sPwd;
                            caller.etProtocol = party.etProtocol;
                            caller.stLineRate.etLineRate = party.stLineRate.etLineRate;
                            caller.etModelType = party.etModelType;
                            caller.iAPIPortNo = party.iAPIPortNo; //Code added for API Port No
                            caller.ip2pCallid = party.ip2pCallid; 
                            caller.iconfnumname = party.iconfnumname; 
                            logger.Trace("2-Caller model type: " + caller.etModelType.ToString());
                            // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                            // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                            if (conf.stLineRate.etLineRate < party.stLineRate.etLineRate)
                            {
                                caller.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                            }
                            
                            caller.iTerminalType = party.iTerminalType;
                            
                            break;
                        }
                    }
                }

                System.Collections.IEnumerator partyEnumerator1;
                partyEnumerator1 = conf.qParties.GetEnumerator();
                int partyCount1 = conf.qParties.Count;

                // now cycle thru the endpoints to go thru all callee's.
                for (int i = 0; i < partyCount1; i++)
                {
                    // src party 
                    logger.Trace("inside loop: " + i.ToString());
                    logger.Trace("inside count: " + partyCount1.ToString());
                    logger.Trace("Caller Model: " + caller.etModelType);
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    // go to next party in the queue
                    partyEnumerator1.MoveNext();
                    party = (NS_MESSENGER.Party)partyEnumerator1.Current;

                    if (party.iDbId == caller.iDbId)
                    {
                        // Party same as caller so skip as we have this party's info already.                    
                        continue;
                    }

                    string emptyString = null; bool ret = false;
                    switch (caller.etModelType)
                    {
                        case NS_MESSENGER.Party.eModelType.POLYCOM_HDX:
                            //ret = Dial_PolycomCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.POLYCOM_VSX:
                            //ret = Dial_PolycomVSXCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, ref caller.ip2pCallid, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.TANDBERG:
                            //ret = Dial_TandbergEdgeCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.CISCO:
                            //ret = Dial_CiscoCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, ref caller.ip2pCallid, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.MXP:
                            //ret = Dial_MXPCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, ref caller.ip2pCallid, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.ViewStation:
                            //ret = Dial_ViewStationCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, ref caller.ip2pCallid, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.CISCO_TS_TX:
                            ret = Dial_CiscoTelepresenceCaller(caller, party.sAddress, eTelnetOperation.CONNECT_HANGUP, connectOrDisconnect, null, ref emptyString, ref caller.ip2pCallid, conf);
                            break;
                        default:
                            {
                                this.errMsg = "Incorrect endpoint model type for p2p calls.";
                                logger.Trace("Incorrect endpoint model type for p2p calls.");
                                return false;
                            }
                    }

                    if (!ret)
                    {
                        errMsg += "SSH call failed for endpoint - " + party.sName;
                        logger.Trace(errMsg);
                        logger.Exception(100, errMsg);

                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool SendMessageTextToParty(NS_MESSENGER.Party party, string messageText, NS_MESSENGER.Conference conf)
        {
            try
            {
                string emptyString = null;
                bool ret = false;
                try
                {
                    switch (party.etModelType)
                    {
                        case NS_MESSENGER.Party.eModelType.POLYCOM_HDX:
                            //ret = Dial_PolycomCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.POLYCOM_VSX:
                            //ret = Dial_PolycomVSXCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, ref party.ip2pCallid, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.TANDBERG:
                            //ret = Dial_TandbergEdgeCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.CISCO:
                            //ret = Dial_CiscoCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, ref party.ip2pCallid, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.MXP:
                            //ret = Dial_MXPCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, ref party.ip2pCallid, conf);
                            break;
                        case NS_MESSENGER.Party.eModelType.ViewStation:
                            //ret = Dial_ViewStationCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, ref party.ip2pCallid, conf);
                            break;
                        default:
                            {
                                this.errMsg = "Incorrect endpoint model type for p2p calls.";
                                logger.Trace("Incorrect endpoint model type for p2p calls.");
                                return false;
                            }
                    }
                }
                catch (Exception)
                {
                    //ret = Dial_PolycomCaller(party, null, eTelnetOperation.DISPLAY_MESSAGE, true, messageText, ref emptyString, conf);
                }

                if (!ret)
                {
                    errMsg += "SSH call failed for endpoint - " + party.sName;
                    logger.Exception(100, errMsg);
                    return false;
                }

                return true;

            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool isEndpointConnected(ref NS_MESSENGER.Party endpoint, NS_MESSENGER.Party otherValidEndpoint, NS_MESSENGER.Conference conf)
        {
            try
            {
                // Call ssh method
                string connectionStatusOutput = null;
                System.Timers.Timer timer = new System.Timers.Timer(60000);
                timer.Enabled = true;
                timer.Start();
                bool ret = false;

                switch (endpoint.etModelType)
                {
                    case NS_MESSENGER.Party.eModelType.POLYCOM_HDX:
                        //ret = Dial_PolycomCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, conf);
                        break;
                    case NS_MESSENGER.Party.eModelType.POLYCOM_VSX:
                        //ret = Dial_PolycomVSXCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, ref endpoint.ip2pCallid, conf);
                        break;
                    case NS_MESSENGER.Party.eModelType.TANDBERG:
                        //ret = Dial_TandbergEdgeCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, conf);
                        break;
                    case NS_MESSENGER.Party.eModelType.CISCO:
                        //ret = Dial_CiscoCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, ref endpoint.ip2pCallid, conf);
                        break;
                    case NS_MESSENGER.Party.eModelType.MXP:
                        //ret = Dial_MXPCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, ref endpoint.ip2pCallid, conf);
                        break;
                    case NS_MESSENGER.Party.eModelType.ViewStation:
                        //ret = Dial_ViewStationCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, ref endpoint.ip2pCallid, conf);
                        break;
                    case NS_MESSENGER.Party.eModelType.CISCO_TS_TX:
                        ret = Dial_CiscoTelepresenceCaller(endpoint, null, eTelnetOperation.CONNECTION_STATUS, true, null, ref connectionStatusOutput, ref endpoint.ip2pCallid, conf);
                        break;
                    default:
                        {
                            endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.UNREACHABLE; //Blue status project
                            this.errMsg = "Incorrect endpoint model type for p2p calls.";
                            logger.Trace("Incorrect endpoint model type for p2p calls.");
                            return false;
                        }
                }

                timer.Stop();
                timer = null;

                logger.Trace("connectionStatusOutput : " + connectionStatusOutput);

                if (!ret)
                {
                    errMsg += "SSH call failed for endpoint - " + endpoint.sName;
                    logger.Exception(100, errMsg);
                }

                // parse the call connection details
                if (connectionStatusOutput == null)
                {
                    // status not available
                    endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.UNREACHABLE; // No color status
                    return true;
                }
                else
                {
                    if (connectionStatusOutput.Contains("system is not in a call"))
                    {
                        // endpoints are not connected
                        endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT; //Red status
                        return true;
                    }
                    else
                    {
                        // connected endpoints 
                        // fetch status 
                        if (connectionStatusOutput.ToLower().Contains("connected") || connectionStatusOutput.Contains("synced") || connectionStatusOutput.ToLower().Contains("answered"))
                        {
                            if (connectionStatusOutput.Contains(otherValidEndpoint.sAddress) || endpoint.etModelType == NS_MESSENGER.Party.eModelType.CISCO_TS_TX)
                            {
                                // endpoint is connected to the correct other endpoint. 
                                endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT; // Green Status
                                return true;
                            }
                            else
                            {
                                // endpoint is online but not connected to the correct other endpoint. 
                                endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.ONLINE; // Blue status
                                return true;
                            }
                        }
                        else if (connectionStatusOutput.ToLower().Contains("dialing"))
                        {
                            endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                        }
                        else //Glowpoint Issue
                        {
                            //endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT; //Red status
                            return false;
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                logger.Trace("8");
                endpoint.etStatus = NS_MESSENGER.Party.eOngoingStatus.UNREACHABLE; // No color status
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool Dial_CiscoTelepresenceCaller(NS_MESSENGER.Party caller, string destAddress, eTelnetOperation telnetOp, bool connectOrDisconnect, string messageText, ref string connectionStatusOutput, ref int p2pCallId, NS_MESSENGER.Conference conf)
        {
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
            try
            {
                NS_MESSENGER.Alert alert = new NS_MESSENGER.Alert();

                db.UpdateConferenceProcessStatus(conf.iDbID, conf.iInstanceID, 1);

                logger.Trace("In the ssh sub-system...");

                lData = new Dart.Ssh.SshLoginData();
                lData.HostNameOrAddress = caller.sAddress;
                lData.Port = caller.iAPIPortNo;

                if (string.IsNullOrEmpty(caller.sLogin))
                    caller.sLogin = "admin";

                lData.Username = caller.sLogin;
                lData.Password = caller.sPwd;
                lData.Pem = "";
                lData.Passphrase = "";

                logger.Trace("SrcIPAddress :" + caller.sAddress);
                logger.Trace("SSH port : " + caller.iAPIPortNo.ToString());
                logger.Trace("SrcLogin : " + caller.sLogin);
                logger.Trace("SrcPwd : " + caller.sPwd);
                logger.Trace("Connecting...");

                ssh_Connecter.Connection.RemoteEndPoint.Port = lData.Port;
                ssh_Connecter.Connection.RemoteEndPoint.HostNameOrAddress = lData.HostNameOrAddress;
                if (ssh_Connecter.State == Dart.Ssh.ConnectionState.Closed)
                    ssh_Connecter.Connect();
                ssh_Connecter.Authenticate(lData);

                sessionStream = ssh_Connecter.StartShell();
                ssh_Connecter.Start(Reader, sessionStream);
                Thread.Sleep(4000);

                //alert = new NS_MESSENGER.Alert();
                //alert.confID = conf.iDbID;
                //alert.instanceID = conf.iInstanceID;
                //alert.message = message.Trim();
                //alert.typeID = 15;
                //bool ret = false;
                //ret = db.InsertConfP2PAlert(alert);
                //if (!ret) logger.Trace("Point2Point alert insert failed.");

                logger.Trace("Data :" + message);

                if (message.ToLower().Contains("admin:"))
                {
                    // check if password fails
                    if (message.ToLower().Contains("password failed"))
                    {
                        logger.Trace("Incorrect password.");
                        return false;
                    }
                    string sendStr = null;
                    logger.Trace("SSHOperation :" + telnetOp);
                    switch (telnetOp)
                    {
                        case eTelnetOperation.CONNECT_HANGUP:
                            {
                                if (connectOrDisconnect)
                                {
                                    sendStr = "call start " + destAddress;
                                    sessionStream.Write(sendStr + (char)Keys.Return);

                                    logger.Trace("Send cmd :" + sendStr);

                                    //alert = new NS_MESSENGER.Alert();
                                    //alert.confID = conf.iDbID;
                                    //alert.instanceID = conf.iInstanceID;
                                    //alert.message =message.Trim();
                                    //alert.typeID = 15;
                                    //ret = false;
                                    //ret = db.InsertConfP2PAlert(alert);
                                    //if (!ret) logger.Trace("Point2Point alert insert failed.");
                                }
                                else
                                {
                                    sendStr = "call end ";
                                    logger.Trace("Send cmd : " + sendStr);
                                    sessionStream.Write(sendStr + (char)Keys.Return);

                                    //alert = new NS_MESSENGER.Alert();
                                    //alert.confID = conf.iDbID;
                                    //alert.instanceID = conf.iInstanceID;
                                    //alert.message = message.Trim();
                                    //alert.typeID = 15;
                                    //ret = false;
                                    //ret = db.InsertConfP2PAlert(alert);
                                    //if (!ret) logger.Trace("Point2Point alert insert failed.");
                                }
                                break;
                            }
                        case eTelnetOperation.DISPLAY_MESSAGE:
                            {
                                logger.Trace("Send Message Method not supported...");

                                //alert = new NS_MESSENGER.Alert();
                                //alert.confID = conf.iDbID;
                                //alert.instanceID = conf.iInstanceID;
                                //alert.message = "Send Message Method not supported...";
                                //alert.typeID = 15;
                                //ret = false;
                                //ret = db.InsertConfP2PAlert(alert);
                                //if (!ret) logger.Trace("Point2Point alert insert failed.");
                                break;
                            }
                        case eTelnetOperation.CONNECTION_STATUS:
                            {
                                sendStr = "call state ";
                                logger.Trace("Send cmd : " + sendStr);
                                sessionStream.Write(sendStr + (char)Keys.Return);
                                Thread.Sleep(2000);
                                connectionStatusOutput = message;

                                //alert = new NS_MESSENGER.Alert();
                                //alert.confID = conf.iDbID;
                                //alert.instanceID = conf.iInstanceID;
                                //alert.message = message.Trim();
                                //alert.typeID = 15;
                                //ret = false;
                                //ret = db.InsertConfP2PAlert(alert);
                                //if (!ret) logger.Trace("Point2Point alert insert failed.");
                                break;
                            }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = "Point to point connection failed. Error - " + e.Message;
                logger.Exception(100, this.errMsg);
                return false;
            }
            finally
            {
                db.UpdateConferenceProcessStatus(conf.iDbID, conf.iInstanceID, 0);
                try
                {
                    sessionStream.Close();
                    sessionStream.Dispose();                    
                    ssh_Connecter.Close();                    
                    ssh_Connecter.Dispose();

                }
                catch (Exception exf)
                {
                    
                    
                }
            }
        }

        private bool EquateWithPolycomLineRate(NS_MESSENGER.LineRate.eLineRate lineRate, ref string polycomLineRate)
        {
            try
            {
                // equating VRM to Polycom endpoint values            
                #region IP linerate mappings
                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                    polycomLineRate = "64";
                else
                {
                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                        polycomLineRate = "128";
                    else
                    {
                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                            polycomLineRate = "256";
                        else
                        {
                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                                polycomLineRate = "512";
                            else
                            {
                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                                    polycomLineRate = "768";
                                else
                                {
                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1024)
                                        polycomLineRate = "1024";
                                    else
                                    {
                                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                                            polycomLineRate = "1152";
                                        else
                                        {
                                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                                                polycomLineRate = "1472";
                                            else
                                            {
                                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                                                    polycomLineRate = "1536";
                                                else
                                                {
                                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1920)
                                                        polycomLineRate = "1920";
                                                    else
                                                    {
                                                        if (lineRate >= NS_MESSENGER.LineRate.eLineRate.M2048)
                                                            polycomLineRate = "2048";
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        public void Reader(object state)
        {
            SessionStream session = (SessionStream)state;
            message = "";
            byte[] buf = null;
            Data data = null;
            try
            {
                while (true)
                {
                    try
                    {
                        buf = new byte[1000];
                        data = sessionStream.Read(buf);

                        if (data.Count == 0)
                            break;

                        message += System.Text.Encoding.Default.GetString(data.Buffer, 0, data.Count);

                        
                    }
                    catch (Exception ex)
                    {
                        logger.Exception(100, ex.Message);
                        continue;
                    }
                }
            }
            catch (Exception e)
            {

                logger.Exception(100, e.Message);
            }
            
            
        }
    }
}
