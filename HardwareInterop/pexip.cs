﻿//ZD 100147 Start //ZD 101217 new class added for Pexip
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/* FILE : pexip.cs
 * DESCRIPTION : All pexip MCU api commands are stored in this file. 
 * 
 */

#region Region of Includes
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Threading;
#endregion

namespace NS_Pexip
{
    class pexip
    {
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;

        #region Pexip
        internal pexip(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
        }
        #endregion

        #region Public Class

        internal class Conference
        {
            public List<ConferenceAlias> aliases;
            public string name { get; set; }
            public bool allow_guests { get; set; }
            public string description { get; set; }
            public int max_callrate_in { get; set; }
            public int max_callrate_out { get; set; }
            public string service_type { get; set; }
            public string pin { get; set; }
            public string conference_id { get; set; }
			public string guest_pin { get; set; } //ALLDEV-826
        }

        internal class Participant
        {
            public string conference_alias { get; set; }
            public string destination { get; set; }
            public string protocol { get; set; }
            public string node { get; set; }
            public string role { get; set; }
            public string system_location { get; set; }
            public string participant_id { get; set; }
        }

        internal class ConferenceAlias
        {
            public string alias { get; set; }
        }

        internal class ConferenceUpdate
        {
            public List<ConferenceAlias> aliases;
            public string pin { get; set; }
            public string description { get; set; }
            public string name { get; set; }
            public bool allow_guests { get; set; }
            public string guest_pin { get; set; } 
        }

        #endregion

        #region Command

        #region  TestMCUConnection
        /// <summary>
        /// To Test MCU Connection
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool TestMCUConnection(NS_MESSENGER.MCU mcu)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            bool ret = false;
            try
            {
                logger.Trace("Entering into Test MCU Connection Command....");

                StrURL = "/api/admin/status/v1/system_location/";//ZD 104086
                method = "GET";
              
                ret = false;
                ret = SendCommand(mcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  SetConference
        /// <summary>
        /// SetConference
        /// </summary>
        /// <param name="conference"></param>
        /// <returns></returns>
        internal bool SetConference(NS_MESSENGER.Conference conference)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            string alias = "", uri = "", Alias2 = "", Alias3 = "";
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Conference> ConferenceList = new List<Conference>();
            Conference Conf = new Conference();
            ConferenceAlias ConfAlias = new ConferenceAlias();
            try
            {
                logger.Trace("Entering into SetConference Command....");

                Jsetting.NullValueHandling = NullValueHandling.Ignore;

                Conf.aliases = new List<ConferenceAlias>();

                ConfAlias.alias = conference.iDbNumName.ToString();//ZD 101522 //ZD 104152

                Conf.name = conference.sDbName;
               

                //ALLDEV-826 Starts
                if (conference.iEnableHostGuestPwd > 0)
                {
                    if (conference.iHostPwd > 0)
                    {
                        Conf.allow_guests = true;
                        Conf.pin = conference.iHostPwd.ToString();
                    }
                    else
                    {
                        Conf.allow_guests = false;
                        Conf.pin = "";
                    }
                    if (conference.iPwd > 0)
                    {
                        Conf.allow_guests = true;
                        Conf.guest_pin = conference.iPwd.ToString(); //ALLDEV-826
                    }
                }
                if (conference.iEnableConfpassword > 0)
                {
                    if (conference.iPwd > 0)
                    {
                        Conf.allow_guests = true;
                        Conf.pin = conference.iPwd.ToString();
                    }
                }
                //ALLDEV-826 Ends

                Conf.description = conference.sDescription;

                int LineRate = 384;
                bool ret2 = EquateLineRate(conference.stLineRate.etLineRate, ref LineRate);
                if (!ret2)
                {
                    logger.Trace("Invalid LineRate.");
                    return false;
                }

                Conf.max_callrate_in = LineRate;
                Conf.max_callrate_out = LineRate;
                Conf.service_type = "conference";
                Conf.aliases.Add(ConfAlias);
                //ALLDEV-854 Start
                if (conference.cMcu.iEnableAlias == 1)
                {
                    if (!String.IsNullOrEmpty(conference.cMcu.sAlias2))
                    {
                        Alias2 = conference.cMcu.sAlias2.Replace("{5}", conference.iDbNumName.ToString());
                        ConfAlias = new ConferenceAlias();
                        ConfAlias.alias = Alias2;
                        Conf.aliases.Add(ConfAlias);
                    }

                    if (!String.IsNullOrEmpty(conference.cMcu.sAlias3))
                    {
                        Alias3 = conference.cMcu.sAlias3.Replace("{5}", conference.iDbNumName.ToString());
                        ConfAlias = new ConferenceAlias();
                        ConfAlias.alias = Alias3;
                        Conf.aliases.Add(ConfAlias);
                    }
                }
                //ALLDEV-854 End
                ConferenceList.Add(Conf);

                StrURL = "/api/admin/configuration/v1/conference/";
                method = "POST";

                input = JsonConvert.SerializeObject(ConferenceList, Jsetting);
                input = "{\"objects\": " + input + " }";

                ret = false;
                ret = SendCommand(conference.cMcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                //StrURL = "/api/admin/configuration/v1/conference/?limit=100"; // ZD 104631
                //StrURL = "/api/admin/configuration/v1/conference_alias/?alias=" + conference.iDbNumName.ToString();  // ZD 104644
                StrURL = "/api/admin/configuration/v1/conference_alias/?alias=" + conference.PexipAlias;//ALLDEV-782 
            
                method = "GET";
                input = "";

                ret = false;
                ret = SendCommand(conference.cMcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    XmlNodeList nodelist = xd.SelectNodes("//Root/objects"); // ZD 104644

                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        node = nodelist[i].SelectSingleNode("alias");
                        if (node != null)
                        {
                            alias = node.InnerText.Trim();
                            //if(conference.iDbNumName.ToString() != alias)//ZD 104152
                            if (conference.PexipAlias != alias)//ALLDEV-782
                                continue;
                            else
                            {
                                node = nodelist[i].SelectSingleNode("conference");
                                if (node != null)
                                    uri = node.InnerText.Trim();

                                db = new NS_DATABASE.Database(configParams);

                                if (conference.dtStartDateTimeInUTC > DateTime.UtcNow)
                                    conference.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                                else
                                    conference.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;

                                db.UpdateConference(conference.iDbID, conference.iInstanceID, "", conference.etStatus, uri);

                                break;
                            }
                        }
                    }

                    logger.Trace("Total parties: " + conference.qParties.Count.ToString());

                    while (conference.qParties.Count > 0)
                    {
                        NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                        party1 = (NS_MESSENGER.Party)conference.qParties.Dequeue();
                        if (party1.etConnType != NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                        {
                            ret = ConnectDisconnectEndpoint(conference, party1, true);
                            if (!ret)
                            {
                                logger.Exception(100, "Participant failed to be added : " + party1.sName);
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region DeleteConference
        /// <summary>
        /// DeleteConference
        /// </summary>
        /// <param name="conference"></param>
        /// <returns></returns>
        internal bool DeleteConference(NS_MESSENGER.Conference conference)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            bool ret = false;

            string Alias2 = "", Alias3 = "";
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            ConferenceUpdate ConfUpdate = new ConferenceUpdate();
            List<string> ConfAliases = new List<string>();
            try
            {
                logger.Trace("Entering into Delete Conference Command....");

                if (conference.iEnableS4BEWS == 0 && conference.iIsLyncConf == 0) //ZD 105371
                {
                    //ALLDEV-782 Starts
                    #region Delete the VMR- Ont time Meeting
                    if (conference.iPexipCOnfTYpe == 2)
                    {
                        method = "DELETE";

                        StrURL = conference.sURI;

                        ret = false;
                        ret = SendCommand(conference.cMcu, StrURL, method, input, ref responseXML);
                        if (!ret) return false;
                    }
                    #endregion

                    #region  Reset the Personal VMR and Delete the conference aliases

                    if (conference.iPexipCOnfTYpe == 1)
                    {
                        //Reset the Values
                        ConfUpdate.allow_guests = false;
                        ConfUpdate.pin = "";
                        ConfUpdate.guest_pin = "";


                        StrURL = conference.sURI;
                        method = "PATCH";
                        Jsetting.NullValueHandling = NullValueHandling.Ignore;
                        input = JsonConvert.SerializeObject(ConfUpdate, Jsetting);

                        ret = false;
                        ret = SendCommand(conference.cMcu, StrURL, method, input, ref responseXML);

                        //Delete the Conference VMR
                        ConfAliases.Add(conference.iDbNumName.ToString());
                        if (conference.cMcu.iEnableAlias == 1)
                        {
                            if (!String.IsNullOrEmpty(conference.cMcu.sAlias2))
                            {
                                Alias2 = conference.cMcu.sAlias2.Replace("{5}", conference.iDbNumName.ToString());
                                ConfAliases.Add(Alias2);
                            }
                            if (!String.IsNullOrEmpty(conference.cMcu.sAlias3))
                            {
                                Alias3 = conference.cMcu.sAlias3.Replace("{5}", conference.iDbNumName.ToString());
                                ConfAliases.Add(Alias3);
                            }
                        }
                        DeleteVMRAlias(ConfAliases, conference);

                    }
                    #endregion
                    //ALLDEV-782 Ends
                }
                #region Disconnect the Parties
                logger.Trace("Total parties: " + conference.qParties.Count.ToString());

                while (conference.qParties.Count > 0)
                {
                    NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                    party1 = (NS_MESSENGER.Party)conference.qParties.Dequeue();
                    
                    ret = ConnectDisconnectEndpoint(conference, party1, false);
                    if (!ret)
                    {
                        logger.Exception(100, "Participant failed to be added : " + party1.sName);
                    }
                }

                #endregion

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  ConnectDisconnectEndpoint
        /// <summary>
        /// ConnectDisconnectEndpoint
        /// </summary>
        /// <param name="conference"></param>
        /// <param name="party"></param>
        /// <param name="ConnectDisconnect"></param>
        /// <returns></returns>
        internal bool ConnectDisconnectEndpoint(NS_MESSENGER.Conference conference, NS_MESSENGER.Party party, bool ConnectDisconnect)
        {
            string StrURL = "", method = "", input = "", responseXML = "", protocol = "", status = "", S4BID = "", DOMAIN = "", USER = "";//ALLDEV-862
            string partyBJNAddress = "", meetingid = "", passcode = "", sysLocationName = "";  //ZD 104114 //ZD 104821
            int Bindex = 0, SysLocation = 0; //ZD 104821 //ZD 104114
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Participant> ParticipantList = new List<Participant>();
            Participant Party = new Participant();
            string[] DomainDelimeter = { "sip:" }; 
            try
            {
                logger.Trace("Entering into ConnectDisconnectEndpoint Command....");

                method = "POST";

                if (string.IsNullOrEmpty(conference.cMcu.sIp))
                    conference.cMcu = party.cMcu;

                if (ConnectDisconnect)
                {
                    ParticipantList = new List<Participant>();
                    Party = new Participant();
                    Jsetting.NullValueHandling = NullValueHandling.Ignore;
                    //ALLDEV-862 Start
                    if (conference.iIsLyncConf == 1)
                    {
                        if (!string.IsNullOrEmpty(conference.sconfLyncMeetingURL))
                        {
                            String[] meeting = conference.sconfLyncMeetingURL.Split('?');
                            meeting = meeting[0].Split(DomainDelimeter, StringSplitOptions.None);
                            Party.conference_alias = meeting[1];
                            conference.PexipAlias = meeting[1]; 
                            //String[] meeting = conference.sconfLyncMeetingURL.Split('/');
                            //S4BID = meeting[meeting.Length - 1];
                            //USER = meeting[meeting.Length - 2];
                            //DOMAIN = meeting[meeting.Length - 3];
                            //if (DOMAIN.Contains(".com"))
                            //{
                            //    DOMAIN = DOMAIN.Replace("meet.", "").Trim();
                            //}
                            //else
                            //    DOMAIN = DOMAIN + ".com";
                            //Party.conference_alias = USER + "@" + DOMAIN + ";gruu;opaque=app:conf:focus:id:" + S4BID;
                        }
                        else
                        {
                            logger.Trace("Error in loading conference alias " + conference.sconfLyncMeetingURL);
                            return false;
                        }
                    }
                    else
                        Party.conference_alias = conference.iDbNumName.ToString();//ZD 101522 //ZD 104152
                    //ALLDEV-862 End

                    Party.conference_alias = conference.PexipAlias;//ZD 101522 //ZD 104152 //ALLDEV-782
                    //ZD 104114 Start
                    if (!String.IsNullOrEmpty(party.sAddress))
                    {
                        if (party.sAddress.Trim().Contains("B"))
                        {
                            Bindex = party.sAddress.Trim().IndexOf("B");
                            partyBJNAddress = party.sAddress.Trim().Substring(0, Bindex);

                            if (party.sAddress.Split('+').Length > 0)
                            {
                                meetingid = party.sAddress.Split('B')[1].Split('+')[0]; //Meeting ID
                                passcode = party.sAddress.Split('B')[1].Split('+')[1]; //Attendee passcode
                            }

                            party.sAddress = meetingid + "." + passcode + "@" + partyBJNAddress.Trim();
                        }
                    }
                    //ZD 104114 End
                    Party.destination = party.sAddress.Trim();
                    Party.role = "guest";

                    if (party.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                        protocol = "h323";
                    else
                        protocol = "sip";

                    Party.protocol = protocol;
                    //ZD 104821 Starts
                    if (party.SysLocationID > 0)
                        SysLocation = party.SysLocationID;
                    else
                        SysLocation = conference.iSystemLocation;
                    
                    db.FetchSystemLocation(conference.cMcu.iDbId, SysLocation, ref sysLocationName);

                    //Party.system_location = conference.sSystemLocationName; //ZD 101522
                    Party.system_location = sysLocationName; //ZD 101522
                    //ZD 104821 Ends
                    ParticipantList.Add(Party);

                    input = JsonConvert.SerializeObject(ParticipantList, Jsetting);
                    input = input.Replace("[", "").Replace("]", "");

                    StrURL = "/api/admin/command/v1/participant/dial/";

                }
                else
                {
                    ParticipantList = new List<Participant>();
                    Party = new Participant();
                    Jsetting.NullValueHandling = NullValueHandling.Ignore;
                    Party.participant_id = party.sGUID;

                    ParticipantList.Add(Party);

                    input = JsonConvert.SerializeObject(ParticipantList, Jsetting);
                    input = input.Replace("[", "").Replace("]", "");

                    StrURL = "/api/admin/command/v1/participant/disconnect/";
                }

                ret = false;
                ret = SendCommand(conference.cMcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);

                    node = xd.SelectSingleNode("//Root/status");
                    if (node != null)
                    {
                        status = node.InnerText.Trim();

                        logger.Trace("Status of connection " + status); //ZD 104579
                        if (status == "error")
                        {
                            int i = 0; bool error = true;
                            while (error && i < 5)
                            {
                                Thread.Sleep(10000);
                                ret = false;
                                ret = SendCommand(conference.cMcu, StrURL, method, input, ref responseXML);
                                if (!ret) return false;

                                node = null;
                                xd = new XmlDocument();
                                xd.LoadXml(responseXML);
                                {
                                    node = xd.SelectSingleNode("//Root/data/participant_id");
                                    if (node != null)
                                    {
                                        error = false;
                                        party.sGUID = node.InnerText.Trim();

                                        if (party.sAddress != "" && party.sGUID != "")
                                            db.UpdateConferenceEndpointStatus(conference.iDbID, conference.iInstanceID, party.sAddress, party.sGUID);
                                    }
                                }

                                i++;
                            }
                        }
                        else if (status == "success")
                        {
                            node = xd.SelectSingleNode("//Root/data/participant_id");
                            if (node != null)
                            {
                                party.sGUID = node.InnerText.Trim();

                                if (party.sAddress != "" && party.sGUID != "")
                                    db.UpdateConferenceEndpointStatus(conference.iDbID, conference.iInstanceID, party.sAddress, party.sGUID);
                            }
                        }
                    }
                    else
                    {
                        node = xd.SelectSingleNode("//Root/data/participant_id");
                        if (node != null)
                        {
                            party.sGUID = node.InnerText.Trim();

                            if (party.sAddress != "" && party.sGUID != "")
                                db.UpdateConferenceEndpointStatus(conference.iDbID, conference.iInstanceID, party.sAddress, party.sGUID);
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion

        #region  MuteEndpoint
        /// <summary>
        /// MuteEndpoint
        /// </summary>
        /// <param name="conference"></param>
        /// <param name="party"></param>
        /// <param name="Mute"></param>
        /// <returns></returns>
        internal bool MuteEndpoint(NS_MESSENGER.Conference conference, NS_MESSENGER.Party party, bool Mute)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Participant> ParticipantList = new List<Participant>();
            Participant Party = new Participant();
            try
            {
                logger.Trace("Entering into MuteEndpoint Command....");

                method = "POST";

                ParticipantList = new List<Participant>();
                Party = new Participant();
                Jsetting.NullValueHandling = NullValueHandling.Ignore;

                Party.participant_id = party.sGUID;

                ParticipantList.Add(Party);

                input = JsonConvert.SerializeObject(ParticipantList, Jsetting);
                input = input.Replace("[", "").Replace("]", "");

                if (Mute)
                    StrURL = "/api/admin/command/v1/participant/mute/";
                else
                    StrURL = "/api/admin/command/v1/participant/unmute/";

                ret = false;
                ret = SendCommand(party.cMcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

		//ZD 101522 Starts

        #region  FetchSystemLocation
        /// <summary>
        /// FetchSystemLocation
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool FetchSystemLocation(NS_MESSENGER.MCU mcu)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            bool ret = false;
            try
            {
                logger.Trace("Entering into FetchSystemLocation Command....");

                StrURL = "/api/admin/configuration/v1/system_location/";
                method = "GET";

                ret = false;
                ret = SendCommand(mcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    List<NS_MESSENGER.SystemLocation> SystemLocationlist = new List<NS_MESSENGER.SystemLocation>();
                    NS_MESSENGER.SystemLocation SystemLocation = null;
                    XmlNodeList nodelist = xd.SelectNodes("//Root/objects");
                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        SystemLocation = new NS_MESSENGER.SystemLocation();

                        node = nodelist[i].SelectSingleNode("id");
                        if (node != null)
                            SystemLocation.id = node.InnerText.Trim();

                        node = nodelist[i].SelectSingleNode("name");
                        if (node != null)
                            SystemLocation.name = node.InnerText.Trim();

                        SystemLocationlist.Add(SystemLocation);
                    }

                    db = new NS_DATABASE.Database(configParams);
                    db.UpdateSystemLocation(mcu.iDbId, SystemLocationlist);
                    return true;
                }


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        //ZD 105372, 105373 Start
        #region GetEndpointStatus
        /// <summary>
        /// GetEndpointStatus
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party party)
        {
            try
            {
                if (conf.iIsLyncConf == 1 && conf.iEnableS4BEWS == 1)
                {
                    if (!GetLyncEndpointStatus(ref party))
                        return false;
                }
                else
                {
                    if (!GetNormalEndpointStatus(conf, ref party))
                        return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

        #region GetLyncEndpointStatus
        /// <summary>
        /// GetLyncEndpointStatus
        /// </summary>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool GetLyncEndpointStatus(ref NS_MESSENGER.Party party)
        {
            string PartyGUID = "", StrURL = "", method = "", input = "", responseXML = "", mute = "";
            try
            {
                logger.Trace("Entering Participant Status method...");


                StrURL = "/api/admin/status/v1/participant/";
                method = "GET";
                input = "";

                bool ret = false;
                ret = SendCommand(party.cMcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;


                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    XmlNodeList nodelist = xd.SelectNodes("//Root/objects");
                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        node = nodelist[i].SelectSingleNode("id");
                        if (node != null)
                        {
                            PartyGUID = node.InnerText.Trim();
                            if (party.sGUID == PartyGUID)
                            {
                                node = nodelist[i].SelectSingleNode("is_muted");
                                if (node != null)
                                {
                                    mute = node.InnerText.Trim();
                                    if (mute == "true")
                                        party.bMute = true;
                                    else
                                        party.bMute = false;
                                }
                                party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                                break;
                            }
                        }
                    }

                    db.UpdateConfPartyDetails(party);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion
        //ZD 105372, 105373 End


        #region GetNormalEndpointStatus
        /// <summary>
        /// GetEndpointStatus
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="party"></param>
        /// <returns></returns>
        internal bool GetNormalEndpointStatus(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party party)  //ZD 105372, 105373
        {
            string StrURL = "", method = "", input = "", responseXML = "", name = "", GUID = "";
            try
            {
                logger.Trace("Entering Conference Status method...");


                StrURL = "/api/admin/status/v1/conference/";
                method = "GET";
                input = "";

                bool ret = false;
                ret = SendCommand(party.cMcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;


                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    XmlNodeList nodelist = xd.SelectNodes("//Root/objects");
                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        name = "";
                        node = nodelist[i].SelectSingleNode("name");
                        if (node != null)
                        {
                            name = node.InnerText.Trim();
                            if (conf.sDbName == name)
                            {
                                node = nodelist[i].SelectSingleNode("id");
                                if (node != null)
                                {
                                    GUID = node.InnerText.Trim();
                                }
                                conf.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;
                            }
                        }
                    }

                    db.UpdateConference(conf.iDbID, conf.iInstanceID, GUID, conf.etStatus, "");

                    ret = false;
                    ret = ProcessXML_AllPartyList(ref party, conf.sDbName, conf.iDbID, conf.iInstanceID);
                    if (!ret)
                    {
                        logger.Trace("Failure in processing the conference Participant list returned by Pexip MCU");
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }


        internal bool ProcessXML_AllPartyList(ref NS_MESSENGER.Party party, string Confname, int confid, int instanceid)
        {
            string PartyGUID = "", StrURL = "", method = "", input = "", responseXML = "", name = "", mute = "";
            try
            {
                logger.Trace("Entering Participant Status method...");


                StrURL = "/api/admin/status/v1/participant/";
                method = "GET";
                input = "";

                bool ret = false;
                ret = SendCommand(party.cMcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;


                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    XmlNodeList nodelist = xd.SelectNodes("//Root/objects");
                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        node = nodelist[i].SelectSingleNode("conference");
                        if (node != null)
                        {
                            name = node.InnerText;
                            if (name == Confname)
                            {
                                node = nodelist[i].SelectSingleNode("id");
                                if (node != null)
                                {
                                    PartyGUID = node.InnerText.Trim();
                                    if (party.sGUID == PartyGUID)
                                    {
                                        node = nodelist[i].SelectSingleNode("is_muted");
                                        if (node != null)
                                        {
                                            mute = node.InnerText.Trim();
                                            if (mute == "true")
                                                party.bMute = true;
                                            else
                                                party.bMute = false;
                                        }
                                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                                        break;
                                    }
                                }
                            }
                        }                       
                    }

                    db.UpdateConfPartyDetails(party);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        #endregion

		//ZD 101522 End

        //ALLDEV-710 Starts
        #region Conference Lock/Unlock
        /// <summary>
        /// LockUnlockConference
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="lockorunlock"></param>
        /// <returns></returns>
        internal bool LockUnlockConference(NS_MESSENGER.Conference conf, int lockorunlock)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            bool ret = false;
            try
            {
                logger.Trace("Entering into LockUnlockConference Command....");

                method = "POST";
                input = "{\"conference_id\":\"" + conf.sGUID + "\"}";

                if (lockorunlock == 1)
                    StrURL = "/api/admin/command/v1/conference/lock/";
                else
                    StrURL = "/api/admin/command/v1/conference/unlock/";

                ret = false;
                ret = SendCommand(conf.cMcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion
        //ALLDEV-710 Ends

        //ALLDEV-782 Starts

        #region  UpdateVMRConference
        /// <summary>
        /// UpdateVMRConference
        /// </summary>
        /// <param name="conference"></param>
        /// <returns></returns>
        internal bool UpdateVMRConference(NS_MESSENGER.Conference conference, string Confalias, string ConfURI)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            string alias = "", uri = "", Alias2 = "", Alias3 = "";
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            ConferenceUpdate ConfUpdate = new ConferenceUpdate();
            ConferenceAlias ConfAlias = new ConferenceAlias();
            try
            {
                logger.Trace("Entering into UpdateVMRConference Command....");

                conference.sPexipStaticAlias = Confalias;

                StrURL = "/api/admin/configuration/v1/conference_alias/?alias=" + Confalias;

                method = "GET";
                input = "";

                ret = false;
                ret = SendCommand(conference.cMcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;


                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    XmlNodeList nodelist = xd.SelectNodes("//Root/objects");

                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        node = nodelist[i].SelectSingleNode("alias");
                        if (node != null)
                        {
                            alias = node.InnerText.Trim();
                            if (conference.sPexipStaticAlias != alias)
                                continue;
                            else
                            {
                                node = nodelist[i].SelectSingleNode("conference");
                                if (node != null)
                                    uri = node.InnerText.Trim();

                                db = new NS_DATABASE.Database(configParams);

                                if (conference.dtStartDateTimeInUTC > DateTime.UtcNow)
                                    conference.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                                else
                                    conference.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;

                                db.UpdateConference(conference.iDbID, conference.iInstanceID, "", conference.etStatus, uri);

                                break;
                            }
                        }
                    }

                    logger.Trace("Total parties: " + conference.qParties.Count.ToString());

                    while (conference.qParties.Count > 0)
                    {
                        NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                        party1 = (NS_MESSENGER.Party)conference.qParties.Dequeue();
                        if (party1.etConnType != NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                        {
                            ret = ConnectDisconnectEndpoint(conference, party1, true);
                            if (!ret)
                            {
                                logger.Exception(100, "Participant failed to be added : " + party1.sName);
                            }
                        }
                    }
                }

                Jsetting.NullValueHandling = NullValueHandling.Ignore;

                ConfUpdate.aliases = new List<ConferenceAlias>();

                ConfAlias.alias = conference.iDbNumName.ToString();
                ConfUpdate.aliases.Add(ConfAlias);

                if (conference.cMcu.iEnableAlias == 1)
                {
                    if (!String.IsNullOrEmpty(conference.cMcu.sAlias2))
                    {
                        Alias2 = conference.cMcu.sAlias2.Replace("{5}", conference.iDbNumName.ToString());
                        ConfAlias = new ConferenceAlias();
                        ConfAlias.alias = Alias2;
                        ConfUpdate.aliases.Add(ConfAlias);
                    }

                    if (!String.IsNullOrEmpty(conference.cMcu.sAlias3))
                    {
                        Alias3 = conference.cMcu.sAlias3.Replace("{5}", conference.iDbNumName.ToString());
                        ConfAlias = new ConferenceAlias();
                        ConfAlias.alias = Alias3;
                        ConfUpdate.aliases.Add(ConfAlias);
                    }
                }
                if (conference.iEnableHostGuestPwd > 0)
                {
                    if (conference.iHostPwd > 0)
                    {
                        ConfUpdate.allow_guests = true;
                        ConfUpdate.pin = conference.iHostPwd.ToString();
                    }
                    else
                    {
                        ConfUpdate.allow_guests = false;
                        ConfUpdate.pin = "";
                    }
                    if (conference.iPwd > 0)
                    {
                        ConfUpdate.allow_guests = true;
                        ConfUpdate.guest_pin = conference.iPwd.ToString();
                    }
                }
                if (conference.iEnableConfpassword > 0)
                {
                    if (conference.iPwd > 0)
                    {
                        ConfUpdate.allow_guests = true;
                        ConfUpdate.pin = conference.iPwd.ToString();
                    }
                }


                StrURL = ConfURI;
                method = "PATCH";

                input = JsonConvert.SerializeObject(ConfUpdate, Jsetting);

                ret = false;
                ret = SendCommand(conference.cMcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  StaticVMR
        /// <summary>
        /// SetConference
        /// </summary>
        /// <param name="conference"></param>
        /// <returns></returns>
        internal bool StaticVMR(NS_MESSENGER.ActiveUser user, NS_MESSENGER.MCU mcu)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            string alias = "", uri = "";
            bool ret = false;
            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            List<Conference> ConferenceList = new List<Conference>();
            Conference Conf = new Conference();
            ConferenceAlias ConfAlias = new ConferenceAlias();
            try
            {
                logger.Trace("Entering into StaticPexipVMR Command....");

                Jsetting.NullValueHandling = NullValueHandling.Ignore;

                Conf.aliases = new List<ConferenceAlias>();

                ConfAlias.alias = user.PexipVMRAlias;

                Conf.name = user.userName;

                Conf.description = "";
                Conf.max_callrate_in = 4096;
                Conf.max_callrate_out = 4096;

                Conf.service_type = "conference";
                Conf.aliases.Add(ConfAlias);
                ConferenceList.Add(Conf);

                //if (user.Email != user.PexipVMRAlias)
                //{
                //    ConfAlias = new ConferenceAlias();
                //    ConfAlias.alias = user.Email;
                //    Conf.aliases.Add(ConfAlias);
                //}
                Conf.name = user.userName;

                StrURL = "/api/admin/configuration/v1/conference/";
                method = "POST";

                input = JsonConvert.SerializeObject(ConferenceList, Jsetting);
                input = "{\"objects\": " + input + " }";

                ret = false;
                ret = SendCommand(mcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                StrURL = "/api/admin/configuration/v1/conference_alias/?alias=" + user.PexipVMRAlias;  

                method = "GET";
                input = "";

                ret = false;
                ret = SendCommand(mcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    XmlNodeList nodelist = xd.SelectNodes("//Root/objects");

                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        node = nodelist[i].SelectSingleNode("alias");
                        if (node != null)
                        {
                            alias = node.InnerText.Trim();
                            if (user.PexipVMRAlias != alias)
                                continue;
                            else
                            {
                                node = nodelist[i].SelectSingleNode("conference");
                                if (node != null)
                                    uri = node.InnerText.Trim();

                                db = new NS_DATABASE.Database(configParams);

                                if (!string.IsNullOrEmpty(uri))
                                    db.UpdateUserPexipAlias(uri, user.UserID);

                                break;
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  DeleteVMRAlias
        /// <summary>
        /// MuteEndpoint
        /// </summary>
        /// <param name="conference"></param>
        /// <param name="party"></param>
        /// <param name="Mute"></param>
        /// <returns></returns>
        internal bool DeleteVMRAlias(List<string> ConferenceAlias, NS_MESSENGER.Conference conference)
        {
            string StrURL = "", method = "", input = "", responseXML = "", alias = "", uri = "";
            bool ret = false;
            try
            {
                logger.Trace("Entering into DeleteVMRAlias Command....");


                //Alias Delete
                for (int cnt = 0; cnt < ConferenceAlias.Count; cnt++)
                {
                    if (ConferenceAlias[cnt] != "")
                    {
                        responseXML = "";
                        method = "GET";
                        input = "";

                        StrURL = "/api/admin/configuration/v1/conference_alias/?alias=" + ConferenceAlias[cnt];
                     
                        ret = false;
                        ret = SendCommand(conference.cMcu, StrURL, method, input, ref responseXML);
                        if (!ret) return false;

                        if (!string.IsNullOrEmpty(responseXML))
                        {
                            XmlNode node = null;
                            XmlDocument xd = new XmlDocument();
                            xd.LoadXml(responseXML);
                            XmlNodeList nodelist = xd.SelectNodes("//Root/objects");

                            for (int i = 0; i < nodelist.Count; i++)
                            {
                                node = nodelist[i].SelectSingleNode("alias");
                                if (node != null)
                                {
                                    alias = node.InnerText.Trim();
                                    if (ConferenceAlias[cnt] != alias)
                                        continue;
                                    else
                                    {
                                        node = nodelist[i].SelectSingleNode("resource_uri");
                                        if (node != null)
                                            uri = node.InnerText.Trim();

                                        break;
                                    }
                                }
                            }

                            method = "DELETE";
                            StrURL = uri;

                            ret = false;
                            ret = SendCommand(conference.cMcu, StrURL, method, input, ref responseXML);
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  DeleteUserVMR
        /// <summary>
        /// DeleteUserVMR - Called while deleting a user
        /// </summary>
        /// <param name="conference"></param>
        /// <returns></returns>
        internal bool DeleteUserVMR(string UserAliasURI, NS_MESSENGER.MCU mcu)
        {
            string StrURL = "", method = "", input = "", responseXML = "";
            bool ret = false;
            try
            {
                logger.Trace("Entering into DeleteUserVMR Command....");

                method = "DELETE";

                StrURL = UserAliasURI;

                ret = false;
                ret = SendCommand(mcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        #region  UpdateUserAlias
      /// <summary>
      /// UpdateUserAlias - Updating alias when changes in myVRM
      /// </summary>
      /// <param name="user"></param>
      /// <param name="mcu"></param>
      /// <returns></returns>
        internal bool UpdateUserAlias(NS_MESSENGER.ActiveUser user, string UserOldAlias, NS_MESSENGER.MCU mcu)
        {
            string StrURL = "", method = "", input = "", responseXML = "", uri = "", alias = "";
            bool ret = false;

            JsonSerializerSettings Jsetting = new JsonSerializerSettings();
            ConferenceUpdate ConfUpdate = new ConferenceUpdate();
            ConferenceAlias ConfAlias = new ConferenceAlias();
            try
            {
                logger.Trace("Entering into DeleteUserVMR Command....");

                Jsetting.NullValueHandling = NullValueHandling.Ignore;

                ConfUpdate.aliases = new List<ConferenceAlias>();

                ConfAlias.alias = user.PexipVMRAlias;
                ConfUpdate.aliases.Add(ConfAlias);

                StrURL = user.PexipALiasURI;

                method = "PATCH";

                input = JsonConvert.SerializeObject(ConfUpdate, Jsetting);

                ret = false;
                ret = SendCommand(mcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;


                //Delete the Old User Alias
                responseXML = "";
                method = "GET";
                input = "";

                StrURL = "/api/admin/configuration/v1/conference_alias/?alias=" + UserOldAlias;

                ret = false;
                ret = SendCommand(mcu, StrURL, method, input, ref responseXML);
                if (!ret) return false;

                if (!string.IsNullOrEmpty(responseXML))
                {
                    XmlNode node = null;
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    XmlNodeList nodelist = xd.SelectNodes("//Root/objects");

                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        node = nodelist[i].SelectSingleNode("alias");
                        if (node != null)
                        {
                            alias = node.InnerText.Trim();
                            if (UserOldAlias != alias)
                                continue;
                            else
                            {
                                node = nodelist[i].SelectSingleNode("resource_uri");
                                if (node != null)
                                    uri = node.InnerText.Trim();

                                break;
                            }
                        }
                    }

                    method = "DELETE";
                    StrURL = uri;

                    ret = false;
                    ret = SendCommand(mcu, StrURL, method, input, ref responseXML);
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion

        //ALLDEV-782 Ends
        //ALLDEV-862 Start
        #region  SetLyncPexipConference
        /// <summary>
        /// SetLyncPexipConference
        /// </summary>
        /// <param name="conference"></param>
        /// <returns></returns>
        internal bool SetLyncPexipConference(NS_MESSENGER.Conference conference)
        {
            string uri = "";
            bool ret = false;
            try
            {
                logger.Trace("Entering into SetLyncPexipConference Command....");

                db = new NS_DATABASE.Database(configParams);

                if (conference.dtStartDateTimeInUTC > DateTime.UtcNow)
                    conference.etStatus = NS_MESSENGER.Conference.eStatus.OnMCU;
                else
                    conference.etStatus = NS_MESSENGER.Conference.eStatus.ONGOING;

                db.UpdateConference(conference.iDbID, conference.iInstanceID, "", conference.etStatus, uri);

                while (conference.qParties.Count > 0)
                {
                    NS_MESSENGER.Party party1 = new NS_MESSENGER.Party();
                    party1 = (NS_MESSENGER.Party)conference.qParties.Dequeue();
                    if (party1.etConnType != NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                    {
                        ret = ConnectDisconnectEndpoint(conference, party1, true);
                        if (!ret)
                        {
                            logger.Exception(100, "Participant failed to be added : " + party1.sName);
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }
        #endregion
        //ALLDEV-862 End

        #endregion

        #region SendCommand
        /// <summary>
        /// SendCommand
        /// </summary>
        /// <param name="mcu"></param>
        /// <param name="strUrl"></param>
        /// <param name="method"></param>
        /// <param name="jsonString"></param>
        /// <param name="receiveXML"></param>
        /// <returns></returns>
        private bool SendCommand(NS_MESSENGER.MCU mcu, string strUrl, string method, string jsonString, ref string receiveXML)
        {
            string access= "http://";
            try
            {

                logger.Trace("*********SendXML***********");
                
                logger.Trace("Call URL: " + strUrl);
                logger.Trace("Call JsonString: " + jsonString);

                System.Net.ServicePointManager.ServerCertificateValidationCallback =
                   ((sender1, certificate, chain, sslPolicyErrors) => true);
                ServicePointManager.Expect100Continue = false;

                //ZD 104086 START
                if (mcu.iURLAccess == 1)
                    access = "https://";

                string Url = access + mcu.sIp.Trim() + ":" + mcu.iHttpPort + strUrl;
                if (mcu.iHttpPort == 443)
                    Url = "https://" + mcu.sIp.Trim() + strUrl;
                if (mcu.iHttpPort == 80)
                    Url = "http://" + mcu.sIp.Trim() + strUrl;
                //ZD 104086 END

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(Url);

                req.Method = method;
                req.KeepAlive = true;
                req.Credentials = new NetworkCredential(mcu.sLogin, mcu.sPwd);
                if (!string.IsNullOrEmpty(jsonString))
                {
                    req.PreAuthenticate = true;
                    req.Accept = "application/json";
                    req.ContentType = "application/json; charset=utf-8";
                    System.IO.Stream stream = req.GetRequestStream();
                    byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(jsonString);
                    stream.Write(arrBytes, 0, arrBytes.Length);
                    stream.Close();
                }
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                Stream respStream = resp.GetResponseStream();
                StreamReader rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                receiveXML = rdr.ReadToEnd();

                logger.Trace("*********ResponseXML***********");
                logger.Trace(receiveXML);

                if (!string.IsNullOrEmpty(receiveXML))
                {
                    XNode node = JsonConvert.DeserializeXNode(receiveXML, "Root");
                    receiveXML = node.ToString();
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                this.errMsg = e.Message;
                return false;
            }
        }

        #endregion

        #region EquateLineRate
        private bool EquateLineRate(NS_MESSENGER.LineRate.eLineRate lineRateValue, ref int codianLineRate)
        {
            try
            {
                switch (lineRateValue)
                {
                    case NS_MESSENGER.LineRate.eLineRate.K64:
                        {
                            codianLineRate = 64;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K128:
                        {
                            codianLineRate = 128;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K192:
                        {
                            codianLineRate = 192;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K256:
                        {
                            codianLineRate = 256;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K320:
                        {
                            codianLineRate = 320;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K384:
                        {
                            codianLineRate = 384;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K512:
                        {
                            codianLineRate = 512;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.K768:
                        {
                            codianLineRate = 768;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1024:
                        {
                            codianLineRate = 1024;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1152:
                        {
                            codianLineRate = 1024;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1250:
                        {
                            codianLineRate = 1250;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1472:
                        {
                            codianLineRate = 1250;
                            break;
                        }

                    case NS_MESSENGER.LineRate.eLineRate.M1536:
                        {
                            codianLineRate = 1536;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1792:
                        {
                            codianLineRate = 1792;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M1920:
                        {
                            codianLineRate = 1792;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M2048:
                        {
                            codianLineRate = 2048;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M2560:
                        {
                            codianLineRate = 2560;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M3072:
                        {
                            codianLineRate = 3072;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M3584:
                        {
                            codianLineRate = 3584;
                            break;
                        }
                    case NS_MESSENGER.LineRate.eLineRate.M4096:
                        {
                            codianLineRate = 4096;
                            break;
                        }
                    default:
                        {
                            // default is 384 kbps
                            codianLineRate = 384;
                            break;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        #endregion

    }
}
