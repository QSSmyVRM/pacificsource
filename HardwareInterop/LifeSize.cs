﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
/* FILE : LifeSize.cs
 * DESCRIPTION : All LifeSize MCU api commands are stored in this file. 
 * 
 */

#region Region of Includes
	using System;
	using System.Xml;
	using System.Net;
	using System.Collections;
    using System.Collections.Generic;
    using System.Text;
    using System.IO;
    using Dart.PowerTCP.Mail;
    using System.Web;
    using System.Diagnostics;
    using System.Xml.Linq;
 
#endregion 

namespace NS_LifeSize
{
    class LifeSize
    	{
		private NS_LOGGER.Log logger;
		internal string errMsg = null;
		NS_MESSENGER.ConfigParams configParams;
        String soapWrapper = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:hel=\"http://lifesize.com/HeliumWS\"><soapenv:Header/><soapenv:Body>{0}</soapenv:Body></soapenv:Envelope>";
        int iContigency = 2;
        NS_DATABASE.Database db = null;

        internal LifeSize(NS_MESSENGER.ConfigParams config)
		{
			configParams = new NS_MESSENGER.ConfigParams();
			configParams = config;
			logger = new NS_LOGGER.Log(configParams);
		}
		

		private bool SendTransaction_overXMLHTTP (NS_MESSENGER.MCU mcu,string sendXML,ref string receiveXML)
		{
            String soapURI = @"/soap";

            if (sendXML.Length < 1)
            {
                logger.Trace("No data being sent so it is delete command");

            }

            try
            {


                int tryAgainCount = 0;
                bool tryAgain = false;
                do
                {
                    tryAgain = false;

                    logger.Trace("*********SendXML***********");
                    logger.Trace(sendXML);

                    string strUrl = "http://" + mcu.sIp.Trim() + ":" + mcu.iHttpPort + soapURI;//Code added for Api port

                    if (mcu.iHttpPort == 443)
                        strUrl = "https://" + mcu.sIp.Trim() + soapURI;
                    else if (mcu.iHttpPort == 80)
                        strUrl = "http://" + mcu.sIp.Trim() + soapURI;

                    Uri mcuUri = new Uri(strUrl);
                    NetworkCredential mcuCredential = new NetworkCredential();
                    mcuCredential.UserName = mcu.sLogin;
                    mcuCredential.Password = mcu.sPwd;
                    CredentialCache mcuCredentialCache = new CredentialCache();
                    mcuCredentialCache.Add(mcuUri, "Basic", mcuCredential);
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strUrl);
                    req.Credentials = mcuCredentialCache;
                    System.Net.ServicePointManager.Expect100Continue = false;
                    

                    req.ContentType = "text/xml;";  //charset=UTF-8";
                    req.Method = "POST";
                    req.MediaType = "HTTP:/1.1";
                    
                    System.IO.Stream stream = req.GetRequestStream();
                    byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sendXML);
                    stream.Write(arrBytes, 0, arrBytes.Length);
                    stream.Close();



                    WebResponse resp = req.GetResponse();
                    Stream respStream = resp.GetResponseStream();
                    StreamReader rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                    receiveXML = rdr.ReadToEnd();
                    logger.Trace("*********ReceiveXML***********");
                    logger.Trace(receiveXML);

                    // FB#845: check for expired token error "STATUS_FAIL_TO_LOCATE_USER"
                    if (receiveXML.Contains(">Invalid Session<"))
                    {
                        NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);

                        // delete the existing tokens
                        db.DeleteMcuTokens();

                        // save the old tokens
                        string oldMcuToken = mcu.sToken;
                        string oldUserToken = mcu.sUserToken;

                        // generate new tokens
                        bool ret = LoginToBridge(ref mcu);
                        if (!ret)
                        {
                            logger.Exception(100, "Login to MCU failed.");
                            return false;
                        }

                        // try the transaction again. Replace the sendXML tokens with the new ones.
                        sendXML = sendXML.Replace("<hel:sessionID>" + oldMcuToken + "</hel:sessionID>", "<hel:sessionID>" + mcu.sToken + "</hel:sessionID>");


                        if (tryAgainCount < 2)
                        {
                            tryAgainCount++;
                            tryAgain = true;
                        }
                    }
                }
                while (tryAgain == true);

                return true;

            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                string debugString = "Error sending data to bridge. Error = " + e.Message;
                debugString = "SendXML = " + sendXML + " , ReceiveXML = " + receiveXML;
                logger.Exception(100, debugString);
                return false;
            }

		}

        //ZD 100369_MCU Start
        private bool SendMCUStatusCommand_overXMLHTTP(NS_MESSENGER.MCU mcu, string sendXML, ref string receiveXML, int Timeout)
        {
            String soapURI = @"/soap";

            if (sendXML.Length < 1)
            {
                logger.Trace("No data being sent so it is delete command");

            }

            try
            {


                int tryAgainCount = 0;
                bool tryAgain = false;
                do
                {
                    tryAgain = false;

                    logger.Trace("*********SendXML***********");
                    logger.Trace(sendXML);

                    string strUrl = "http://" + mcu.sIp.Trim() + ":" + mcu.iHttpPort + soapURI;//Code added for Api port

                    if (mcu.iHttpPort == 443)
                        strUrl = "https://" + mcu.sIp.Trim() + soapURI;
                    else if (mcu.iHttpPort == 80)
                        strUrl = "http://" + mcu.sIp.Trim() + soapURI;

                    Uri mcuUri = new Uri(strUrl);
                    NetworkCredential mcuCredential = new NetworkCredential();
                    mcuCredential.UserName = mcu.sLogin;
                    mcuCredential.Password = mcu.sPwd;
                    CredentialCache mcuCredentialCache = new CredentialCache();
                    mcuCredentialCache.Add(mcuUri, "Basic", mcuCredential);
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strUrl);
                    req.Credentials = mcuCredentialCache;
                    System.Net.ServicePointManager.Expect100Continue = false;


                    req.ContentType = "text/xml;";  //charset=UTF-8";
                    req.Method = "POST";
                    req.MediaType = "HTTP:/1.1";
                    req.Timeout = Timeout;
                    System.IO.Stream stream = req.GetRequestStream();
                    byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sendXML);
                    stream.Write(arrBytes, 0, arrBytes.Length);
                    stream.Close();



                    WebResponse resp = req.GetResponse();
                    Stream respStream = resp.GetResponseStream();
                    StreamReader rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                    receiveXML = rdr.ReadToEnd();
                    logger.Trace("*********ReceiveXML***********");
                    logger.Trace(receiveXML);

                    // FB#845: check for expired token error "STATUS_FAIL_TO_LOCATE_USER"
                    if (receiveXML.Contains(">Invalid Session<"))
                    {
                        NS_DATABASE.Database db = new NS_DATABASE.Database(this.configParams);

                        // delete the existing tokens
                        db.DeleteMcuTokens();

                        // save the old tokens
                        string oldMcuToken = mcu.sToken;
                        string oldUserToken = mcu.sUserToken;

                        // generate new tokens
                        bool ret = LoginToBridge(ref mcu);
                        if (!ret)
                        {
                            logger.Exception(100, "Login to MCU failed.");
                            return false;
                        }

                        // try the transaction again. Replace the sendXML tokens with the new ones.
                        sendXML = sendXML.Replace("<hel:sessionID>" + oldMcuToken + "</hel:sessionID>", "<hel:sessionID>" + mcu.sToken + "</hel:sessionID>");


                        if (tryAgainCount < 2)
                        {
                            tryAgainCount++;
                            tryAgain = true;
                        }
                    }
                }
                while (tryAgain == true);

                return true;

            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                string debugString = "Error sending data to bridge. Error = " + e.Message;
                debugString = "SendXML = " + sendXML + " , ReceiveXML = " + receiveXML;
                logger.Exception(100, debugString);
                return false;
            }

        }
        //ZD 100369_MCU End
		
		#region Login to Bridge
		private bool LoginToBridge(ref NS_MESSENGER.MCU bridge)
		{			
			logger.Trace("LoginToBridge()");
			try
			{			
				// check database if the tokens are present or not for this mcu
				NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
				bool ret = db.FetchMcuTokens(bridge.iDbId,ref bridge.sToken,ref bridge.sUserToken,true);
				
				// check if tokens are valid
				if (bridge.sToken == null )
				{
					logger.Trace ("Bridge tokens are null.");

					// Create the login record 
					string loginXml = null; ret = false;
					ret = CreateXML_Login (ref bridge,ref loginXml);
					if (!ret) 
					{
						logger.Exception (100,"CreateXML failed for bridge .Bridge Name = " + bridge.sName);
						return false;
					}


					// Login to the bridge.
					string responseXml = null;ret= false;
					{
						ret = SendTransaction_overXMLHTTP(bridge,loginXml,ref responseXml);
					}

					if (!ret) 
					{
						logger.Exception (100," SendTransaction failed for bridge .Bridge Name = " + bridge.sName);
						return false; 
					}

					// Parse the response.
					ret = false;
					ret = ProcessXML_Login(ref bridge,responseXml);
					if (!ret) 
					{
						logger.Exception (100," ProcessXML failed for bridge .Bridge Name = " + bridge.sName);
						return false; 
					}
				
					// Save these tokens in the database 
					db.InsertMcuTokens(bridge.iDbId,bridge.sToken,bridge.sUserToken);
				}
				else
				{
					// tokens are still valid and can be used. 
					// hence, update the timesnapshot for the tokens
					db.UpdateMcuTokens(bridge.iDbId);
				}
				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}// end of BridgeLogin()

		private bool CreateXML_Login(ref NS_MESSENGER.MCU bridge,ref string loginXml)
		{
			// Create the login xml string.
			logger.Trace("LOGIN XML()");

			try
			{
				if (bridge.sIp.Length < 1 || bridge.sLogin.Length < 1 || bridge.sPwd.Length < 1 )
				{
					//Check for empty fields.
					logger.Exception(100,"Empty bridge fields");
					return false;
				}
				loginXml = "<hel:startSession>";
                loginXml += "<hel:username>"+ bridge.sLogin.Trim() +"</hel:username>";
                loginXml += "</hel:startSession>";

                loginXml = CreateRequestXML(loginXml);

				return true;
			}
			catch(Exception e)
			{ 
				logger.Exception (100,e.Message);
				return false;
			}
		}

		private bool ProcessXML_Login(ref NS_MESSENGER.MCU bridge,string responseXml)
		{
			logger.Trace ("ProcessXML_Login");
            String bodyXML = "";

			try
			{
				if (responseXml.Length < 1) return false; 	// empty login response 

                bodyXML = GetSoapBody(responseXml);
				
				XmlDocument xd = new XmlDocument();
                xd.LoadXml(bodyXML);

				
				// bridge connection is ok
                bridge.sToken = xd.GetElementsByTagName("sessionID").Item(0).InnerText.Trim();
                bridge.sUserToken = bridge.sToken;

				if (bridge.sToken.Length <1 || bridge.sUserToken.Length <1) 
				{
					// empty values . return error.
					this.errMsg = "Login tokens from MCU are not valid.";
					logger.Exception(100,"Login token from bridge is not valid. ResponseXML = " + responseXml );
					return false ; 
				}
			}
			catch (Exception e)
			{
				this.errMsg = e.Message;
				string debugString = "Login to bridge failed. Error = " + e.Message;
				debugString += ". ResponseXML = " + responseXml;
				logger.Exception(100,debugString);
				return false;
			}
		
			return true; 
		}

		#endregion

		internal bool SetConference(NS_MESSENGER.Conference conf)
		{
            logger.Trace("Entering SetConference function...");
            String confUID = "";
            String confID = "";
            String responseXml = "";
			try
			{
				// Setup conferences on the bridge
							
				// Login to the bridge
				bool ret = LoginToBridge(ref conf.cMcu);
				if (!ret) 
				{
					logger.Exception (100,"Login to bridge failed. Bridge Name = " + conf.cMcu.sName);
					return false; 
				}

                if (conf.ESId != "" && conf.ESId.Length > 12)
                    confUID = conf.ESId;

				// Step 1: Create the reservation xml string with data
				string reservationXML = null;ret = false;
				ret = CreateXML_Reservation(ref conf,ref reservationXML,ref confUID);
				if (!ret || reservationXML.Length < 1) 
				{
					logger.Exception (100,"Creation of CreateXML_Reservation failed . commonXML = " + reservationXML);
					return false;
				}

				
			
                logger.Trace("Sending to life Size");
                ret = SendTransaction_overXMLHTTP(conf.cMcu, reservationXML, ref responseXml);

				if (!ret) 
				{
                    logger.Exception(100, "SendTransaction failed . confXml = " + reservationXML);
					return false; 
				}

				// Parse the response.
				ret=false;
                ret = ProcessXML_Reservation(responseXml, ref confID);
				if (!ret) 
				{
					logger.Exception (100,"ProcessXML_Reservation failed . responseXml = " + responseXml );
					return false; 
				}

                if(db == null)
                    db = new NS_DATABASE.Database(this.configParams);

                db.UpdateConferenceExternalID(conf.iDbID, conf.iInstanceID, confUID);

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

        internal bool ExtendConference(NS_MESSENGER.Conference conf, int extendTime)
        {
            logger.Trace("Entering Extend Conference function...");
            bool ret = false;
            String confUID = "";
            String confID = "";
            String responseXml = "";
            try
            {
                if (conf != null)
                {
                    conf.iDuration = conf.iDuration + extendTime;
                   

                    ret = LoginToBridge(ref conf.cMcu);
                    if (!ret)
                    {
                        logger.Exception(100, "Login to bridge failed. Bridge Name = " + conf.cMcu.sName);
                        return false;
                    }

                    if (conf.ESId != "" && conf.ESId.Length > 12)
                        confUID = conf.ESId;

                    // Step 1: Create the reservation xml string with data
                    string reservationXML = null; ret = false;
                    ret = CreateXML_Reservation(ref conf, ref reservationXML, ref confUID);
                    if (!ret || reservationXML.Length < 1)
                    {
                        logger.Exception(100, "Creation of CreateXML_Reservation failed . commonXML = " + reservationXML);
                        return false;
                    }



                    logger.Trace("Sending to life Size");
                    ret = SendTransaction_overXMLHTTP(conf.cMcu, reservationXML, ref responseXml);

                    if (!ret)
                    {
                        logger.Exception(100, "SendTransaction failed . confXml = " + reservationXML);
                        return false;
                    }

                    // Parse the response.
                    ret = false;
                    ret = ProcessXML_Reservation(responseXml, ref confID);
                    if (!ret)
                    {
                        logger.Exception(100, "ProcessXML_Reservation failed . responseXml = " + responseXml);
                        return false;
                    }
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool TerminateConference(NS_MESSENGER.Conference conf)
        {
            logger.Trace("Entering Extend Conference function...");
            bool ret = false;
            String confUID = "";
            String responseXml = "";
            try
            {
                if (conf != null)
                {
                    ret = LoginToBridge(ref conf.cMcu);
                    if (!ret)
                    {
                        logger.Exception(100, "Login to bridge failed. Bridge Name = " + conf.cMcu.sName);
                        return false;
                    }

                    if (conf.ESId != "" && conf.ESId.Length > 12)
                        confUID = conf.ESId;

                    // Step 1: Create the reservation xml string with data
                    string terminateXML = null; ret = false;
                    ret = CreateXML_TerminateConference(ref terminateXML,conf.cMcu, confUID);
                    if (!ret || terminateXML.Length < 1)
                    {
                        logger.Exception(100, "Creation of CreateXML_Reservation failed . commonXML = " + terminateXML);
                        return false;
                    }



                    logger.Trace("Sending to life Size");
                    ret = SendTransaction_overXMLHTTP(conf.cMcu, terminateXML, ref responseXml);

                    if (!ret)
                    {
                        logger.Exception(100, "SendTransaction failed . confXml = " + terminateXML);
                        return false;
                    }

                    // Parse the response.
                    ret = false;
                    ret = ProcessXML_TerminateReservation(responseXml);
                    if (!ret)
                    {
                        logger.Exception(100, "ProcessXML_Reservation failed . responseXml = " + responseXml);
                        return false;
                    }
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
		
		#region Reservation
		private bool CreateXML_Reservation(ref NS_MESSENGER.Conference conf,ref string reservationXml,ref string confUID)
		{
            String eventStr = "";
            String confStartTime = "", ConfEndTime = "", sLinerate = "1024", sParties = "", sLayout = "1"; 
            int numResources = 0;
			try
			{
                if(confUID.Trim() == "")
                    confUID = Guid.NewGuid().ToString().Trim();

                if (conf.sDescription == null)
                    conf.sDescription = conf.sExternalName;

                if (conf.sDescription.Trim() == "")
                    conf.sDescription = conf.sExternalName;

                TimeSpan timeDiff = conf.dtStartDateTimeInUTC.Subtract(DateTime.UtcNow);

                 if (timeDiff.Days != 0 || timeDiff.Hours != 0 || timeDiff.Minutes >= 5)
                    confStartTime = conf.dtStartDateTimeInUTC.ToString(@"yyyyMMdd\THHmmss\Z");    // scheduled reservation
                else
                    confStartTime = DateTime.UtcNow.AddSeconds(15).ToString(@"yyyyMMdd\THHmmss\Z");// immediate conf. so it needs adjustment.

                ConfEndTime = conf.dtStartDateTimeInUTC.AddMinutes(conf.iDuration).ToString(@"yyyyMMdd\THHmmss\Z");
                numResources = conf.qParties.Count + iContigency;
                EquateLineRate(conf.stLineRate.etLineRate, true, ref sLinerate);
                EquateVideoLayout(conf.iVideoLayout, ref sLayout);
                Create_PartyList(ref conf,ref sParties);
                

				#region reservation xml

                eventStr =  "BEGIN:VEVENT \r\n";
                eventStr += "UID:" + confUID + " \r\n";
                eventStr += "TRANSP:OPAQUE \r\n";
                eventStr += "PRIORITY:1 \r\n";
                eventStr += "CONTACT:" + conf.cMcu.sLogin + " \r\n";
                eventStr += "SUMMARY:" + conf.sExternalName + "(" + conf.iDbNumName.ToString().Trim() + ") \r\n";
                eventStr += "DESCRIPTION:" + conf.sDescription + " \r\n";
                eventStr += "DTSTART:" + confStartTime.Trim() + " \r\n";
                eventStr += "DTEND:" + ConfEndTime.Trim() + " \r\n";
                eventStr += sParties;
                eventStr += "RESOURCES:VIDEO=" + numResources.ToString() + " \r\n";
                eventStr += "X-LIFESIZE:conferenceId=" + conf.iDbNumName.ToString().Trim() + " password=" + conf.sPwd.Trim() + " language=en_US videoCodecs=auto  \r\n";
                eventStr += "  voiceCodecs=auto maxParticipantBitrate=" + sLinerate + " presentations=on \r\n";
                eventStr += "  presentationCodecs=auto register=true resolution=auto security=auto \r\n";
                eventStr += "  streaming=false expanded=false uiAnnouncements=on uiInput=dtmf uiInset=7 \r\n";
                eventStr += "  uiLayoutLocked=false uiUserControl=on uiLayoutName=" + sLayout + " uiOverlay=on \r\n";
                eventStr += "  uiSelfView=off uiTalkerOrder=on fec=off vop=false vopState=-1 \r\n";
                eventStr += "  createdByVop=false mediaServer=0 profile=none version=1.5.0.17 \r\n";
                eventStr += "END:VEVENT";

               reservationXml = "<hel:Scheduler_saveEventLong>";
               reservationXml += "<hel:sessionID>" + conf.cMcu.sToken.Trim() + "</hel:sessionID>";
               reservationXml += "<hel:event>" + eventStr + "</hel:event>";
               reservationXml += "<hel:transId>" + Guid.NewGuid().ToString() + "</hel:transId>";
               reservationXml += "</hel:Scheduler_saveEventLong>";

               reservationXml = CreateRequestXML(reservationXml);
				#endregion
			}
			catch(Exception e)
			{ 
				logger.Exception(100,e.Message);
				return false;
			}
			return true;
		}//end of Reservation XML

        private bool ProcessXML_Reservation(string responseXML, ref String confID)
		{
            String description = "";
            XmlNode node = null;
            XmlDocument xd = null;
            String bodyXML = "";
			try
			{
                if (responseXML.Length < 1) return false; 	// empty login response 

                bodyXML = GetSoapBody(responseXML);

                if (bodyXML.Length < 1) return false; 

                xd = new XmlDocument();
                xd.LoadXml(bodyXML);

                node = xd.GetElementsByTagName("faultstring")[0];

                if (node != null)
                {
                    this.errMsg = node.InnerText;
                    return false; //error

                }


                node = xd.GetElementsByTagName("_rv")[0];

				
				if (node != null)
				{	
					description = node.InnerText.Trim().ToUpper();

                    if (Int32.Parse(description) < 0)
                        return false;

                    node = xd.GetElementsByTagName("conferenceId")[0];

                    if (node != null)
                    {
                        description = node.InnerText.Trim();

                        if (Int32.Parse(description) < 0)
                            return false;

                        node = xd.GetElementsByTagName("conferenceId")[0];
                        if (node != null)
                            confID = node.InnerText.Trim();

                        if (confID != "")
                            return true;
                    }
                   
				}
                
				logger.Exception(100,"Conf setup on lifesize failed. ResponseXML = " + description);
				return false;//conf setup was unsuccessfull	
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		private bool Create_PartyList(ref NS_MESSENGER.Conference conf , ref string partyList)
		{
			try 
			{
				//Create the participant xml list 
				string party = null;
				logger.Trace ("Create Party List count =" + conf.qParties.Count.ToString());
		
				//cycle through each participant.
				while (conf.qParties.Count > 0)
				{
					//individual party info
					NS_MESSENGER.Party partyObj = new NS_MESSENGER.Party();
					partyObj = (NS_MESSENGER.Party)conf.qParties.Dequeue();

                    if (partyObj.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                        continue;
                    
                    // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                    // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                    if (conf.stLineRate.etLineRate < partyObj.stLineRate.etLineRate)
                    {
                        partyObj.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                    }

					bool ret = Create_Party(ref party,partyObj);
					if (!ret) party = null;
			
					// append party info to partylist
					partyList += party;
				}		
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

		private bool isValidIPAddress(string ip)
		{
			bool ret = true;
			string[] octets = ip.Split('.');
			if ( octets.Length != 4 )
				ret = false;
			else
			{
				for ( int i=0; i<4; i++ )
				{
					int dig = Convert.ToInt32(octets[i]);
					if ( dig < 0 || dig > 255 )
					{
						ret = false;
						break;
					}
				}
			}
			return ret;
		}

		private bool Create_Party(ref string partyXml,NS_MESSENGER.Party partyObj)
		{
			logger.Trace("Party = " + partyObj.sName);
            if (partyObj.sName == null)
            {
                this.errMsg = "Invalid participant name.";
                return false; 
            }
            if (partyObj.sName.Length < 3)
            {
                this.errMsg = "Invalid participant name.";
                return false; 
            }

			partyXml = null;
            String sPartLinerate = "1024";
            String sPartyprotocol = "h323";
			try
			{


                if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                    sPartyprotocol = "ip";
                else if (partyObj.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    sPartyprotocol = "sip";

                EquateLineRate(partyObj.stLineRate.etLineRate , true, ref sPartLinerate);
                partyXml = "ATTENDEE:X-LSDIAL=" + partyObj.sAddress + ",," + sPartyprotocol + "," + sPartLinerate + ", \r\n";

              	return true;
			}
			catch (Exception e)
			{
				// log the error and trash this participant info.
				string debugString = "Participant Not Added . Error = "+ e.Message ;
				debugString += ". PartyXML = " + partyXml;
				logger.Exception (100,debugString);
				return false;
			}
		}

		#endregion

		#region Add/Delete Party

        internal bool AddPartyToMcu(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            try
            {
               
                bool ret = false;
                // create a tmp conf which is for this party only.
                NS_MESSENGER.Conference partyConf = new NS_MESSENGER.Conference();
                // Login to the bridge the party is on.
                ret = LoginToBridge(ref party.cMcu);
                if (!ret)
                {
                    logger.Exception(100, "Login to bridge failed. Bridge Name = " + party.cMcu.sName);
                    return false;
                }
                logger.Trace("Into Add Party Finally");
                // if conf found, add the participant
                string addPartyXml = null; ret = false;
                ret = CreateXML_AddParty(ref addPartyXml, party, conf);
                if (!ret) return false; //error

                // send the transaction
                ret = false;
                string resAddPartyXml = null;
                ret = SendTransaction_overXMLHTTP(party.cMcu, addPartyXml, ref resAddPartyXml);
                if (!ret) return false; //error


                // Process the response
                ret = false;
                ret = ProcessXML_AddParty(ref resAddPartyXml);
                if (!ret) return false; //error

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool CreateXML_AddParty(ref string addPartyXml, NS_MESSENGER.Party party, NS_MESSENGER.Conference conf)
		{
            String sPartyLineRate, sPartyprotocol = "";
			try
			{
                sPartyLineRate = "1024";
                EquateLineRate(party.stLineRate.etLineRate, true, ref sPartyLineRate);
                sPartyprotocol = "h323";

                if (party.etProtocol == NS_MESSENGER.Party.eProtocol.IP)
                    sPartyprotocol = "ip";
                else if (party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP)
                    sPartyprotocol = "sip";

				addPartyXml ="<hel:GuiPlayer_dialCall>";
                addPartyXml +="<hel:sessionID>"+ party.cMcu.sToken +"</hel:sessionID>";
                addPartyXml += "<hel:confUid>"+ conf.ESId +"</hel:confUid>";
                addPartyXml +="<hel:number>"+ party.sAddress.Trim() +"</hel:number>";
                addPartyXml +="<hel:type>"+ party.etCallType +"</hel:type>";
                addPartyXml += "<hel:protocol>" + sPartyprotocol + "</hel:protocol>";
                addPartyXml += "<hel:bitrate>" + sPartyLineRate + "</hel:bitrate>";
                addPartyXml +="<hel:transId>"+ Guid.NewGuid().ToString() +"</hel:transId>";
                addPartyXml +="</hel:GuiPlayer_dialCall>";

                addPartyXml = CreateRequestXML(addPartyXml);
                			
				return true;
			}
			catch (Exception e)
			{
				logger.Exception (100,e.Message);
				return false;
			}
		}

		private bool ProcessXML_AddParty (ref string responseXml)
		{
            logger.Trace("ProcessXML_Login");
            String bodyXML = "";
            XmlNode node = null;
            XmlDocument xd = null;

            try
            {
                if (responseXml.Length < 1) return false; 	// empty login response 

                bodyXML = GetSoapBody(responseXml);

                xd = new XmlDocument();
                xd.LoadXml(bodyXML);

                node = xd.GetElementsByTagName("faultstring")[0];

                if (node != null)
                {
                    this.errMsg = node.InnerText;
                    return false; //error
 
                }
				
				
				node = xd.GetElementsByTagName("_rv")[0];


                if (node == null)
                {
                    this.errMsg = "Add endpoint failed in MCU.";
                    return false; //error
                }

                if (Int32.Parse(node.InnerText) <= 0)
                {
                    this.errMsg = "Add endpoint failed in MCU.";
                    return false; //error
                }

                return true;

			}
			catch(Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
			return true;
		}
		
		#endregion

		#region Terminal Services Operations
        		
        private bool CreateXML_ChangeDisplayLayoutOfConference( ref string newLayoutPartyXml,NS_MESSENGER.MCU mcu , int confIdOnMcu,int layout)
		{
			try
			{
				
			
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}        
        
		private bool CreateXML_MuteParty(ref string mutePartyXml,NS_MESSENGER.MCU mcu, int confIdOnMcu,int partyIdOnMcu,bool audioMute)
		{
			try
			{
				
			
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		
		private bool CreateXML_TerminateConference(ref string terminateXml,NS_MESSENGER.MCU mcu, string confIdOnMcu)
		{
			try 
			{
                if (confIdOnMcu != "" && confIdOnMcu.Length > 12)
                {
                    terminateXml = "<hel:Scheduler_removeEvents>";
                    terminateXml += "<hel:sessionID>"+ mcu.sToken +"</hel:sessionID>";
                    terminateXml += "<hel:uids>" + confIdOnMcu + "</hel:uids>";
                    terminateXml += "</hel:Scheduler_removeEvents>";

                    terminateXml = CreateRequestXML(terminateXml);

 
                }
				

				return true;
			}			
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}

        private bool ProcessXML_TerminateReservation(string responseXML)
        {
            String description = "";
            XmlNode node = null;
            XmlDocument xd = null;
            String bodyXML = "";
            try
            {
                if (responseXML.Length < 1) return false; 	// empty login response 

                bodyXML = GetSoapBody(responseXML);

                if (bodyXML.Length < 1) return false;

                xd = new XmlDocument();
                xd.LoadXml(bodyXML);

                node = xd.GetElementsByTagName("faultstring")[0];

                if (node != null)
                {
                    this.errMsg = node.InnerText;
                    return false; //error

                }


                node = xd.GetElementsByTagName("_rv")[0];


                if (node != null)
                {
                    description = node.InnerText.Trim().ToUpper();

                    if (Int32.Parse(description) < 0)
                    {
                        this.errMsg = "Request to terminate falied.";
                        return false;
                    }


                    return true;

                }

                logger.Exception(100, "Conf terminate on lifesize failed. ResponseXML = " + description);
                return false;//conf setup was unsuccessfull	
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
		
		private bool EquateVideoCodecs (NS_MESSENGER.Conference.eVideoCodec videoCodec,ref string polycomVideoCodec)
		{
			try 
			{
				// equating to VRM to Polycom values
				if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.AUTO)
				{
					polycomVideoCodec = "auto";
				}
				else
				{
					if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.H261)
					{
						polycomVideoCodec = "h261";
					}
					else
					{
						if (videoCodec == NS_MESSENGER.Conference.eVideoCodec.H263)
						{
							polycomVideoCodec = "h263";
						}
						else
						{
							polycomVideoCodec = "h264";
						}
					}
				}
				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		private bool EquateAudioCodec (NS_MESSENGER.Conference.eAudioCodec audioCodec, ref string polycomAudioCodec)
		{
			try 
			{
			
				if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_56)
					polycomAudioCodec = "g722_56";
				else
				{
					if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G728)
						polycomAudioCodec = "g728";
					else
					{
						if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_32)
							polycomAudioCodec = "g722_32";
						else
						{
							if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G722_24)
								polycomAudioCodec = "g722_24";
							else
							{
								if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.G711_56)
									polycomAudioCodec = "g711_56";
								else
								{
									if (audioCodec == NS_MESSENGER.Conference.eAudioCodec.AUTO)
										polycomAudioCodec = "auto";								
								}
							}
						}
					}	
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		private bool EquateLineRate (NS_MESSENGER.LineRate.eLineRate lineRate,bool isIP, ref string lifesizeLineRate)
		{
			try 
			{
                // equating VRM to Polycom values

                
                if (isIP)
                {
                    #region IP linerate mappings
                    //Modified during FB 1742
                    switch (lineRate)
                    {
                        case NS_MESSENGER.LineRate.eLineRate.K64:
                            {
                                lifesizeLineRate = "64";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K128:
                            {
                                lifesizeLineRate = "128";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K192:
                            {
                                lifesizeLineRate = "128";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K256:
                            {
                                lifesizeLineRate = "256";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K320:
                            {
                                lifesizeLineRate = "256";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K384:
                            {
                                lifesizeLineRate = "384";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K512:
                            {
                                lifesizeLineRate = "512";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.K768:
                            {
                                lifesizeLineRate = "768";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1024:
                            {
                                lifesizeLineRate = "1024";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1152:
                            {
                                lifesizeLineRate = "1152";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1250:
                            {
                                lifesizeLineRate = "1152";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1472:
                            {
                                lifesizeLineRate = "1472";
                                break;
                            }

                        case NS_MESSENGER.LineRate.eLineRate.M1536:
                            {
                                lifesizeLineRate = "1472";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1792:
                            {
                                lifesizeLineRate = "1472";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M1920:
                            {
                                lifesizeLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M2048:
                            {
                                lifesizeLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M2560:
                            {
                                lifesizeLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M3072:
                            {
                                lifesizeLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M3584:
                            {
                                lifesizeLineRate = "1920";
                                break;
                            }
                        case NS_MESSENGER.LineRate.eLineRate.M4096:
                            {
                                lifesizeLineRate = "4096";
                                break;
                            }
                        default:
                            {
                                // default is 384 kbps
                                lifesizeLineRate = "384";
                                break;
                            }
                    }
            #endregion
                }
                else
                {
                    #region ISDN linerate mappings
                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                        lifesizeLineRate = "channel_1";
                    else
                    {
                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                            lifesizeLineRate = "channel_2";
                        else
                        {
                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                                lifesizeLineRate = "channel_4";
                            else
                            {
                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                                    lifesizeLineRate = "channel_8";
                                else
                                {
                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                                        lifesizeLineRate = "channel_12";
                                    else
                                    {
                                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                                            lifesizeLineRate = "channel_18";
                                        else
                                        {
                                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                                                lifesizeLineRate = "channel_23";
                                            else
                                            {
                                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                                                    lifesizeLineRate = "channel_24";
                                                else
                                                {
                                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1920)
                                                        lifesizeLineRate = "channel_30";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }

				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}
		}
		private bool EquateVideoLayout (int videoLayoutID, ref string lifesizeVideoLayoutValue)
		{

            #if COMMENT_DO_NOT_DELETE
            full.screen,Full Screen (1)
            part.2.5.ceo,Large Center (5)
            part.6.8.left,Large Left (8)
            part.2.10.horiz,2 Horizontal (10)
            part.2.10.vert,2 Vertical (10)
            part.4.4.2x2,2 x 2 (4)
            part.3.7.top,3 Top (7)
            part.3.7.bottom,3 Bottom (7)
            part.3.10.top,2 Top (10)
            part.5.16.4x4,4 x 4 (16)
            #endif
            lifesizeVideoLayoutValue = "fullscreen";
			try
			{
				switch (videoLayoutID)
				{
					case 1:
					{
						lifesizeVideoLayoutValue = "fullscreen";
						break;
					}
					case 2:
					{
						lifesizeVideoLayoutValue = "4";
						break;
					}
                    case 4: //FB 2335
                    {
                        lifesizeVideoLayoutValue = "16";
                        break;
                    }
                    #region Commented Layouts
                    /* Commented out for now tillwe get a better picture
					case 5:
					{
						lifesizeVideoLayoutValue = "1and5";
						break;
					}
					case 6:
					{
						lifesizeVideoLayoutValue = "1and7";
						break;
					}
					// case 7-11 : layout is not supported 
					case 12:
					{
						lifesizeVideoLayoutValue = "1x2Hor";
						break;
					}
					case 13:
					{
						lifesizeVideoLayoutValue = "1and2HorUpper";
						break;
					}
					case 14:
					{
						lifesizeVideoLayoutValue = "1and3HorUpper";
						break;
					}
					case 15:
					{
						lifesizeVideoLayoutValue = "1and4HorUpper";
						break;
					}
					case 16:
					{
						// Changed from 1x2Ver to 1x2 on BTBoces request (case 1606)
						lifesizeVideoLayoutValue = "1x2";
						break;
					}
					case 17:
					{
						lifesizeVideoLayoutValue = "1and2Ver";
						break;
					}
					case 18:
					{
						lifesizeVideoLayoutValue = "1and3Ver";
						break;
					}
					case 19:
					{
						lifesizeVideoLayoutValue = "1and4Ver";
						break;
					}
					case 20:
					{
						lifesizeVideoLayoutValue = "1and8Lower";
						break;
					}
					
					//case 21-23 : layout is not supported
					case 24:
					{
						lifesizeVideoLayoutValue = "1and8Central"; 
						break;
					}
					case 25:
					{
						lifesizeVideoLayoutValue = "2and8"; 
						break;
					}
					//case 26-32: layout is not supported					
					case 33:
					{
						lifesizeVideoLayoutValue = "1and12"; 
						break;
					}
                    case 60://FB 2335
                    {
                        lifesizeVideoLayoutValue = "1and8Upper";
                        break;
                    }
                    case 61://FB 2335
                    {
                        lifesizeVideoLayoutValue = "1and2Hor";
                        break;
                    }
                    case 62://FB 2335
                    {
                        lifesizeVideoLayoutValue = "1and3Hor";
                        break;
                    }
                    case 63://FB 2335
                    {
                        lifesizeVideoLayoutValue = "1and4Hor";
                        break;
                    }
                    
                        

                    
                       
					//case 33-42: layout is not supported					

					//case 101-109 are all custom to btboces for MGC accord bridges
					case 101:
					{
						lifesizeVideoLayoutValue = "1x1"; 
						break;
					}
					case 102:
					{
						lifesizeVideoLayoutValue = "1x2";//"1x2Ver"; 
						break;
					}
					case 103:
					{
						lifesizeVideoLayoutValue = "1and2HorUpper"; //"1x2"; 
						break;
					}
					case 104:
					{
						lifesizeVideoLayoutValue = "2x2"; //"1and2HorUpper"; 
						break;
					}
					case 105:
					{
						lifesizeVideoLayoutValue = "1and5"; //"1and2Ver"; 
						break;
					}
					case 106:
					{
						lifesizeVideoLayoutValue = "1x2Ver"; //"2x2"; 
						break;
					}
					case 107:
					{
						lifesizeVideoLayoutValue = "1and2Ver";//"1and3Ver"; 
						break;
					}
					case 108:
					{
						lifesizeVideoLayoutValue = "1and3Ver"; //"1and5"; 
						break;
					}
					case 109:
					{
						lifesizeVideoLayoutValue = "1and7"; 
						break;
					}	*/
                    #endregion

                    default:
					{
						// layout not supported
						this.errMsg = "This video layout is not supported on Polycom MCU.";
						return false;
					}
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return (false);
			}		
		}

		
				
		#endregion
        
		internal bool TestMCUConnection (NS_MESSENGER.MCU mcu)
		{
			try 
			{				
				// Login to the bridge the party is on.
				bool ret = LoginToBridge(ref mcu);
				if (!ret) 
				{
                    if (this.errMsg != null)
                    {
                        this.errMsg = "Connection to MCU failed. Error: " + this.errMsg;
                    }
                    else
                    {
                        this.errMsg = "Connection to MCU failed.";
                    }
					return false; 
				}				
				return true;
			}
			catch (Exception e)
			{
				logger.Exception(100,e.Message);
				return false;
			}			
		}

        private String GetSoapBody(string responseXml)
        {
            logger.Trace("ProcessXML_Login");
            String soapBody = "";
            String env = "http://schemas.xmlsoap.org/soap/envelope/";
            XmlDocument respDoc = null;
            XmlNamespaceManager nsmgr = null;
            XmlNode soapBodyNode = null;

            try
            {
                if (responseXml.Length < 1) return ""; 	// empty login response 

                respDoc = new XmlDocument();
                respDoc.LoadXml(responseXml);

                nsmgr = new XmlNamespaceManager(respDoc.NameTable);
                nsmgr.AddNamespace("env", env);
                soapBodyNode = respDoc.DocumentElement.SelectSingleNode("env:Body", nsmgr);

                if (soapBodyNode == null) return "";

                soapBody = soapBodyNode.InnerXml;                

            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                string debugString = "Error Getting body = " + e.Message;
                debugString += ". ResponseXML = " + responseXml;
                logger.Exception(100, debugString);
            }

            return soapBody;
        }

        private String CreateRequestXML(String requestXml)
        {
            String soapRequest = "";

            try
            {
                soapRequest = string.Format(soapWrapper, requestXml);

            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                string debugString = "Error Creating Request = " + e.Message;
                logger.Exception(100, debugString);
            }

            return soapRequest;
        }

        //ZD 100369_MCU Start
        internal bool FetchMCUDetails(NS_MESSENGER.MCU mcu, int Timeout)
        {
            bool ret = false;
            int pollstatus = (int)NS_MESSENGER.MCU.PollState.PASS;
            try
            {
                
                // Create the login record 
                string loginXml = null; ret = false;
                ret = CreateXML_Login(ref mcu, ref loginXml);
                if (!ret)
                {
                    logger.Exception(100, "CreateXML failed for bridge .Bridge Name = " + mcu.sName);
                    pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                }
                else
                {
                    // Login to the bridge.
                    string responseXml = null; ret = false;
                    {
                        ret = SendMCUStatusCommand_overXMLHTTP(mcu, loginXml, ref responseXml, Timeout);
                    }
                    if (!ret)
                    {
                        logger.Exception(100, " SendTransaction failed for bridge .Bridge Name = " + mcu.sName);
                        pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                    }
                    else if (responseXml.Length < 1)
                    {
                        pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                    }
                    else
                    {
                        // Parse the response.
                        ret = false;
                        ret = ProcessXML_Login(ref mcu, responseXml);
                        if (!ret)
                        {
                            logger.Exception(100, " ProcessXML failed for bridge .Bridge Name = " + mcu.sName);
                            pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                        }
                    }
                }
                db.UpdateMcuStatus(mcu, pollstatus);
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                db.UpdateMcuStatus(mcu, pollstatus);
                return false;
            }
        }
        //ZD 100369_MCU End
	}
}
